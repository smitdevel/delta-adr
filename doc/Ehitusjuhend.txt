
Build:
	mvn clean package
And result is target/adr.war

Regenerate DAO and model classes (needs to establish database connection):
	mvn mybatis-generator:generate

Check if there are newer versions of dependencies available:
	mvn versions:display-dependency-updates
