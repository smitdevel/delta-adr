/* IE6 flicker fix
-------------------------------------------------- */
/*@cc_on
try {
	document.execCommand("BackgroundImageCache", false, true);
} catch(err) {}
@*/

/* jQuery
-------------------------------------------------- */

function dataHover() {
	$("table.data tbody tr").hover(
		function(){
			$(this).addClass("hover");
		},
		function(){
			$(this).removeClass("hover");
		}
	);
}

/* multiselect */

function openMulti() {
	$('.multi-select-trigger').click(function() {
		if ($(this).next('ul').is(':visible')){
			$(this).next('ul').hide();
		}
		else {
			$('.multi-select-trigger').not(this).next('ul').hide();
			$(this).next('ul').show().outerClick(multiEscape);
		}
		escape();
		return false;
	});
}
function checkKlick() {
	$('.multi-select-list').find('LI.item').find('INPUT').click(function() {
		var anchor = $(this).parents('DIV').find('.multi-select-trigger');
		var allTogether = anchor.attr('title');
		var anchorIdSelector = "#"+anchor.attr('id');
		var thisValue = $(this).parents('LI').find('SPAN').html();
		if ( $(this).is(':checked') ){
			if (allTogether.length > 2){
				strPoint = ', '
			}
			else {
				strPoint = ''
			}
			allTogether = allTogether + strPoint + thisValue;
			if (allTogether.substr(0, 2) == ', '){
				allTogether = allTogether.substr(2,allTogether.length-2);
			}
			if (allTogether.substr(-2, 2) == ', '){
				allTogether = allTogether.substr(0,allTogether.length-2);
			}
			allTogether = allTogether.replace(', , ',', ');
			if (allTogether.length > 28){
				allTogetherVisible = allTogether.substr(0, 28) + "&#133;";
			}
			else {
				allTogetherVisible = allTogether;
			}
			$(anchorIdSelector).attr('title',allTogether);
			$(anchorIdSelector).find('SPAN').html(allTogetherVisible);
		} 
		else {
		   var thisWordCount = allTogether.split(thisValue).length - 1;
	      if(thisWordCount > 1) { // more than one occurence of this word
	         var words = allTogether.split(',');
	         allTogether = '';
	         for(var i = 0; i < words.length; i++) {
	            if(thisValue === words[i].trim()) {
	               continue;
	            }
	            allTogether += words[i].trim() + ", ";
	         }
	      } else {
	         allTogether = allTogether.replace(thisValue,'');
	      }
			if (allTogether.substr(0, 2) == ', '){
				allTogether = allTogether.substr(2,allTogether.length-2);
			}
			if (allTogether.substr(-2, 2) == ', '){
				allTogether = allTogether.substr(0,allTogether.length-2);
			}
			allTogether = allTogether.replace(', , ',', ');
			if (allTogether.length > 28){
				allTogetherVisible = allTogether.substr(0, 28) + "&#133;";
			}
			else {
				allTogetherVisible = allTogether;
			}
			$(anchorIdSelector).attr('title',allTogether);
			$(anchorIdSelector).find('SPAN').html(allTogetherVisible);
		}
	});
}
function checkKlickAll() {
	$('.multi-select-list').find('LI.all').find('INPUT').click(function() {
		var allTogether = '';
		var anchorIdSelector = "#"+$(this).parents('DIV').find('.multi-select-trigger').attr('id');
		$(anchorIdSelector).parent().find('LI.item').find('INPUT[type=checkbox]').each(function() {
			allTogether = allTogether + $(this).parents('LI').find('SPAN').html() + ", ";
		});
		if (allTogether.substr(0, 2) == ', '){
			allTogether = allTogether.substr(2,allTogether.length-2);
		}
		if (allTogether.substr(-2, 2) == ', '){
			allTogether = allTogether.substr(0,allTogether.length-2);
		}
		allTogether = allTogether.replace(', , ',', ');
		if ( $(this).is(':checked') ){
			$(this).parents('UL').find('INPUT').attr('checked','checked');
			$(anchorIdSelector).attr('title',allTogether);
			if (allTogether.length > 28){
				allTogetherVisible = allTogether.substr(0, 21) + "&#133;";
			}
			else {
				allTogetherVisible = allTogether;
			}
			$(anchorIdSelector).find('SPAN').html(allTogetherVisible);
		}
		else {
			$(this).parents('UL').find('INPUT').attr('checked','');
			$(anchorIdSelector).attr('title','');
			$(anchorIdSelector).find('SPAN').html('');
		}
	});
}
function multiHover() {
	$('.multi-select-list').find('LI').hover(
		function() {
			$(this).addClass('hover');
		},
		function() {
			$(this).removeClass('hover');
		}
	);
}
function multiEscape() {
	$(this).hide();
}
function multihide() {
	$('.multi-select-list').hide();
}
function escape(){
	document.onkeyup = function(e) {
		var code;
		if (!e) var e = window.event;
		if (e.keyCode) code = e.keyCode;
		else if (e.which) code = e.which;
		if (code == 27){
			multihide();
		}
	}
}

/* hint */

function hint(){
	if (!($('#hint').length)){
		$('body').append('<div id="hint"><div class="inner"></div></div>')
	}
	var hint = $('#hint');

	$('.hint-trigger').mouseenter(function(){
		$('#hint').clearQueue().stop();
		$('.hint-trigger-hover').clearQueue().stop();

		var wrap = $(this);
		var offset = wrap.offset();
		var content = wrap.find('.hint-content').html();
		
		$('.hint-trigger-hover').removeClass('hint-trigger-hover');
		wrap.addClass('hint-trigger-hover');
		
		hint.children('.inner').html(content)
		hint.css({
			top: offset.top + 'px',
			left: offset.left + wrap.outerWidth() - 3 + 'px'
		}).show()
	});
	$('.hint-trigger').mouseleave(function(){
		$(this).delay(50).slideDown(0, function(){ $(this).removeClass('hint-trigger-hover'); });
		hint.delay(50).slideDown(0, function(){ $(this).hide() });
	})
	$('#hint').hover(function(){
		$('.hint-trigger-hover').clearQueue().stop();
		$(this).clearQueue().stop();
	},
	function(){
		$(this).delay(50).slideDown(0, function(){ $(this).hide() });
		$('.hint-trigger-hover').delay(50).slideDown(0, function(){ $(this).removeClass('hint-trigger-hover'); });
	});
	
	if($.browser.msie && ($.browser.version < 7) ){
		$("#hint").bgiframe();
	}
}

/* document ready */

$(function(){

	/* datepicker */

	if ( $("input.date").length ) {
		$("input.date").datepick({
			showSpeed: 100,
			dateFormat: 'dd.mm.yyyy',
			firstDay: 1,
			onChangeMonthYear : function() {
				$('.datepick').find('.datepick-highlight').removeClass('datepick-highlight');
			}
		});
	}

	/* hint */

	hint()

	/* multi check */

	openMulti();
	checkKlick();
	checkKlickAll();
	multiHover();
	if($.browser.msie && ($.browser.version < 7) ){
		$(".multi-select-list").bgiframe();
	}

	/* IE6 */
	if($.browser.msie && ($.browser.version < 7) ){
		dataHover();
	}

   if ($('form.submitted').length > 0) {
   	$('.pager a').click(function(){
   		var pageNumber = $(this).attr('href');
   		if (pageNumber.lastIndexOf('/') >= 0) {
   		   pageNumber = pageNumber.substr(pageNumber.lastIndexOf('/') + 1);
      		$('form.submitted input[name=pageNumber]').val(pageNumber);
      		$('form.submitted').submit();
   	   }
         return false;
   	});
   }

	$('.multi-select').each(function() {
		var allTogether = '';
		var anchorIdSelector = "#" + $(this).find('.multi-select-trigger').attr('id');
		$(this).find('.multi-select-list').find('LI.item').find('INPUT:checked').each(function() {
			allTogether = allTogether + $(this).parents('LI.item').find('SPAN').html() + ", "
		});
		if (allTogether.substr(0, 2) == ', ') {
			allTogether = allTogether.substr(2,allTogether.length-2);
		}
		if (allTogether.substr(-2, 2) == ', ') {
			allTogether = allTogether.substr(0,allTogether.length-2);
		}
		allTogether = allTogether.replace(', , ',', ');
		$(anchorIdSelector).attr('title',allTogether);
		if (allTogether.length > 28) {
			allTogetherVisible = allTogether.substr(0, 21) + "&#133;";
		} else {
			allTogetherVisible = allTogether;
		}
		$(anchorIdSelector).find('SPAN').html(allTogetherVisible);
	});

});

/* window onload */

$(window).load(function(){

});