package ee.webmedia.adr.dao;

import ee.webmedia.adr.model.Document;
import ee.webmedia.adr.model.DocumentExample;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;

public interface DocumentMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    @SelectProvider(type=DocumentSqlProvider.class, method="countByExample")
    int countByExample(DocumentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    @DeleteProvider(type=DocumentSqlProvider.class, method="deleteByExample")
    int deleteByExample(DocumentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    @Delete({
        "delete from document",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    @Insert({
        "insert into document (id, organization_id, ",
        "noderef, reg_date_time, ",
        "reg_number, title, ",
        "type_id, volume_id, ",
        "access_restriction, access_restriction_reason, ",
        "access_restriction_begin_date, access_restriction_end_date, ",
        "access_restriction_end_desc, due_date, ",
        "compliance_date, compilator, ",
        "annex, sender_reg_number, ",
        "transmittal_mode, party, ",
        "created_date_time, modified_date_time, ",
        "by_request_only, access_restriction_change_reason, ",
        "send_date)",
        "values (#{id,jdbcType=INTEGER}, #{organizationId,jdbcType=INTEGER}, ",
        "#{noderef,jdbcType=VARCHAR}, #{regDateTime,jdbcType=TIMESTAMP}, ",
        "#{regNumber,jdbcType=VARCHAR}, #{title,jdbcType=VARCHAR}, ",
        "#{typeId,jdbcType=INTEGER}, #{volumeId,jdbcType=INTEGER}, ",
        "#{accessRestriction,jdbcType=VARCHAR}, #{accessRestrictionReason,jdbcType=VARCHAR}, ",
        "#{accessRestrictionBeginDate,jdbcType=DATE}, #{accessRestrictionEndDate,jdbcType=DATE}, ",
        "#{accessRestrictionEndDesc,jdbcType=VARCHAR}, #{dueDate,jdbcType=DATE}, ",
        "#{complianceDate,jdbcType=DATE}, #{compilator,jdbcType=VARCHAR}, ",
        "#{annex,jdbcType=VARCHAR}, #{senderRegNumber,jdbcType=VARCHAR}, ",
        "#{transmittalMode,jdbcType=VARCHAR}, #{party,jdbcType=VARCHAR}, ",
        "#{createdDateTime,jdbcType=TIMESTAMP}, #{modifiedDateTime,jdbcType=TIMESTAMP}, ",
        "#{byRequestOnly,jdbcType=BIT}, #{accessRestrictionChangeReason,jdbcType=VARCHAR}, ",
        "#{sendDate,jdbcType=TIMESTAMP})"
    })
    int insert(Document record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    @InsertProvider(type=DocumentSqlProvider.class, method="insertSelective")
    int insertSelective(Document record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    @SelectProvider(type=DocumentSqlProvider.class, method="selectByExample")
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="organization_id", property="organizationId", jdbcType=JdbcType.INTEGER),
        @Result(column="noderef", property="noderef", jdbcType=JdbcType.VARCHAR),
        @Result(column="reg_date_time", property="regDateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="reg_number", property="regNumber", jdbcType=JdbcType.VARCHAR),
        @Result(column="title", property="title", jdbcType=JdbcType.VARCHAR),
        @Result(column="type_id", property="typeId", jdbcType=JdbcType.INTEGER),
        @Result(column="volume_id", property="volumeId", jdbcType=JdbcType.INTEGER),
        @Result(column="access_restriction", property="accessRestriction", jdbcType=JdbcType.VARCHAR),
        @Result(column="access_restriction_reason", property="accessRestrictionReason", jdbcType=JdbcType.VARCHAR),
        @Result(column="access_restriction_begin_date", property="accessRestrictionBeginDate", jdbcType=JdbcType.DATE),
        @Result(column="access_restriction_end_date", property="accessRestrictionEndDate", jdbcType=JdbcType.DATE),
        @Result(column="access_restriction_end_desc", property="accessRestrictionEndDesc", jdbcType=JdbcType.VARCHAR),
        @Result(column="due_date", property="dueDate", jdbcType=JdbcType.DATE),
        @Result(column="compliance_date", property="complianceDate", jdbcType=JdbcType.DATE),
        @Result(column="compilator", property="compilator", jdbcType=JdbcType.VARCHAR),
        @Result(column="annex", property="annex", jdbcType=JdbcType.VARCHAR),
        @Result(column="sender_reg_number", property="senderRegNumber", jdbcType=JdbcType.VARCHAR),
        @Result(column="transmittal_mode", property="transmittalMode", jdbcType=JdbcType.VARCHAR),
        @Result(column="party", property="party", jdbcType=JdbcType.VARCHAR),
        @Result(column="created_date_time", property="createdDateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="modified_date_time", property="modifiedDateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="by_request_only", property="byRequestOnly", jdbcType=JdbcType.BIT),
        @Result(column="access_restriction_change_reason", property="accessRestrictionChangeReason", jdbcType=JdbcType.VARCHAR),
        @Result(column="send_date", property="sendDate", jdbcType=JdbcType.TIMESTAMP)
    })
    List<Document> selectByExample(DocumentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    @Select({
        "select",
        "id, organization_id, noderef, reg_date_time, reg_number, title, type_id, volume_id, ",
        "access_restriction, access_restriction_reason, access_restriction_begin_date, ",
        "access_restriction_end_date, access_restriction_end_desc, due_date, compliance_date, ",
        "compilator, annex, sender_reg_number, transmittal_mode, party, created_date_time, ",
        "modified_date_time, by_request_only, access_restriction_change_reason, send_date",
        "from document",
        "where id = #{id,jdbcType=INTEGER}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.INTEGER, id=true),
        @Result(column="organization_id", property="organizationId", jdbcType=JdbcType.INTEGER),
        @Result(column="noderef", property="noderef", jdbcType=JdbcType.VARCHAR),
        @Result(column="reg_date_time", property="regDateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="reg_number", property="regNumber", jdbcType=JdbcType.VARCHAR),
        @Result(column="title", property="title", jdbcType=JdbcType.VARCHAR),
        @Result(column="type_id", property="typeId", jdbcType=JdbcType.INTEGER),
        @Result(column="volume_id", property="volumeId", jdbcType=JdbcType.INTEGER),
        @Result(column="access_restriction", property="accessRestriction", jdbcType=JdbcType.VARCHAR),
        @Result(column="access_restriction_reason", property="accessRestrictionReason", jdbcType=JdbcType.VARCHAR),
        @Result(column="access_restriction_begin_date", property="accessRestrictionBeginDate", jdbcType=JdbcType.DATE),
        @Result(column="access_restriction_end_date", property="accessRestrictionEndDate", jdbcType=JdbcType.DATE),
        @Result(column="access_restriction_end_desc", property="accessRestrictionEndDesc", jdbcType=JdbcType.VARCHAR),
        @Result(column="due_date", property="dueDate", jdbcType=JdbcType.DATE),
        @Result(column="compliance_date", property="complianceDate", jdbcType=JdbcType.DATE),
        @Result(column="compilator", property="compilator", jdbcType=JdbcType.VARCHAR),
        @Result(column="annex", property="annex", jdbcType=JdbcType.VARCHAR),
        @Result(column="sender_reg_number", property="senderRegNumber", jdbcType=JdbcType.VARCHAR),
        @Result(column="transmittal_mode", property="transmittalMode", jdbcType=JdbcType.VARCHAR),
        @Result(column="party", property="party", jdbcType=JdbcType.VARCHAR),
        @Result(column="created_date_time", property="createdDateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="modified_date_time", property="modifiedDateTime", jdbcType=JdbcType.TIMESTAMP),
        @Result(column="by_request_only", property="byRequestOnly", jdbcType=JdbcType.BIT),
        @Result(column="access_restriction_change_reason", property="accessRestrictionChangeReason", jdbcType=JdbcType.VARCHAR),
        @Result(column="send_date", property="sendDate", jdbcType=JdbcType.TIMESTAMP)
    })
    Document selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    @UpdateProvider(type=DocumentSqlProvider.class, method="updateByExampleSelective")
    int updateByExampleSelective(@Param("record") Document record, @Param("example") DocumentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    @UpdateProvider(type=DocumentSqlProvider.class, method="updateByExample")
    int updateByExample(@Param("record") Document record, @Param("example") DocumentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    @UpdateProvider(type=DocumentSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(Document record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    @Update({
        "update document",
        "set organization_id = #{organizationId,jdbcType=INTEGER},",
          "noderef = #{noderef,jdbcType=VARCHAR},",
          "reg_date_time = #{regDateTime,jdbcType=TIMESTAMP},",
          "reg_number = #{regNumber,jdbcType=VARCHAR},",
          "title = #{title,jdbcType=VARCHAR},",
          "type_id = #{typeId,jdbcType=INTEGER},",
          "volume_id = #{volumeId,jdbcType=INTEGER},",
          "access_restriction = #{accessRestriction,jdbcType=VARCHAR},",
          "access_restriction_reason = #{accessRestrictionReason,jdbcType=VARCHAR},",
          "access_restriction_begin_date = #{accessRestrictionBeginDate,jdbcType=DATE},",
          "access_restriction_end_date = #{accessRestrictionEndDate,jdbcType=DATE},",
          "access_restriction_end_desc = #{accessRestrictionEndDesc,jdbcType=VARCHAR},",
          "due_date = #{dueDate,jdbcType=DATE},",
          "compliance_date = #{complianceDate,jdbcType=DATE},",
          "compilator = #{compilator,jdbcType=VARCHAR},",
          "annex = #{annex,jdbcType=VARCHAR},",
          "sender_reg_number = #{senderRegNumber,jdbcType=VARCHAR},",
          "transmittal_mode = #{transmittalMode,jdbcType=VARCHAR},",
          "party = #{party,jdbcType=VARCHAR},",
          "created_date_time = #{createdDateTime,jdbcType=TIMESTAMP},",
          "modified_date_time = #{modifiedDateTime,jdbcType=TIMESTAMP},",
          "by_request_only = #{byRequestOnly,jdbcType=BIT},",
          "access_restriction_change_reason = #{accessRestrictionChangeReason,jdbcType=VARCHAR},",
          "send_date = #{sendDate,jdbcType=TIMESTAMP}",
        "where id = #{id,jdbcType=INTEGER}"
    })
    int updateByPrimaryKey(Document record);
}