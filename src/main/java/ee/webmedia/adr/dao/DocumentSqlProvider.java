package ee.webmedia.adr.dao;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import ee.webmedia.adr.model.Document;
import ee.webmedia.adr.model.DocumentExample.Criteria;
import ee.webmedia.adr.model.DocumentExample.Criterion;
import ee.webmedia.adr.model.DocumentExample;
import java.util.List;
import java.util.Map;

public class DocumentSqlProvider {

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    public String countByExample(DocumentExample example) {
        BEGIN();
        SELECT("count (*)");
        FROM("document");
        applyWhere(example, false);
        return SQL();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    public String deleteByExample(DocumentExample example) {
        BEGIN();
        DELETE_FROM("document");
        applyWhere(example, false);
        return SQL();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    public String insertSelective(Document record) {
        BEGIN();
        INSERT_INTO("document");
        
        if (record.getId() != null) {
            VALUES("id", "#{id,jdbcType=INTEGER}");
        }
        
        if (record.getOrganizationId() != null) {
            VALUES("organization_id", "#{organizationId,jdbcType=INTEGER}");
        }
        
        if (record.getNoderef() != null) {
            VALUES("noderef", "#{noderef,jdbcType=VARCHAR}");
        }
        
        if (record.getRegDateTime() != null) {
            VALUES("reg_date_time", "#{regDateTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getRegNumber() != null) {
            VALUES("reg_number", "#{regNumber,jdbcType=VARCHAR}");
        }
        
        if (record.getTitle() != null) {
            VALUES("title", "#{title,jdbcType=VARCHAR}");
        }
        
        if (record.getTypeId() != null) {
            VALUES("type_id", "#{typeId,jdbcType=INTEGER}");
        }
        
        if (record.getVolumeId() != null) {
            VALUES("volume_id", "#{volumeId,jdbcType=INTEGER}");
        }
        
        if (record.getAccessRestriction() != null) {
            VALUES("access_restriction", "#{accessRestriction,jdbcType=VARCHAR}");
        }
        
        if (record.getAccessRestrictionReason() != null) {
            VALUES("access_restriction_reason", "#{accessRestrictionReason,jdbcType=VARCHAR}");
        }
        
        if (record.getAccessRestrictionBeginDate() != null) {
            VALUES("access_restriction_begin_date", "#{accessRestrictionBeginDate,jdbcType=DATE}");
        }
        
        if (record.getAccessRestrictionEndDate() != null) {
            VALUES("access_restriction_end_date", "#{accessRestrictionEndDate,jdbcType=DATE}");
        }
        
        if (record.getAccessRestrictionEndDesc() != null) {
            VALUES("access_restriction_end_desc", "#{accessRestrictionEndDesc,jdbcType=VARCHAR}");
        }
        
        if (record.getDueDate() != null) {
            VALUES("due_date", "#{dueDate,jdbcType=DATE}");
        }
        
        if (record.getComplianceDate() != null) {
            VALUES("compliance_date", "#{complianceDate,jdbcType=DATE}");
        }
        
        if (record.getCompilator() != null) {
            VALUES("compilator", "#{compilator,jdbcType=VARCHAR}");
        }
        
        if (record.getAnnex() != null) {
            VALUES("annex", "#{annex,jdbcType=VARCHAR}");
        }
        
        if (record.getSenderRegNumber() != null) {
            VALUES("sender_reg_number", "#{senderRegNumber,jdbcType=VARCHAR}");
        }
        
        if (record.getTransmittalMode() != null) {
            VALUES("transmittal_mode", "#{transmittalMode,jdbcType=VARCHAR}");
        }
        
        if (record.getParty() != null) {
            VALUES("party", "#{party,jdbcType=VARCHAR}");
        }
        
        if (record.getCreatedDateTime() != null) {
            VALUES("created_date_time", "#{createdDateTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getModifiedDateTime() != null) {
            VALUES("modified_date_time", "#{modifiedDateTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getByRequestOnly() != null) {
            VALUES("by_request_only", "#{byRequestOnly,jdbcType=BIT}");
        }
        
        if (record.getAccessRestrictionChangeReason() != null) {
            VALUES("access_restriction_change_reason", "#{accessRestrictionChangeReason,jdbcType=VARCHAR}");
        }
        
        if (record.getSendDate() != null) {
            VALUES("send_date", "#{sendDate,jdbcType=TIMESTAMP}");
        }
        
        return SQL();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    public String selectByExample(DocumentExample example) {
        BEGIN();
        if (example != null && example.isDistinct()) {
            SELECT_DISTINCT("id");
        } else {
            SELECT("id");
        }
        SELECT("organization_id");
        SELECT("noderef");
        SELECT("reg_date_time");
        SELECT("reg_number");
        SELECT("title");
        SELECT("type_id");
        SELECT("volume_id");
        SELECT("access_restriction");
        SELECT("access_restriction_reason");
        SELECT("access_restriction_begin_date");
        SELECT("access_restriction_end_date");
        SELECT("access_restriction_end_desc");
        SELECT("due_date");
        SELECT("compliance_date");
        SELECT("compilator");
        SELECT("annex");
        SELECT("sender_reg_number");
        SELECT("transmittal_mode");
        SELECT("party");
        SELECT("created_date_time");
        SELECT("modified_date_time");
        SELECT("by_request_only");
        SELECT("access_restriction_change_reason");
        SELECT("send_date");
        FROM("document");
        applyWhere(example, false);
        
        if (example != null && example.getOrderByClause() != null) {
            ORDER_BY(example.getOrderByClause());
        }
        
        return SQL();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    public String updateByExampleSelective(Map<String, Object> parameter) {
        Document record = (Document) parameter.get("record");
        DocumentExample example = (DocumentExample) parameter.get("example");
        
        BEGIN();
        UPDATE("document");
        
        if (record.getId() != null) {
            SET("id = #{record.id,jdbcType=INTEGER}");
        }
        
        if (record.getOrganizationId() != null) {
            SET("organization_id = #{record.organizationId,jdbcType=INTEGER}");
        }
        
        if (record.getNoderef() != null) {
            SET("noderef = #{record.noderef,jdbcType=VARCHAR}");
        }
        
        if (record.getRegDateTime() != null) {
            SET("reg_date_time = #{record.regDateTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getRegNumber() != null) {
            SET("reg_number = #{record.regNumber,jdbcType=VARCHAR}");
        }
        
        if (record.getTitle() != null) {
            SET("title = #{record.title,jdbcType=VARCHAR}");
        }
        
        if (record.getTypeId() != null) {
            SET("type_id = #{record.typeId,jdbcType=INTEGER}");
        }
        
        if (record.getVolumeId() != null) {
            SET("volume_id = #{record.volumeId,jdbcType=INTEGER}");
        }
        
        if (record.getAccessRestriction() != null) {
            SET("access_restriction = #{record.accessRestriction,jdbcType=VARCHAR}");
        }
        
        if (record.getAccessRestrictionReason() != null) {
            SET("access_restriction_reason = #{record.accessRestrictionReason,jdbcType=VARCHAR}");
        }
        
        if (record.getAccessRestrictionBeginDate() != null) {
            SET("access_restriction_begin_date = #{record.accessRestrictionBeginDate,jdbcType=DATE}");
        }
        
        if (record.getAccessRestrictionEndDate() != null) {
            SET("access_restriction_end_date = #{record.accessRestrictionEndDate,jdbcType=DATE}");
        }
        
        if (record.getAccessRestrictionEndDesc() != null) {
            SET("access_restriction_end_desc = #{record.accessRestrictionEndDesc,jdbcType=VARCHAR}");
        }
        
        if (record.getDueDate() != null) {
            SET("due_date = #{record.dueDate,jdbcType=DATE}");
        }
        
        if (record.getComplianceDate() != null) {
            SET("compliance_date = #{record.complianceDate,jdbcType=DATE}");
        }
        
        if (record.getCompilator() != null) {
            SET("compilator = #{record.compilator,jdbcType=VARCHAR}");
        }
        
        if (record.getAnnex() != null) {
            SET("annex = #{record.annex,jdbcType=VARCHAR}");
        }
        
        if (record.getSenderRegNumber() != null) {
            SET("sender_reg_number = #{record.senderRegNumber,jdbcType=VARCHAR}");
        }
        
        if (record.getTransmittalMode() != null) {
            SET("transmittal_mode = #{record.transmittalMode,jdbcType=VARCHAR}");
        }
        
        if (record.getParty() != null) {
            SET("party = #{record.party,jdbcType=VARCHAR}");
        }
        
        if (record.getCreatedDateTime() != null) {
            SET("created_date_time = #{record.createdDateTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getModifiedDateTime() != null) {
            SET("modified_date_time = #{record.modifiedDateTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getByRequestOnly() != null) {
            SET("by_request_only = #{record.byRequestOnly,jdbcType=BIT}");
        }
        
        if (record.getAccessRestrictionChangeReason() != null) {
            SET("access_restriction_change_reason = #{record.accessRestrictionChangeReason,jdbcType=VARCHAR}");
        }
        
        if (record.getSendDate() != null) {
            SET("send_date = #{record.sendDate,jdbcType=TIMESTAMP}");
        }
        
        applyWhere(example, true);
        return SQL();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    public String updateByExample(Map<String, Object> parameter) {
        BEGIN();
        UPDATE("document");
        
        SET("id = #{record.id,jdbcType=INTEGER}");
        SET("organization_id = #{record.organizationId,jdbcType=INTEGER}");
        SET("noderef = #{record.noderef,jdbcType=VARCHAR}");
        SET("reg_date_time = #{record.regDateTime,jdbcType=TIMESTAMP}");
        SET("reg_number = #{record.regNumber,jdbcType=VARCHAR}");
        SET("title = #{record.title,jdbcType=VARCHAR}");
        SET("type_id = #{record.typeId,jdbcType=INTEGER}");
        SET("volume_id = #{record.volumeId,jdbcType=INTEGER}");
        SET("access_restriction = #{record.accessRestriction,jdbcType=VARCHAR}");
        SET("access_restriction_reason = #{record.accessRestrictionReason,jdbcType=VARCHAR}");
        SET("access_restriction_begin_date = #{record.accessRestrictionBeginDate,jdbcType=DATE}");
        SET("access_restriction_end_date = #{record.accessRestrictionEndDate,jdbcType=DATE}");
        SET("access_restriction_end_desc = #{record.accessRestrictionEndDesc,jdbcType=VARCHAR}");
        SET("due_date = #{record.dueDate,jdbcType=DATE}");
        SET("compliance_date = #{record.complianceDate,jdbcType=DATE}");
        SET("compilator = #{record.compilator,jdbcType=VARCHAR}");
        SET("annex = #{record.annex,jdbcType=VARCHAR}");
        SET("sender_reg_number = #{record.senderRegNumber,jdbcType=VARCHAR}");
        SET("transmittal_mode = #{record.transmittalMode,jdbcType=VARCHAR}");
        SET("party = #{record.party,jdbcType=VARCHAR}");
        SET("created_date_time = #{record.createdDateTime,jdbcType=TIMESTAMP}");
        SET("modified_date_time = #{record.modifiedDateTime,jdbcType=TIMESTAMP}");
        SET("by_request_only = #{record.byRequestOnly,jdbcType=BIT}");
        SET("access_restriction_change_reason = #{record.accessRestrictionChangeReason,jdbcType=VARCHAR}");
        SET("send_date = #{record.sendDate,jdbcType=TIMESTAMP}");
        
        DocumentExample example = (DocumentExample) parameter.get("example");
        applyWhere(example, true);
        return SQL();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    public String updateByPrimaryKeySelective(Document record) {
        BEGIN();
        UPDATE("document");
        
        if (record.getOrganizationId() != null) {
            SET("organization_id = #{organizationId,jdbcType=INTEGER}");
        }
        
        if (record.getNoderef() != null) {
            SET("noderef = #{noderef,jdbcType=VARCHAR}");
        }
        
        if (record.getRegDateTime() != null) {
            SET("reg_date_time = #{regDateTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getRegNumber() != null) {
            SET("reg_number = #{regNumber,jdbcType=VARCHAR}");
        }
        
        if (record.getTitle() != null) {
            SET("title = #{title,jdbcType=VARCHAR}");
        }
        
        if (record.getTypeId() != null) {
            SET("type_id = #{typeId,jdbcType=INTEGER}");
        }
        
        if (record.getVolumeId() != null) {
            SET("volume_id = #{volumeId,jdbcType=INTEGER}");
        }
        
        if (record.getAccessRestriction() != null) {
            SET("access_restriction = #{accessRestriction,jdbcType=VARCHAR}");
        }
        
        if (record.getAccessRestrictionReason() != null) {
            SET("access_restriction_reason = #{accessRestrictionReason,jdbcType=VARCHAR}");
        }
        
        if (record.getAccessRestrictionBeginDate() != null) {
            SET("access_restriction_begin_date = #{accessRestrictionBeginDate,jdbcType=DATE}");
        }
        
        if (record.getAccessRestrictionEndDate() != null) {
            SET("access_restriction_end_date = #{accessRestrictionEndDate,jdbcType=DATE}");
        }
        
        if (record.getAccessRestrictionEndDesc() != null) {
            SET("access_restriction_end_desc = #{accessRestrictionEndDesc,jdbcType=VARCHAR}");
        }
        
        if (record.getDueDate() != null) {
            SET("due_date = #{dueDate,jdbcType=DATE}");
        }
        
        if (record.getComplianceDate() != null) {
            SET("compliance_date = #{complianceDate,jdbcType=DATE}");
        }
        
        if (record.getCompilator() != null) {
            SET("compilator = #{compilator,jdbcType=VARCHAR}");
        }
        
        if (record.getAnnex() != null) {
            SET("annex = #{annex,jdbcType=VARCHAR}");
        }
        
        if (record.getSenderRegNumber() != null) {
            SET("sender_reg_number = #{senderRegNumber,jdbcType=VARCHAR}");
        }
        
        if (record.getTransmittalMode() != null) {
            SET("transmittal_mode = #{transmittalMode,jdbcType=VARCHAR}");
        }
        
        if (record.getParty() != null) {
            SET("party = #{party,jdbcType=VARCHAR}");
        }
        
        if (record.getCreatedDateTime() != null) {
            SET("created_date_time = #{createdDateTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getModifiedDateTime() != null) {
            SET("modified_date_time = #{modifiedDateTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getByRequestOnly() != null) {
            SET("by_request_only = #{byRequestOnly,jdbcType=BIT}");
        }
        
        if (record.getAccessRestrictionChangeReason() != null) {
            SET("access_restriction_change_reason = #{accessRestrictionChangeReason,jdbcType=VARCHAR}");
        }
        
        if (record.getSendDate() != null) {
            SET("send_date = #{sendDate,jdbcType=TIMESTAMP}");
        }
        
        WHERE("id = #{id,jdbcType=INTEGER}");
        
        return SQL();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table document
     *
     * @mbggenerated
     */
    protected void applyWhere(DocumentExample example, boolean includeExamplePhrase) {
        if (example == null) {
            return;
        }
        
        String parmPhrase1;
        String parmPhrase1_th;
        String parmPhrase2;
        String parmPhrase2_th;
        String parmPhrase3;
        String parmPhrase3_th;
        if (includeExamplePhrase) {
            parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        } else {
            parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
            parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
            parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
            parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
            parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
            parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
        }
        
        StringBuilder sb = new StringBuilder();
        List<Criteria> oredCriteria = example.getOredCriteria();
        boolean firstCriteria = true;
        for (int i = 0; i < oredCriteria.size(); i++) {
            Criteria criteria = oredCriteria.get(i);
            if (criteria.isValid()) {
                if (firstCriteria) {
                    firstCriteria = false;
                } else {
                    sb.append(" or ");
                }
                
                sb.append('(');
                List<Criterion> criterions = criteria.getAllCriteria();
                boolean firstCriterion = true;
                for (int j = 0; j < criterions.size(); j++) {
                    Criterion criterion = criterions.get(j);
                    if (firstCriterion) {
                        firstCriterion = false;
                    } else {
                        sb.append(" and ");
                    }
                    
                    if (criterion.isNoValue()) {
                        sb.append(criterion.getCondition());
                    } else if (criterion.isSingleValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
                        } else {
                            sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,criterion.getTypeHandler()));
                        }
                    } else if (criterion.isBetweenValue()) {
                        if (criterion.getTypeHandler() == null) {
                            sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
                        } else {
                            sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j, criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
                        }
                    } else if (criterion.isListValue()) {
                        sb.append(criterion.getCondition());
                        sb.append(" (");
                        List<?> listItems = (List<?>) criterion.getValue();
                        boolean comma = false;
                        for (int k = 0; k < listItems.size(); k++) {
                            if (comma) {
                                sb.append(", ");
                            } else {
                                comma = true;
                            }
                            if (criterion.getTypeHandler() == null) {
                                sb.append(String.format(parmPhrase3, i, j, k));
                            } else {
                                sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
                            }
                        }
                        sb.append(')');
                    }
                }
                sb.append(')');
            }
        }
        
        if (sb.length() > 0) {
            WHERE(sb.toString());
        }
    }
}