package ee.webmedia.adr;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class AdvancedSearchForm {

    private String title;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date regDateBegin;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date regDateEnd;

    private Integer[] documentTypes;

    private String senderRegNr;

    private Integer[] transmittalModes;

    private String party;

    private String accessRestriction;

    private String accessRestrictionReason;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date accessRestrictionBeginDate;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date accessRestrictionEndDate;

    private int pageNumber;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getRegDateBegin() {
        return regDateBegin;
    }

    public void setRegDateBegin(Date regDateBegin) {
        this.regDateBegin = regDateBegin;
    }

    public Date getRegDateEnd() {
        return regDateEnd;
    }

    public void setRegDateEnd(Date regDateEnd) {
        this.regDateEnd = regDateEnd;
    }

    public Integer[] getDocumentTypes() {
        return documentTypes;
    }

    public void setDocumentTypes(Integer[] documentTypes) {
        this.documentTypes = documentTypes;
    }

    public String getSenderRegNr() {
        return senderRegNr;
    }

    public void setSenderRegNr(String senderRegNr) {
        this.senderRegNr = senderRegNr;
    }

    public Integer[] getTransmittalModes() {
        return transmittalModes;
    }

    public void setTransmittalModes(Integer[] transmittalModes) {
        this.transmittalModes = transmittalModes;
    }

    public String getParty() {
        return party;
    }

    public void setParty(String party) {
        this.party = party;
    }

    public String getAccessRestriction() {
        return accessRestriction;
    }

    public void setAccessRestriction(String accessRestriction) {
        this.accessRestriction = accessRestriction;
    }

    public String getAccessRestrictionReason() {
        return accessRestrictionReason;
    }

    public void setAccessRestrictionReason(String accessRestrictionReason) {
        this.accessRestrictionReason = accessRestrictionReason;
    }

    public Date getAccessRestrictionBeginDate() {
        return accessRestrictionBeginDate;
    }

    public void setAccessRestrictionBeginDate(Date accessRestrictionBeginDate) {
        this.accessRestrictionBeginDate = accessRestrictionBeginDate;
    }

    public Date getAccessRestrictionEndDate() {
        return accessRestrictionEndDate;
    }

    public void setAccessRestrictionEndDate(Date accessRestrictionEndDate) {
        this.accessRestrictionEndDate = accessRestrictionEndDate;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

}
