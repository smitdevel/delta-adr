package ee.webmedia.adr;

import ee.webmedia.adr.model.ContainerFile;
import ee.webmedia.adr.model.File;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ContainerFileHelper {

    public static final String DDOC_EXTENSION = ".ddoc";
    public static final String BDOC_EXTENSION = ".bdoc";
    public static final String ASICE_EXTENSION = ".asice";
    public static final String RESTRICTED_FILE_NAME = "Fail";

    public static List<ContainerFile> getContainerFiles(File file, List<String> filesNames) {
        JSONArray accessRestrictions = StringUtils.isNotBlank(file.getInnerAccessRestriction()) ? new JSONArray(file.getInnerAccessRestriction()) : new JSONArray();
        List<ContainerFile> containerFiles = new ArrayList<>();
        int i = 0;
        for (String containerFileName : filesNames) {
            JSONObject accessRestriction = !accessRestrictions.isNull(i) ? (JSONObject) accessRestrictions.get(i) : null;
            boolean isPublicAccessRestriction = accessRestriction == null || (accessRestriction.isNull("publicAccessRestriction") || (boolean) accessRestriction.get("publicAccessRestriction"));
            ContainerFile containerFile = new ContainerFile();
            containerFile.setFileName(isPublicAccessRestriction ? containerFileName : RESTRICTED_FILE_NAME);
            containerFile.setPublicAccessRestriction(isPublicAccessRestriction);
            containerFile.setAccessRestrictionText(accessRestriction == null || accessRestriction.isNull("accessRestriction") ? StringUtils.EMPTY : accessRestriction.getString("accessRestriction"));
            containerFiles.add(containerFile);
            i++;
        }
        return containerFiles;
    }

    public static boolean hasOnlyPublicAccessFiles(List<ContainerFile> containerFiles) {
        if (CollectionUtils.isNotEmpty(containerFiles)) {
            for (ContainerFile file : containerFiles) {
                if (!file.isPublicAccessRestriction()) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean isDigiDocFile(String fileName) {
        String lowerCase = StringUtils.defaultString(fileName).toLowerCase();
        return lowerCase.endsWith(DDOC_EXTENSION) || lowerCase.endsWith(BDOC_EXTENSION)
                || lowerCase.endsWith(ASICE_EXTENSION);
    }

}

