package ee.webmedia.adr.model;

public class ContainerFile {

    private String fileName;
    private String accessRestrictionText;
    private boolean publicAccessRestriction;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getAccessRestrictionText() {
        return accessRestrictionText;
    }

    public void setAccessRestrictionText(String accessRestrictionText) {
        this.accessRestrictionText = accessRestrictionText;
    }

    public boolean isPublicAccessRestriction() {
        return publicAccessRestriction;
    }

    public void setPublicAccessRestriction(boolean publicAccessRestriction) {
        this.publicAccessRestriction = publicAccessRestriction;
    }
}
