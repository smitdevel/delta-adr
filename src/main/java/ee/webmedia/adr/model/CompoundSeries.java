package ee.webmedia.adr.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CompoundSeries {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column compound_series.id
     *
     * @mbggenerated
     */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column compound_series.compound_function_id
     *
     * @mbggenerated
     */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer compoundFunctionId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column compound_series.mark
     *
     * @mbggenerated
     */
    private String mark;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column compound_series.title
     *
     * @mbggenerated
     */
    private String title;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column compound_series.order_num
     *
     * @mbggenerated
     */
    private Integer orderNum;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column compound_series.created_date_time
     *
     * @mbggenerated
     */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Date createdDateTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column compound_series.modified_date_time
     *
     * @mbggenerated
     */
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Date modifiedDateTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column compound_series.id
     *
     * @return the value of compound_series.id
     *
     * @mbggenerated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column compound_series.id
     *
     * @param id the value for compound_series.id
     *
     * @mbggenerated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column compound_series.compound_function_id
     *
     * @return the value of compound_series.compound_function_id
     *
     * @mbggenerated
     */
    public Integer getCompoundFunctionId() {
        return compoundFunctionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column compound_series.compound_function_id
     *
     * @param compoundFunctionId the value for compound_series.compound_function_id
     *
     * @mbggenerated
     */
    public void setCompoundFunctionId(Integer compoundFunctionId) {
        this.compoundFunctionId = compoundFunctionId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column compound_series.mark
     *
     * @return the value of compound_series.mark
     *
     * @mbggenerated
     */
    public String getMark() {
        return mark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column compound_series.mark
     *
     * @param mark the value for compound_series.mark
     *
     * @mbggenerated
     */
    public void setMark(String mark) {
        this.mark = mark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column compound_series.title
     *
     * @return the value of compound_series.title
     *
     * @mbggenerated
     */
    public String getTitle() {
        return title;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column compound_series.title
     *
     * @param title the value for compound_series.title
     *
     * @mbggenerated
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column compound_series.order_num
     *
     * @return the value of compound_series.order_num
     *
     * @mbggenerated
     */
    public Integer getOrderNum() {
        return orderNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column compound_series.order_num
     *
     * @param orderNum the value for compound_series.order_num
     *
     * @mbggenerated
     */
    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column compound_series.created_date_time
     *
     * @return the value of compound_series.created_date_time
     *
     * @mbggenerated
     */
    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column compound_series.created_date_time
     *
     * @param createdDateTime the value for compound_series.created_date_time
     *
     * @mbggenerated
     */
    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column compound_series.modified_date_time
     *
     * @return the value of compound_series.modified_date_time
     *
     * @mbggenerated
     */
    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column compound_series.modified_date_time
     *
     * @param modifiedDateTime the value for compound_series.modified_date_time
     *
     * @mbggenerated
     */
    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }
}