package ee.webmedia.adr;

import ee.webmedia.adr.model.Document;

public class DocumentWithType extends Document {

    private String typeTitle;

    public String getTypeTitle() {
        return typeTitle;
    }

    public void setTypeTitle(String typeTitle) {
        this.typeTitle = typeTitle;
    }

}
