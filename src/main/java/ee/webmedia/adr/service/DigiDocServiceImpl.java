package ee.webmedia.adr.service;

import ee.webmedia.adr.model.File;
import org.digidoc4j.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;

@Service("digiDocService")
public class DigiDocServiceImpl implements DigiDocService {
    protected final Logger log = LoggerFactory.getLogger(getClass());

	@Value("#{config['dir.root']}")
    private String filePath;
	private Configuration configuration = new Configuration(Configuration.Mode.PROD);

	@Override
	public List<String> getFilesNamesFromContainer(File file, int organizationId){
		List<DataFile> dataFiles = getDataFiles(file, organizationId);
		if(dataFiles == null){
			return Collections.emptyList();
		}
        return getFilesNames(dataFiles);
    }
	
	@Override
	public List<String> getFilesNames(List<DataFile> dataFiles){
		List<String> fileNames = new ArrayList<String>();
		for(DataFile dataFile : dataFiles){
			fileNames.add(dataFile.getName());
		}
		
		return fileNames;
	}
	
	@Override
    public List<DataFile> getDataFiles(File file, int organizationId){
        Container signedContainer = null;
        try {
        	java.io.File organizationDir = new java.io.File(filePath, Integer.toString(organizationId));
            java.io.File fsFile = new java.io.File(organizationDir, Integer.toString(file.getId()));
            FileInputStream in = new FileInputStream(fsFile);
            signedContainer = getContainerFromStream(in, null);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        } 	
        return signedContainer.getDataFiles();
    }
	
	private Container getContainerFromStream(InputStream contentInputStream, String ext) throws IOException {
        try {
            if (contentInputStream != null) {
                Container container = null;
                return container = ContainerBuilder.
                    aContainer().
                    fromStream(contentInputStream).
                    withConfiguration(configuration).
                    build();
            }
            throw new Exception("contentInputStream is empty.");
        } catch (Exception e) {
            e.printStackTrace();
            throw new IOException("Failed to build container!");
        }
    }

}
