package ee.webmedia.adr.service;

import static org.apache.commons.lang.StringUtils.stripToEmpty;

import ee.webmedia.adr.*;
import ee.webmedia.adr.dao.*;
import ee.webmedia.adr.model.File;
import ee.webmedia.adr.model.*;
import ee.webmedia.adr.ws.*;
import org.apache.commons.lang.*;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.Map.Entry;

@Service("adrImportService")
public class AdrImportServiceImpl implements AdrImportService, InitializingBean {
    protected final Logger log = LoggerFactory.getLogger(getClass());
    
    private static final String RUN_ON_WEEKEND_ALLOWED = "true";

    @Autowired
    private AvalikDokumendiRegister adrWebService;
    @Autowired
    private DocumentMapper documentMapper;
    @Autowired
    private FileMapper fileMapper;
    @Autowired
    private DocumentTypeMapper documentTypeMapper;
    @Autowired
    private CompoundFunctionMapper compoundFunctionMapper;
    @Autowired
    private FunctionMapper functionMapper;
    @Autowired
    private CompoundSeriesMapper compoundSeriesMapper;
    @Autowired
    private SeriesMapper seriesMapper;
    @Autowired
    private VolumeMapper volumeMapper;
    @Autowired
    private DocumentAssociationMapper documentAssociationMapper;
    @Autowired
    private ImportMapper importMapper;
    @Autowired
    private GeneralMapper generalMapper;
    @Value("#{config['dir.root']}")
    private String dataPath;
    @Value("#{config['dir.log']}")
    private String logPath;
    @Value("#{config['import.limit']}")
    private int limit;
    @Value("#{config['import.weekend']}")
    private String runOnWeekend;
    @Value("#{config['import.delay']}")
    private int delay;
    @Autowired
    private AdrController adrController;

    private DatatypeFactory dataTypeFactory;

    @Override
    public void afterPropertiesSet() throws Exception {
        dataTypeFactory = DatatypeFactory.newInstance();
        log.info("Data folder: " + dataPath);
        java.io.File dataDir = new java.io.File(dataPath);
        if (!dataDir.exists()) {
            log.info("Data folder does not exist, creating: " + dataDir);
            if (!dataDir.mkdirs()) {
                throw new RuntimeException("Error creating data folder: " + dataDir);
            }
        }
    }

    private static interface InvokeCallback<T> {
        T invoke();
    }

    @Override
    public synchronized void importDocumentsSinceLastImportAllOrganizations() {
        log.info("importDocumentsSinceLastImportAllOrganizations");
        Date now = new Date();
        for (Entry<Integer, Properties> orgProps : adrController.getOrganizationConfigs().entrySet()) {
            importDocumentsSinceLastImport(orgProps.getKey(), now);
        }
    }
    
    @Override
    public synchronized void importDocumentsSinceLastImportOrganizations(List<Integer> organizationIds) {
        log.info("importDocumentsSinceLastImportOrganizations");
        Date now = new Date();
        for (Integer orgId : organizationIds) {
            importDocumentsSinceLastImport(orgId);
        }
    }

    @Override
    public synchronized void importDocumentsSinceLastImport(int organizationId) {
    	log.info("importDocumentsSinceLastImport for organization id = " + organizationId);
    	Properties conf = adrController.getOrganizationConfigs().get(organizationId);
        if (conf == null) {
            throw new RuntimeException("Couldn't find config for organizationId=" + organizationId);
        }
    	// check if run on weekend is allowed
    	if (isWeekend()) {

            final String orgRunOnWeekend = conf.getProperty("import.weekend");
            if ((StringUtils.isEmpty(orgRunOnWeekend) &&  !RUN_ON_WEEKEND_ALLOWED.equals(runOnWeekend)) 
            		|| !RUN_ON_WEEKEND_ALLOWED.equals(orgRunOnWeekend)) {
            	
            	log.info("Import on weekend is not allowed for organization with id: " + organizationId);
            	return;
            }
    	}
    	
    	String delayStr = conf.getProperty("import.delay");
    	int	orgImportDelay = (StringUtils.isNotBlank(delayStr)) ? Integer.parseInt(delayStr) : delay;

        Date now = getImportDate(orgImportDelay);

        importDocumentsSinceLastImport(organizationId, now);
    }

    private Date getImportDate(int delay) {
    	Calendar cal = Calendar.getInstance();
    	cal.add(Calendar.DATE, delay);
    	return cal.getTime();
    }

    private void importDocumentsSinceLastImport(int organizationId, Date now) {
        log.info("importDocumentsSinceLastImport organizationId=" + organizationId);

        Date begin;
        List<ImportInterval> intervals = generalMapper.getImportIntervals(organizationId);
        if (intervals.isEmpty()) {
            begin = now;
        } else {
            Date globalEndDate = intervals.get(0).getEndDate();
            Date globalEndDateAtIterationStart;
            do {
                globalEndDateAtIterationStart = globalEndDate;
                for (Iterator<ImportInterval> i = intervals.iterator(); i.hasNext();) {
                    ImportInterval interval = i.next();
                    if (!interval.getEndDate().after(globalEndDate)) {
                        i.remove();
                        continue;
                    }
                    // If interval.getBeginDate() is same or earlier than globalEndDate + 1
                    if (!DateUtils.addDays(interval.getBeginDate(), -1).after(globalEndDate)) {
                        i.remove();
                        globalEndDate = interval.getEndDate();
                    }
                }
            } while (!intervals.isEmpty() && globalEndDate.after(globalEndDateAtIterationStart));
            begin = DateUtils.addDays(globalEndDate, 1);
        }
        importDocuments(organizationId, begin, now, now, true);
    }

    @Override
    public synchronized void importDocuments(int organizationId, Date begin, Date end) {
        Date now = new Date();
        importDocuments(organizationId, begin, end, now, false);
    }

    private void importDocuments(int organizationId, Date begin, Date end, Date now, boolean automatic) {
        if (begin == null || end == null || begin.after(end)) {
            throw new RuntimeException("Invalid dates: begin = " + begin + ", end = " + end);
        }

        Properties conf = adrController.getOrganizationConfigs().get(organizationId);
        if (conf == null) {
            throw new RuntimeException("Couldn't find config for organizationId=" + organizationId);
        }
        final String endpointUrl = conf.getProperty("import.url");
        if (StringUtils.isBlank(endpointUrl)) {
            log.info("Organization endpoint URL is blank, skipping ADR service invocation and import for organizationId=" + organizationId);
            return;
        }
        // Set endpoint URL for current organization. ADR service must support BindingProvider functionality
        ((BindingProvider) adrWebService).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

        log.info("importDocuments, organizationId=" + organizationId + ", begin=" + begin + ", end=" + end);
        final XMLGregorianCalendar beginDate = convert(begin);
        final XMLGregorianCalendar endDate = convert(end);

        Import dbImport = new Import();
        dbImport.setOrganizationId(organizationId);
        dbImport.setQueryBeginDate(begin);
        dbImport.setQueryEndDate(end);
        dbImport.setExecutionDateTime(now);
        dbImport.setType(automatic ? "AUTOMATIC" : "MANUAL");
        dbImport.setDocumentsCreated(-1);
        dbImport.setDocumentsUpdated(-1);
        dbImport.setDocumentsDeleted(-1);
        dbImport.setDocumentsError(0);

        try {
            int countSuccessCreated = 0;
            int countSuccessUpdated = 0;
            int countError = 0;
            int skip = 0;
            do {
                final int finalSkip = skip;
                log.info("Executing SOAP request koikDokumendidLisatudMuudetudV2 with arguments:\n  perioodiAlgusKuupaev=" + beginDate.toString() + "\n  perioodiLoppKuupaev="
                        + endDate.toString() + "\n  jataAlgusestVahele=" + finalSkip + "\n  tulemustePiirang=" + limit);
                List<DokumentDetailidegaV2> wsDocuments = invokeWithRedirectedOut(new InvokeCallback<List<DokumentDetailidegaV2>>() {
                    @Override
                    public List<DokumentDetailidegaV2> invoke() {
                        return adrWebService.koikDokumendidLisatudMuudetudV2(beginDate, endDate, finalSkip, limit);
                    }
                }, now, "0-koikDokumendidLisatudMuudetudV2-" + finalSkip);
                log.info("Response contains " + wsDocuments.size() + " documents, starting import");
                int documentNumber = 1;
                for (DokumentDetailidegaV2 wsDocument : wsDocuments) {
                    // TODO each document in separate transaction
                    try {
                        int totalDocumentNumber = countSuccessCreated + countSuccessUpdated + countError + 1;
                        String totalDocumentNumberPadded = StringUtils.leftPad(Integer.toString(totalDocumentNumber), Integer.toString(wsDocuments.size()).length(), '0');
                        log.info("Importing document " + documentNumber + " of " + wsDocuments.size() + " (total " + totalDocumentNumber + "), wsDocument documentId="
                                + wsDocument.getId());
                        boolean created = importDocument(organizationId, wsDocument, now, totalDocumentNumberPadded);
                        if (created) {
                            countSuccessCreated++;
                        } else {
                            countSuccessUpdated++;
                        }
                    } catch (Exception e) {
                        countError++;
                        log.error("Error importing document: " + e.getMessage() + "\n  wsDocument=" + toString(wsDocument), e);
                    }
                    documentNumber++;
                }
                if (wsDocuments.size() < limit) {
                    break;
                }
                skip += wsDocuments.size();
            } while (true);
            dbImport.setDocumentsCreated(countSuccessCreated);
            dbImport.setDocumentsUpdated(countSuccessUpdated);
            dbImport.setDocumentsError(dbImport.getDocumentsError() + countError);
            log.info("Completed documents import, out of " + (countSuccessCreated + countSuccessUpdated + countError) + " documents " + (countSuccessCreated + countSuccessUpdated)
                    + " were successfully imported (" + countSuccessCreated + " created, " + countSuccessUpdated + " updated), " + countError + " had errors");
        } catch (Exception e) {
            log.error("Error executing SOAP request koikDokumendidLisatudMuudetudV2", e);
        }

        try {
            int countSuccess = 0;
            int countError = 0;
            int skip = 0;
            do {
                final int finalSkip = skip;
                log.info("Executing SOAP request koikDokumendidKustutatudV2 with arguments:\n  perioodiAlgusKuupaev=" + beginDate.toString() + "\n  perioodiLoppKuupaev="
                        + endDate.toString() + "\n  jataAlgusestVahele=" + finalSkip + "\n  tulemustePiirang=" + limit);
                List<DokumentId> wsDeletedDocuments = invokeWithRedirectedOut(new InvokeCallback<List<DokumentId>>() {
                    @Override
                    public List<DokumentId> invoke() {
                        return adrWebService.koikDokumendidKustutatudV2(beginDate, endDate, finalSkip, limit);
                    }
                }, now, "0-koikDokumendidKustutatudV2-" + finalSkip);
                log.info("Response contains " + wsDeletedDocuments.size() + " documents, starting delete");
                int documentNumber = 1;
                for (DokumentId wsDeletedDocument : wsDeletedDocuments) {
                    // TODO each document in separate transaction
                    try {
                        log.info("Deleting document " + documentNumber + " of " + wsDeletedDocuments.size() + " (total " + (countSuccess + countError + 1) + ")");
                        deleteDocument(organizationId, wsDeletedDocument);
                        countSuccess++;
                    } catch (Exception e) {
                        countError++;
                        log.error("Error deleting document: " + e.getMessage() + "\n  wsDeletedDocument=" + toString(wsDeletedDocument), e);
                    }
                    documentNumber++;
                }
                if (wsDeletedDocuments.size() < limit) {
                    break;
                }
                skip += wsDeletedDocuments.size();
            } while (true);
            dbImport.setDocumentsDeleted(countSuccess);
            dbImport.setDocumentsError(dbImport.getDocumentsError() + countError);
            log.info("Completed documents delete, out of " + (countSuccess + countError) + " documents " + countSuccess + " were successfully deleted, " + countError
                    + " had errors");
        } catch (Exception e) {
            log.error("Error executing SOAP request koikDokumendidKustutatudV2", e);
        }

        // TODO it is sufficient to call this only once after all organization syncs have completed; refactor it upwards
        try {
            adrController.loadDocumentTypes();
        } catch (Exception e) {
            log.error("Error reloading document types", e);
        }

        int importId = generalMapper.defaultSeqNextVal();
        dbImport.setId(importId);
        importMapper.insert(dbImport);
    }

    public boolean importDocument(int organizationId, DokumentDetailidegaV2 wsDocument, Date now, String documentNumberPadded) throws Exception {
        Properties conf = adrController.getOrganizationConfigs().get(organizationId);
        if (conf == null) {
            throw new RuntimeException("Couldn't find config for organizationId=" + organizationId);
        }
        final String endpointUrl = conf.getProperty("import.url");
        if (StringUtils.isBlank(endpointUrl)) {
            log.info("Organization endpoint URL is blank, skipping ADR service invocation and import for organizationId=" + organizationId);
            return false;
        }
        // Set endpoint URL for current organization. ADR service must support BindingProvider functionality
        ((BindingProvider) adrWebService).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

        log.info("importDocuments, organizationId=" + organizationId);
        Document dbDocument = getDocument(organizationId, wsDocument.getId());
        if (dbDocument == null) {
            createDocument(organizationId, wsDocument, now, documentNumberPadded);
            return true;
        }
        updateDocument(organizationId, dbDocument, wsDocument, now, documentNumberPadded);
        return false;
    }

    private void updateDocument(int organizationId, Document dbDocument, DokumentDetailidegaV2 wsDocument, Date now, String documentNumberPadded) throws Exception {
        int documentId = dbDocument.getId();
        dbDocument.setModifiedDateTime(now);

        Date wsDocumentRegDateTime = wsDocument.getRegistreerimiseAeg().toGregorianCalendar().getTime();
        if (!ObjectUtils.equals(dbDocument.getRegDateTime(), wsDocumentRegDateTime)
                || !ObjectUtils.equals(dbDocument.getRegNumber(), wsDocument.getViit())) {
            log.warn("Document regDateTime or regNumber changed, this is not supposed to happen normally"
                    + "\n  dbDocument=" + toString(dbDocument)
                    + "\n  wsDocument=" + toString(wsDocument));
            dbDocument.setRegDateTime(wsDocumentRegDateTime);
            dbDocument.setRegNumber(wsDocument.getViit());
        }

        DocumentType dbDocumentType = addOrUpdateDocumentType(organizationId, wsDocument, now);
        Integer oldDocumentTypeId = null;
        if (!ObjectUtils.equals(dbDocument.getTypeId(), dbDocumentType.getId())) {
            oldDocumentTypeId = dbDocument.getTypeId();
            dbDocument.setTypeId(dbDocumentType.getId());
        }

        Volume dbVolume = addOrUpdateFunctionSeriesVolume(organizationId, wsDocument, now);
        Integer oldVolumeId = null;
        if (!ObjectUtils.equals(dbDocument.getVolumeId(), dbVolume.getId())) {
            oldVolumeId = dbDocument.getVolumeId();
            dbDocument.setVolumeId(dbVolume.getId());
        }

        setDocumentProperties(dbDocument, wsDocument);

        log.debug("Updating dbDocument documentId=" + dbDocument.getId() + ", wsDocument documentId=" + wsDocument.getId());
        documentMapper.updateByPrimaryKey(dbDocument);

        if (oldDocumentTypeId != null) {
            deleteDocumentTypeIfNecessary(oldDocumentTypeId);
        }
        if (oldVolumeId != null) {
            deleteVolumeAndUpwardsIfNecessary(oldVolumeId);
        }

        List<DocumentAssociationWithData> dbAssocs = generalMapper.getAssociations(documentId);
        try {
            for (SeotudDokument wsAssoc : wsDocument.getSeotudDokument()) {
                DocumentAssociation newAssoc = buildAssociation(organizationId, documentId, wsAssoc, now);
                if (newAssoc == null) {
                    continue;
                }
                // Search for newAssoc in dbAssocs
                boolean found = false;
                for (Iterator<DocumentAssociationWithData> i = dbAssocs.iterator(); i.hasNext();) {
                    DocumentAssociationWithData dbAssoc = i.next();
                    if (ObjectUtils.equals(dbAssoc.getType(), newAssoc.getType())
                            && ObjectUtils.equals(dbAssoc.getSourceDocumentId(), newAssoc.getSourceDocumentId())
                            && ObjectUtils.equals(dbAssoc.getTargetDocumentId(), newAssoc.getTargetDocumentId())) {
                        log.debug("Found association, keeping\n  dbAssoc=" + toString(dbAssoc) + "\n  newAssoc=" + toString(newAssoc) + "\n  wsAssoc=" + toString(wsAssoc));
                        found = true;
                        i.remove();
                    }
                }
                if (!found) {
                    createAssociation(newAssoc, wsAssoc);
                }
            }
            for (DocumentAssociationWithData dbAssoc : dbAssocs) {
                deleteAssociation(dbAssoc);
            }
        } catch (Exception e) {
            // TODO XXX FIXME This is only a temporary solution. Must fix real problem...
            log.error("Error updating associations for document: " + e.getMessage() + "\n  wsDocument=" + toString(wsDocument) + "\n  dbAssocs=" + toStringEach(dbAssocs)
                    + "\n  wsAssocs=" + toStringEach(wsDocument.getSeotudDokument()), e);
        }

        List<File> dbFiles = getFiles(documentId);
        int fileNumber = 0;
        for (FailV2 wsFile : wsDocument.getFail()) {
            fileNumber++;
            String fileNumberPadded = StringUtils.leftPad(Integer.toString(fileNumber), Integer.toString(wsDocument.getFail().size()).length(), '0');

            boolean found = false;
            for (Iterator<File> i = dbFiles.iterator(); i.hasNext();) {
                File dbFile = i.next();
                if (ObjectUtils.equals(dbFile.getNoderef(), wsFile.getId())) {
                    found = true;
                    i.remove();

                    boolean success = setFilePropertiesAndContents(dbFile, wsDocument, wsFile, organizationId, now, documentNumberPadded, fileNumberPadded, false);
                    if (success) {
                        log.debug("Updating dbFile fileId=" + dbFile.getId() + ", wsFile fileId=" + wsFile.getId());
                        fileMapper.updateByPrimaryKey(dbFile);
                    } else {
                        log.error("Error updating file, deleting\n  dbFile=" + dbFile + "\n  wsFile=" + wsFile);
                        deleteFile(organizationId, dbFile);
                    }

                    // It shouldn't be possible that two files have the same name under one document (DB constraint)
                    // but just in case, we don't break here
                }
            }
            if (!found) {
                createFile(organizationId, documentId, wsDocument, wsFile, now, documentNumberPadded, fileNumberPadded);
            }
        }
        for (File dbFile : dbFiles) {
            deleteFile(organizationId, dbFile);
        }
    }

    private void createFile(int organizationId, int documentId, DokumentDetailidegaV2 wsDocument, FailV2 wsFile, Date now, String documentNumberPadded,
            String fileNumberPadded) throws IOException, FileNotFoundException {

        File dbFile = new File();
        dbFile.setNoderef(wsFile.getId());
        dbFile.setCreatedDateTime(now);
        dbFile.setDocumentId(documentId);
        dbFile.setName(wsFile.getFailinimi());
        boolean success = setFilePropertiesAndContents(dbFile, wsDocument, wsFile, organizationId, now, documentNumberPadded, fileNumberPadded, true);
        if (success) {
            log.debug("Creating dbFile fileId=" + dbFile.getId() + ", wsFile fileId=" + wsFile.getId());
            fileMapper.insert(dbFile);
        }
    }

    private void createDocument(int organizationId, DokumentDetailidegaV2 wsDocument, Date now, String documentNumberPadded) throws Exception {
        Document dbDocument = new Document();
        dbDocument.setModifiedDateTime(now);
        dbDocument.setCreatedDateTime(now);
        dbDocument.setOrganizationId(organizationId);
        dbDocument.setNoderef(wsDocument.getId());
        dbDocument.setRegDateTime(wsDocument.getRegistreerimiseAeg().toGregorianCalendar().getTime());
        dbDocument.setRegNumber(wsDocument.getViit());

        // common - required in ws
        DocumentType dbDocumentType = addOrUpdateDocumentType(organizationId, wsDocument, now);
        dbDocument.setTypeId(dbDocumentType.getId());

        Volume dbVolume = addOrUpdateFunctionSeriesVolume(organizationId, wsDocument, now);
        dbDocument.setVolumeId(dbVolume.getId());

        setDocumentProperties(dbDocument, wsDocument);

        int documentId = generalMapper.defaultSeqNextVal();
        dbDocument.setId(documentId);
        log.debug("Creating dbDocument documentId=" + documentId + ", wsDocument documentId=" + wsDocument.getId());
        documentMapper.insert(dbDocument);

        for (SeotudDokument wsAssoc : wsDocument.getSeotudDokument()) {
            DocumentAssociation dbAssoc = buildAssociation(organizationId, documentId, wsAssoc, now);
            if (dbAssoc == null) {
                continue;
            }

            List<DocumentAssociation> existingAssocs = new ArrayList<DocumentAssociation>();
            existingAssocs.addAll(getAssociations(dbAssoc.getSourceDocumentId(), dbAssoc.getTargetDocumentId(), dbAssoc.getType()));
            existingAssocs.addAll(getAssociations(dbAssoc.getTargetDocumentId(), dbAssoc.getSourceDocumentId(), dbAssoc.getType()));
            if (existingAssocs.size() > 0) {
                log.warn("Skipping creating association for documentId=" + documentId + ", association to other document already exists, wsAssoc=" + toString(wsAssoc)
                        + "\n  existingAssocs=" + toStringEach(existingAssocs));
                continue;
            }

            createAssociation(dbAssoc, wsAssoc);
        }

        int fileNumber = 0;
        for (FailV2 wsFile : wsDocument.getFail()) {
            fileNumber++;
            String fileNumberPadded = StringUtils.leftPad(Integer.toString(fileNumber), Integer.toString(wsDocument.getFail().size()).length(), '0');

            createFile(organizationId, documentId, wsDocument, wsFile, now, documentNumberPadded, fileNumberPadded);
        }
    }
    
    public URI createDocument(int organizationId, Document dbDocument, HttpServletRequest request) throws URISyntaxException {
        dbDocument.setModifiedDateTime(new Date());
        dbDocument.setCreatedDateTime(new Date());
        dbDocument.setOrganizationId(organizationId);
        int documentId = generalMapper.defaultSeqNextVal();
        dbDocument.setId(documentId);
        documentMapper.insert(dbDocument);
        return createDocumentUri(dbDocument, request.getServerName());
    }
    
    private URI createDocumentUri(Document document, String base) throws URISyntaxException {
    	return new URI(createDocumentUriString(document, base));
    }
    
	public String createDocumentUriString(Document document, String base) {
		return String.format("https://%s/%s/dokument/%s", base, adrController.getOrganizationName(document.getOrganizationId()), document.getId());
	}

    private boolean setFilePropertiesAndContents(File dbFile, DokumentDetailidegaV2 wsDocument, FailV2 wsFile, int organizationId, Date now, String documentNumberPadded,
            String fileNumberPadded, boolean create) throws IOException, FileNotFoundException {
        dbFile.setModifiedDateTime(now);
        final OtsiFailSisugaV2 failSisugaRequest = new OtsiFailSisugaV2();
        failSisugaRequest.setDokumentId(wsDocument.getId());
        failSisugaRequest.setFailinimi(wsFile.getFailinimi());
        OtsiFailSisugaV2Response failSisugaResponse;
        try {
            failSisugaResponse = invokeWithRedirectedOut(new InvokeCallback<OtsiFailSisugaV2Response>() {
                @Override
                public OtsiFailSisugaV2Response invoke() {
                    return adrWebService.failSisugaV2(failSisugaRequest);
                }
            }, now, "" + documentNumberPadded + "-" + fileNumberPadded + "-failSisugaV2");
        } catch (Exception e) {
            log.error("Executing SOAP request failSisugaV2"
                    + "\n  failSisugaRequest=" + toString(failSisugaRequest)
                    + "\n  wsDocument=" + toString(wsDocument), e);
            return false;
        }
        if (failSisugaResponse.getFail() == null || failSisugaResponse.getFail().getSisu() == null
                && !BooleanUtils.isTrue(failSisugaResponse.getFail().isPrivateAccess())) {
            log.error("Error importing file, failSisuga response is empty"
                    + "\n  failSisugaRequest=" + toString(failSisugaRequest)
                    + "\n  failSisugaResponse=" + toString(failSisugaResponse)
                    + "\n  wsDocument=" + toString(wsDocument));
            return false;
        }

        java.io.File organizationDir = new java.io.File(dataPath, Integer.toString(organizationId));
        if (!organizationDir.exists()) {
            boolean result = organizationDir.mkdir();
            if (!result) {
                throw new RuntimeException("Organization directory creation failed: " + organizationDir);
            }
        }

        if (create) {
            int fileId = generalMapper.defaultSeqNextVal();
            dbFile.setId(fileId);
        }

        java.io.File file = new java.io.File(organizationDir, Integer.toString(dbFile.getId()));
        if (file.exists()) {
            if (create) {
                throw new RuntimeException("File exists: " + file);
            }
            if (!file.delete()) {
                log.error("Error deleting file: " + file);
                return false;
            }
        }
        // Re-set dbFile properties, since Delta might return PDF instead of previously requested document
        FailV2 responseFile = failSisugaResponse.getFail();
        dbFile.setName(responseFile.getFailinimi());
        dbFile.setAccessRestriction(responseFile.getAccessRestriction());
        String jsonObject = responseFile.getInnerAccessRestriction() != null ? new JSONArray(responseFile.getInnerAccessRestriction()).toString() : null;
        dbFile.setInnerAccessRestriction(jsonObject);
        dbFile.setPublicAccess(BooleanUtils.isNotTrue(responseFile.isPrivateAccess()));
        dbFile.setTitle(responseFile.getPealkiri());
        dbFile.setSize(responseFile.getSuurus());
        dbFile.setMimeType(responseFile.getMimeType());
        dbFile.setEncoding(responseFile.getEncoding());
        dbFile.setFileModifiedDateTime(convert(responseFile.getMuutmiseAeg()));
        InputStream in = responseFile.getSisu() != null ? responseFile.getSisu().getInputStream() : null;
        if (in != null) {
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
            FileCopyUtils.copy(in, out); // both streams are closed
        }
        return true;
    }

    private int createAssociation(DocumentAssociation dbAssoc, SeotudDokument wsAssoc) {
        int associationId = generalMapper.defaultSeqNextVal();
        dbAssoc.setId(associationId);
        log.debug("Creating association\n  dbAssoc=" + toString(dbAssoc) + "\n  wsAssoc=" + toString(wsAssoc));
        documentAssociationMapper.insert(dbAssoc);
        return associationId;
    }

    private DocumentAssociation buildAssociation(int organizationId, int documentId, SeotudDokument wsAssoc, Date now) {
        Document dbOtherDocument = getDocument(organizationId, wsAssoc.getId());
        if (dbOtherDocument == null) {
            log.debug("Skipping creating association for documentId=" + documentId + ", other document does not yet exist, wsAssoc=" + toString(wsAssoc));
            return null;
        }
        DocumentAssociation dbAssoc = new DocumentAssociation();
        dbAssoc.setCreatedDateTime(now);
        dbAssoc.setSourceDocumentId(documentId);
        dbAssoc.setTargetDocumentId(dbOtherDocument.getId());
        String assocType = wsAssoc.getSeosLiik();
        if ("tavaline".equals(assocType)) {
            dbAssoc.setType("DEFAULT");

        } else if ("vastusdokument".equals(assocType)) {
            dbAssoc.setType("REPLY");

        } else if ("alusdokument (vastusdokument)".equals(assocType)) {
            dbAssoc.setType("REPLY");
            // Switch source and target
            dbAssoc.setSourceDocumentId(dbOtherDocument.getId());
            dbAssoc.setTargetDocumentId(documentId);

        } else if ("järgdokument".equals(assocType)) {
            dbAssoc.setType("FOLLOWUP");

        } else if ("alusdokument (järgdokument)".equals(assocType)) {
            dbAssoc.setType("FOLLOWUP");
            // Switch source and target
            dbAssoc.setSourceDocumentId(dbOtherDocument.getId());
            dbAssoc.setTargetDocumentId(documentId);

        } else {
            log.error("Skipping creating association for documentId=" + documentId + ", seosLiik value is unknown, wsAssoc=" + toString(wsAssoc));
            return null;
        }
        return dbAssoc;
    }

    private void setDocumentProperties(Document dbDocument, DokumentDetailidegaV2 wsDocument) {
        String documentTypeId = wsDocument.getDokumendiLiik().getId();
        boolean isIncomingLetter = "incomingLetter".equals(documentTypeId);
        boolean isLetter = isIncomingLetter || "incomingLetterMv".equals(documentTypeId) || "outgoingLetter".equals(documentTypeId)
                || "outgoingLetterMv".equals(documentTypeId);
        boolean isContract = "contractSim".equals(documentTypeId) || "contractSmit".equals(documentTypeId) || "contractMv".equals(documentTypeId) // Delta 2 documentTypeIds
                || "contract".equals(documentTypeId); // Delta 3 documentTypeId

        // common - optional in ws
        dbDocument.setTitle(stripToEmpty(wsDocument.getPealkiri()));
        dbDocument.setAccessRestriction(wsDocument.getJuurdepaasuPiirang()); // this is mandatory in ws
        dbDocument.setAccessRestrictionReason(stripToEmpty(wsDocument.getJuurdepaasuPiiranguAlus()));
        dbDocument.setAccessRestrictionBeginDate(convert(wsDocument.getJuurdepaasuPiiranguKehtivuseAlgusKuupaev()));
        dbDocument.setAccessRestrictionEndDate(convert(wsDocument.getJuurdepaasuPiiranguKehtivuseLoppKuupaev()));
        dbDocument.setAccessRestrictionEndDesc(stripToEmpty(wsDocument.getJuurdepaasuPiiranguLopp()));
        dbDocument.setByRequestOnly(wsDocument.isAinultTeabenoudeKorras());
        dbDocument.setAccessRestrictionChangeReason(stripToEmpty(wsDocument.getJuurdepaasuPiiranguMuutmisePohjus()));

        dbDocument.setAnnex(isLetter ? stripToEmpty(wsDocument.getLisad()) : "");
        dbDocument.setSenderRegNumber(isLetter ? stripToEmpty(wsDocument.getSaatjaViit()) : "");
        dbDocument.setTransmittalMode(isLetter ? stripToEmpty(wsDocument.getSaatmisviis()) : "");
        dbDocument.setDueDate(isLetter || isContract ? convert(wsDocument.getTahtaeg()) : null);
        dbDocument.setComplianceDate(isLetter ? convert(wsDocument.getVastamiseKuupaev()) : null);
        dbDocument.setSendDate(!isIncomingLetter ? convert(wsDocument.getSaatmiseKuupaev()) : null);
        dbDocument.setCompilator(stripToEmpty(wsDocument.getKoostaja()));

        if (isLetter || isContract) {
            if (StringUtils.isNotEmpty(wsDocument.getSaatja()) && StringUtils.isNotEmpty(wsDocument.getOsapool())) {
                // This should never happen, according to DELTA ADR interface specification and implementation
                log.warn("Both saatja and osapool fields have non-empty value, wsDocument=" + toString(wsDocument));
                dbDocument.setParty(wsDocument.getSaatja() + ", " + wsDocument.getOsapool());
            } else if (StringUtils.isNotEmpty(wsDocument.getSaatja())) {
                dbDocument.setParty(wsDocument.getSaatja());
            } else if (StringUtils.isNotEmpty(wsDocument.getOsapool())) {
                dbDocument.setParty(wsDocument.getOsapool());
            } else {
                dbDocument.setParty("");
            }
        } else {
            dbDocument.setParty("");
        }
    }

    private Volume addOrUpdateFunctionSeriesVolume(int organizationId, DokumentDetailidegaV2 wsDocument, Date now) {
        Funktsioon wsFunction = wsDocument.getFunktsioon();
        Function dbFunction = generalMapper.getFunction(organizationId, wsFunction.getId());
        boolean needToCheckCompoundFunction = false;
        CompoundFunction dbCompoundFunction = null;
        if (dbFunction == null) {
            dbCompoundFunction = generalMapper.getCompoundFunction(organizationId, wsFunction.getViit(), wsFunction.getPealkiri());
            if (dbCompoundFunction == null) {
                dbCompoundFunction = new CompoundFunction();
                dbCompoundFunction.setModifiedDateTime(now);
                dbCompoundFunction.setCreatedDateTime(now);
                dbCompoundFunction.setOrganizationId(organizationId);
                dbCompoundFunction.setMark(wsFunction.getViit());
                dbCompoundFunction.setTitle(wsFunction.getPealkiri());
                dbCompoundFunction.setOrderNum(wsFunction.getJarjekorraNumber());

                int compoundFunctionId = generalMapper.defaultSeqNextVal();
                dbCompoundFunction.setId(compoundFunctionId);
                log.debug("Creating dbCompoundFunction=" + toString(dbCompoundFunction));
                compoundFunctionMapper.insert(dbCompoundFunction);
            } else {
                needToCheckCompoundFunction = true;
            }

            dbFunction = new Function();
            dbFunction.setModifiedDateTime(now);
            dbFunction.setCreatedDateTime(now);
            dbFunction.setCompoundFunctionId(dbCompoundFunction.getId());
            dbFunction.setNoderef(wsFunction.getId());

            int functionId = generalMapper.defaultSeqNextVal();
            dbFunction.setId(functionId);
            log.debug("Creating dbFunction=" + toString(dbFunction));
            functionMapper.insert(dbFunction);
        } else {
            dbCompoundFunction = compoundFunctionMapper.selectByPrimaryKey(dbFunction.getCompoundFunctionId()); // result cannot be null
            needToCheckCompoundFunction = true;
        }
        if (needToCheckCompoundFunction) {
            // check if viit or pealkiri or jrknum has changed
            if (!ObjectUtils.equals(dbCompoundFunction.getMark(), wsFunction.getViit())
                    || !ObjectUtils.equals(dbCompoundFunction.getTitle(), wsFunction.getPealkiri())
                    || !ObjectUtils.equals(dbCompoundFunction.getOrderNum(), wsFunction.getJarjekorraNumber())) {

                dbCompoundFunction.setModifiedDateTime(now);
                dbCompoundFunction.setMark(wsFunction.getViit());
                dbCompoundFunction.setTitle(wsFunction.getPealkiri());
                dbCompoundFunction.setOrderNum(wsFunction.getJarjekorraNumber());
                log.debug("Updating dbCompoundFunction=" + toString(dbCompoundFunction));
                compoundFunctionMapper.updateByPrimaryKey(dbCompoundFunction);
            }
        }

        Sari wsSeries = wsDocument.getSari();
        Series dbSeries = generalMapper.getSeries(dbCompoundFunction.getId(), wsSeries.getId());
        boolean needToCheckCompoundSeries = false;
        CompoundSeries dbCompoundSeries = null;
        if (dbSeries == null) {
            dbCompoundSeries = generalMapper.getCompoundSeries(dbCompoundFunction.getId(), wsSeries.getViit(), wsSeries.getPealkiri());
            if (dbCompoundSeries == null) {
                dbCompoundSeries = new CompoundSeries();
                dbCompoundSeries.setModifiedDateTime(now);
                dbCompoundSeries.setCreatedDateTime(now);
                dbCompoundSeries.setCompoundFunctionId(dbCompoundFunction.getId());
                dbCompoundSeries.setMark(wsSeries.getViit());
                dbCompoundSeries.setTitle(wsSeries.getPealkiri());
                dbCompoundSeries.setOrderNum(wsSeries.getJarjekorraNumber());

                int compoundFunctionId = generalMapper.defaultSeqNextVal();
                dbCompoundSeries.setId(compoundFunctionId);
                log.debug("Creating dbCompoundSeries=" + toString(dbCompoundSeries));
                compoundSeriesMapper.insert(dbCompoundSeries);
            } else {
                needToCheckCompoundSeries = true;
            }

            dbSeries = new Series();
            dbSeries.setModifiedDateTime(now);
            dbSeries.setCreatedDateTime(now);
            dbSeries.setCompoundSeriesId(dbCompoundSeries.getId());
            dbSeries.setNoderef(wsSeries.getId());

            int functionId = generalMapper.defaultSeqNextVal();
            dbSeries.setId(functionId);
            log.debug("Creating dbSeries=" + toString(dbSeries));
            seriesMapper.insert(dbSeries);
        } else {
            dbCompoundSeries = compoundSeriesMapper.selectByPrimaryKey(dbSeries.getCompoundSeriesId()); // result cannot be null
            needToCheckCompoundSeries = true;
        }
        if (needToCheckCompoundSeries) {
            // check if viit or pealkiri or jrknum has changed
            if (!ObjectUtils.equals(dbCompoundSeries.getMark(), wsSeries.getViit())
                    || !ObjectUtils.equals(dbCompoundSeries.getTitle(), wsSeries.getPealkiri())
                    || !ObjectUtils.equals(dbCompoundSeries.getOrderNum(), wsSeries.getJarjekorraNumber())) {

                dbCompoundSeries.setModifiedDateTime(now);
                dbCompoundSeries.setMark(wsSeries.getViit());
                dbCompoundSeries.setTitle(wsSeries.getPealkiri());
                dbCompoundSeries.setOrderNum(wsSeries.getJarjekorraNumber());
                log.debug("Updating dbCompoundSeries=" + toString(dbCompoundSeries));
                compoundSeriesMapper.updateByPrimaryKey(dbCompoundSeries);
            }
        }

        Toimik wsVolume = wsDocument.getToimik();
        Volume dbVolume = generalMapper.getVolume(dbCompoundSeries.getId(), wsVolume.getId());
        Date wsVolumeValidFromDate = convert(wsVolume.getKehtivAlatesKuupaev());
        Date wsVolumeValidToDate = convert(wsVolume.getKehtivKuniKuupaev());
        if (dbVolume == null) {
            dbVolume = new Volume();
            dbVolume.setModifiedDateTime(now);
            dbVolume.setCreatedDateTime(now);
            dbVolume.setCompoundSeriesId(dbCompoundSeries.getId());
            dbVolume.setNoderef(wsVolume.getId());
            dbVolume.setMark(wsVolume.getViit());
            dbVolume.setTitle(wsVolume.getPealkiri());
            dbVolume.setValidFromDate(wsVolumeValidFromDate);
            dbVolume.setValidToDate(wsVolumeValidToDate);

            int volumeId = generalMapper.defaultSeqNextVal();
            dbVolume.setId(volumeId);
            log.debug("Creating dbVolume=" + toString(dbVolume));
            volumeMapper.insert(dbVolume);
        } else {
            // check if viit or pealkiri or dates have changed
            if (!ObjectUtils.equals(dbVolume.getMark(), wsVolume.getViit())
                    || !ObjectUtils.equals(dbVolume.getTitle(), wsVolume.getPealkiri())
                    || !ObjectUtils.equals(dbVolume.getValidFromDate(), wsVolumeValidFromDate)
                    || !ObjectUtils.equals(dbVolume.getValidToDate(), wsVolumeValidToDate)) {

                dbVolume.setModifiedDateTime(now);
                dbVolume.setMark(wsVolume.getViit());
                dbVolume.setTitle(wsVolume.getPealkiri());
                dbVolume.setValidFromDate(wsVolumeValidFromDate);
                dbVolume.setValidToDate(wsVolumeValidToDate);
                log.debug("Updating dbVolume=" + toString(dbVolume));
                volumeMapper.updateByPrimaryKey(dbVolume);
            }
        }
        return dbVolume;
    }

    private DocumentType addOrUpdateDocumentType(int organizationId, DokumentDetailidegaV2 wsDocument, Date now) {
        DokumendiliikV2 wsDocumentType = wsDocument.getDokumendiLiik();
        DocumentTypeExample documentTypeQuery = new DocumentTypeExample();
        documentTypeQuery.createCriteria().andOrganizationIdEqualTo(organizationId).andNameEqualTo(wsDocumentType.getId());
        List<DocumentType> dbDocumentTypes = documentTypeMapper.selectByExample(documentTypeQuery);
        DocumentType dbDocumentType = null;
        if (dbDocumentTypes.size() == 0) {
            dbDocumentType = new DocumentType();
            dbDocumentType.setCreatedDateTime(now);
            dbDocumentType.setModifiedDateTime(now);
            dbDocumentType.setOrganizationId(organizationId);
            dbDocumentType.setName(wsDocumentType.getId());
            dbDocumentType.setTitle(wsDocumentType.getNimi());

            int documentTypeId = generalMapper.defaultSeqNextVal();
            dbDocumentType.setId(documentTypeId);
            log.debug("Creating dbDocumentType=" + toString(dbDocumentType));
            documentTypeMapper.insert(dbDocumentType);
        } else if (dbDocumentTypes.size() == 1) {
            dbDocumentType = dbDocumentTypes.get(0);
            // Check if documentTypeTitle is changed
            if (!ObjectUtils.equals(dbDocumentType.getTitle(), wsDocumentType.getNimi())) {
                dbDocumentType.setModifiedDateTime(now);
                dbDocumentType.setTitle(wsDocumentType.getNimi());
                log.debug("Updating dbDocumentType=" + toString(dbDocumentType));
                documentTypeMapper.updateByPrimaryKey(dbDocumentType);
            }
        } else {
            // Cannot happen, DB has constraint UNIQUE (organization_id, name)
            throw new RuntimeException("More than one document type exists for '" + documentTypeQuery.getOredCriteria() + "': " + toStringEach(dbDocumentTypes));
        }
        return dbDocumentType;
    }

    private void deleteDocument(int organizationId, DokumentId wsDocument) {
        deleteDocument(organizationId, wsDocument.getId());
    }
    
    public boolean deleteDocument(int organizationId, String nodeRef) {
        Document dbDocument = getDocument(organizationId, nodeRef);
        if (dbDocument == null) {
            log.info("Failed to delete document: document not found\n  wsDeletedDocument=" + nodeRef);
            return false;
        }

        Integer documentId = dbDocument.getId();

        for (File dbFile : getFiles(documentId)) {
            deleteFile(organizationId, dbFile);
        }

        for (DocumentAssociationWithData dbAssociation : generalMapper.getAssociations(documentId)) {
            deleteAssociation(dbAssociation);
        }

        log.debug("Deleting document=" + toString(dbDocument));
        documentMapper.deleteByPrimaryKey(documentId);
        return true;
    }

    private void deleteFile(int organizationId, File dbFile) {
        log.debug("Deleting file=" + toString(dbFile));
        fileMapper.deleteByPrimaryKey(dbFile.getId());

        java.io.File organizationDir = new java.io.File(dataPath, Integer.toString(organizationId));
        java.io.File file = new java.io.File(organizationDir, dbFile.getId().toString());
        if (!file.delete()) {
            log.warn("Error deleting file: " + file);
        }
    }

    private void deleteAssociation(DocumentAssociationWithData dbAssociation) {
        log.debug("Deleting association=" + toString(dbAssociation));
        documentAssociationMapper.deleteByPrimaryKey(dbAssociation.getId());
    }

    private void deleteDocumentTypeIfNecessary(int documentTypeId) {
        if (generalMapper.getDocumentCountByType(documentTypeId) == 0) {
            DocumentType dbDocumentType = documentTypeMapper.selectByPrimaryKey(documentTypeId);
            log.debug("Deleting documentType=" + toString(dbDocumentType));
            documentTypeMapper.deleteByPrimaryKey(dbDocumentType.getId());
        }
    }

    private void deleteVolumeAndUpwardsIfNecessary(Integer volumeId) {
        if (generalMapper.getDocumentCountByVolume(volumeId) == 0) {
            Volume dbVolume = volumeMapper.selectByPrimaryKey(volumeId);
            log.debug("Deleting volume=" + toString(dbVolume));
            volumeMapper.deleteByPrimaryKey(dbVolume.getId());

            Integer compoundSeriesId = dbVolume.getCompoundSeriesId();
            if (generalMapper.getVolumeCount(compoundSeriesId) == 0) {
                CompoundSeries dbCompoundSeries = compoundSeriesMapper.selectByPrimaryKey(compoundSeriesId);

                SeriesExample seriesQuery = new SeriesExample();
                seriesQuery.createCriteria().andCompoundSeriesIdEqualTo(dbCompoundSeries.getId());
                for (Series dbSeries : seriesMapper.selectByExample(seriesQuery)) {
                    log.debug("Deleting series=" + toString(dbSeries));
                    seriesMapper.deleteByPrimaryKey(dbSeries.getId());
                }

                log.debug("Deleting compoundSeries=" + toString(dbCompoundSeries));
                compoundSeriesMapper.deleteByPrimaryKey(dbCompoundSeries.getId());

                Integer compoundFunctionId = dbCompoundSeries.getCompoundFunctionId();
                if (generalMapper.getCompoundSeriesCount(compoundFunctionId) == 0) {
                    CompoundFunction dbCompoundFunction = compoundFunctionMapper.selectByPrimaryKey(compoundFunctionId);

                    FunctionExample functionQuery = new FunctionExample();
                    functionQuery.createCriteria().andCompoundFunctionIdEqualTo(dbCompoundFunction.getId());
                    for (Function dbFunction : functionMapper.selectByExample(functionQuery)) {
                        log.debug("Deleting function=" + toString(dbFunction));
                        functionMapper.deleteByPrimaryKey(dbFunction.getId());
                    }

                    log.debug("Deleting compoundFunction=" + toString(dbCompoundFunction));
                    compoundFunctionMapper.deleteByPrimaryKey(dbCompoundFunction.getId());
                }
            }
        }
    }

    private List<DocumentAssociation> getAssociations(Integer sourceDocumentId, Integer targetDocumentId, String type) {
        DocumentAssociationExample assocQuery = new DocumentAssociationExample();
        assocQuery.createCriteria()
                .andSourceDocumentIdEqualTo(sourceDocumentId)
                .andTargetDocumentIdEqualTo(targetDocumentId)
                .andTypeEqualTo(type);
        return documentAssociationMapper.selectByExample(assocQuery);
    }

    public List<File> getFiles(Integer documentId) {
        FileExample fileQuery = new FileExample();
        fileQuery.createCriteria().andDocumentIdEqualTo(documentId);
        List<File> files = fileMapper.selectByExample(fileQuery);
        return files;
    }

    public Document getDocument(int organizationId, String noderef) {
        DocumentExample query = new DocumentExample();
        query.createCriteria()
                .andOrganizationIdEqualTo(organizationId)
                .andNoderefEqualTo(noderef);
        List<Document> documents = documentMapper.selectByExample(query);
        if (documents.size() == 0) {
            return null;
        } else if (documents.size() == 1) {
            return documents.get(0);
        }
        // Cannot happen, DB has constraint UNIQUE (organization_id, noderef)
        throw new RuntimeException("More than one document exists for '" + query.getOredCriteria() + "': " + toStringEach(documents));
    }
    
    public List<Document> getDocuments(int organizationId) {
        DocumentExample query = new DocumentExample();
        query.createCriteria()
                .andOrganizationIdEqualTo(organizationId);
        return documentMapper.selectByExample(query);
    }

    public static String toString(Object object) {
        // TODO also traverse child objects
        return StringUtils.replace(ToStringBuilder.reflectionToString(object, ToStringStyle.MULTI_LINE_STYLE), "\n", "\n  ");
    }

    public static String toStringEach(Collection<? extends Object> collection) {
        if (collection == null) {
            return null;
        }
        StringBuilder s = new StringBuilder("[");
        for (Object object : collection) {
            if (s.length() > 0) {
                s.append(", ");
            }
            s.append(StringUtils.replace(ToStringBuilder.reflectionToString(object, ToStringStyle.MULTI_LINE_STYLE), "\n", "\n  "));
        }
        s.append("]");
        return s.toString();
    }

    public XMLGregorianCalendar convert(Date date) {
        if (date == null) {
            return null;
        }
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setLenient(false);
        gregorianCalendar.setTime(date);
        XMLGregorianCalendar xmlGregorianCalendar = dataTypeFactory.newXMLGregorianCalendar(gregorianCalendar);
        if (log.isTraceEnabled()) {
            log.trace("Converted[1] " + DateFormatUtils.format(date, "yyyy-MM-dd'T'HH:mm:ss.SSSZZ") + " -> " + xmlGregorianCalendar.toString());
        }
        return xmlGregorianCalendar;
    }

    private Date convert(XMLGregorianCalendar xmlGregorianCalendar) {
        if (xmlGregorianCalendar == null) {
            return null;
        }
        Date date = xmlGregorianCalendar.toGregorianCalendar().getTime();
        if (log.isTraceEnabled()) {
            log.trace("Converted[2] " + xmlGregorianCalendar.toString() + " -> " + DateFormatUtils.format(date, "yyyy-MM-dd'T'HH:mm:ss.SSSZZ"));
        }
        return date;
    }

    private synchronized <T> T invokeWithRedirectedOut(InvokeCallback<T> callback, Date now, String logFileSuffix) throws Exception {
        if (log.isDebugEnabled()) {
            System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        }
        PrintStream originalOut = System.out;
        PrintStream newOut = null;
        try {
            if (log.isDebugEnabled()) {
                java.io.File dumpFile = new java.io.File(logPath, "adr-wsrequest-" + DateFormatUtils.format(now, "yyyy-MM-dd'T'HH-mm-ss.SSSZZ") + "-" + logFileSuffix + ".dump");
                newOut = new PrintStream(dumpFile);
                System.setOut(newOut);
            }
            return callback.invoke();
        } finally {
            if (log.isDebugEnabled()) {
                System.setOut(originalOut);
                try {
                    newOut.close();
                } catch (Exception e) {
                    log.error("Error closing file", e);
                }
            }
        }
    }
    
    private static boolean isWeekend() {
    	Calendar cal = Calendar.getInstance();
    	return 	cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
    }

}
