package ee.webmedia.adr.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import ee.webmedia.adr.model.Document;
import ee.webmedia.adr.model.File;
import ee.webmedia.adr.ws.DokumentDetailidegaV2;

public interface AdrImportService {

    void importDocumentsSinceLastImportAllOrganizations();
    
    void importDocumentsSinceLastImportOrganizations(List<Integer> organizationIds);

    void importDocumentsSinceLastImport(int organizationId);

    void importDocuments(int organizationId, Date begin, Date end);

    boolean importDocument(int organizationId, DokumentDetailidegaV2 wsDocument, Date now, String documentNumberPadded) throws Exception;
    
    Document getDocument(int organizationId, String noderef);
    
    boolean deleteDocument(int organizationId, String nodeRef);
    
    URI createDocument(int organizationId, Document dbDocument, HttpServletRequest request) throws URISyntaxException;

    List<Document> getDocuments(int organizationId);
    
    String createDocumentUriString(Document document, String base);
    
    List<File> getFiles(Integer documentId);
}
