package ee.webmedia.adr.service;

import java.util.List;

import org.digidoc4j.DataFile;

import ee.webmedia.adr.model.File;

public interface DigiDocService {
	
	List<String> getFilesNamesFromContainer(File file, int organizationId);
		
	List<String> getFilesNames(List<DataFile> dataFiles);
		
	List<DataFile> getDataFiles(File file, int organizationId);

}
