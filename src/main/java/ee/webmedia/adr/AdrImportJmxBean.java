package ee.webmedia.adr;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import ee.webmedia.adr.service.AdrImportService;

@Component
@ManagedResource
public class AdrImportJmxBean {
    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AdrImportService adrImportService;

    @ManagedOperation
    public void importDocumentsSinceLastImportAllOrganizations() throws Exception {
        try {
            adrImportService.importDocumentsSinceLastImportAllOrganizations();
        } catch (Exception e) {
            log.error("Error executing importDocumentsSinceLastImportAllOrganizations", e);
            throw e;
        }
    }

    @ManagedOperation
    public void importDocumentsSinceLastImport(String organizationIdString) throws Exception {
        try {
            int organizationId = Integer.parseInt(organizationIdString);
            adrImportService.importDocumentsSinceLastImport(organizationId);
        } catch (Exception e) {
            log.error("Error executing importDocumentsSinceLastImport", e);
            throw e;
        }
    }

    @ManagedOperation
    public void importDocuments(String organizationIdString, String beginString, String endString) throws Exception {
        try {
            int organizationId = Integer.parseInt(organizationIdString);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dateFormat.setLenient(false);
            Date begin = dateFormat.parse(beginString);
            if (begin == null) {
                throw new RuntimeException("Error parsing begin date: " + beginString);
            }
            Date end = dateFormat.parse(endString);
            if (end == null) {
                throw new RuntimeException("Error parsing end date: " + endString);
            }
            adrImportService.importDocuments(organizationId, begin, end);
        } catch (Exception e) {
            log.error("Error executing importDocuments", e);
            throw e;
        }
    }

}
