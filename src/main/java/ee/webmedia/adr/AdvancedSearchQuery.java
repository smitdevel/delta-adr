package ee.webmedia.adr;

import java.util.Date;

public class AdvancedSearchQuery {

    private final Date regDateBegin;
    private final Date regDateEnd;
    private final int organizationId;
    private final String documentTypesArray;
    private final String titleTsquery;
    private final String transmittalModesArray;
    private final String partyTsquery;
    private final String senderRegNumberEscapedLike;
    private final String accessRestriction;
    private final String accessRestrictionReasonTsquery;
    private final Date accessRestrictionBeginDate;
    private final Date accessRestrictionEndDate;

    private final int offset;
    private final int limit;

    public AdvancedSearchQuery(AdvancedSearchForm form, int organizationId, String documentTypesArray, String titleTsquery, String transtmittalModesArray, String partyTsquery,
            String senderRegNumberEscapedLike, String accessRestrictionReasonTsquery, int offset, int limit) {
        regDateBegin = form.getRegDateBegin();
        regDateEnd = form.getRegDateEnd();
        this.organizationId = organizationId;
        this.documentTypesArray = documentTypesArray;
        this.titleTsquery = titleTsquery;
        this.transmittalModesArray = transtmittalModesArray;
        this.partyTsquery = partyTsquery;
        this.senderRegNumberEscapedLike = senderRegNumberEscapedLike;
        accessRestriction = form.getAccessRestriction();
        this.accessRestrictionReasonTsquery = accessRestrictionReasonTsquery;
        accessRestrictionBeginDate = form.getAccessRestrictionBeginDate();
        accessRestrictionEndDate = form.getAccessRestrictionEndDate();
        this.offset = offset;
        this.limit = limit;
    }

    public Date getRegDateBegin() {
        return regDateBegin;
    }

    public Date getRegDateEnd() {
        return regDateEnd;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public String getDocumentTypesArray() {
        return documentTypesArray;
    }

    public String getTitleTsquery() {
        return titleTsquery;
    }

    public String getPartyTsquery() {
        return partyTsquery;
    }

    public String getSenderRegNumberEscapedLike() {
        return senderRegNumberEscapedLike;
    }

    public String getAccessRestriction() {
        return accessRestriction;
    }

    public String getAccessRestrictionReasonTsquery() {
        return accessRestrictionReasonTsquery;
    }

    public Date getAccessRestrictionBeginDate() {
        return accessRestrictionBeginDate;
    }

    public Date getAccessRestrictionEndDate() {
        return accessRestrictionEndDate;
    }

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }

    public String getTransmittalModesArray() {
        return transmittalModesArray;
    }

}
