package ee.webmedia.adr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import ee.webmedia.adr.service.AdrImportService;

@Component
public class AdrImportTaskScheduler {
    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AdrImportService adrImportService;

    @Autowired
    private ThreadPoolTaskScheduler taskScheduler;
    
    @Autowired
    private AdrController adrController;
    
    @Value("#{config['import.cron']}")
    private String cronExperession;

    

    @PostConstruct
    public void scheduleRunnableWithCronTrigger() {
    	
    	List<Integer> orgIdsWithCommonCronTrigger = new ArrayList<>();
    	
    	for (Entry<Integer, Properties> orgProps : adrController.getOrganizationConfigs().entrySet()) {
            Integer orgId = orgProps.getKey();
            
            Properties orgConfig = orgProps.getValue();
            
            if (orgConfig == null) {
                log.warn("No configuration properties found for organization with id = " + orgId);
            }
            
            String orgCronExperession = orgConfig.getProperty("import.cron");
            if (StringUtils.isEmpty(orgCronExperession)) {
            	orgIdsWithCommonCronTrigger.add(orgId);
            } else {
            	scheduleTask(Arrays.asList(orgId), orgCronExperession);
            }
            	
        }
    	
    	if (!CollectionUtils.isEmpty(orgIdsWithCommonCronTrigger)) {
    		scheduleTask(orgIdsWithCommonCronTrigger, cronExperession);
    	}
    	
        
    }
    
    
    private void scheduleTask(List<Integer> organizationIds, String cronExpression) {
    	CronTrigger cronTrigger = new CronTrigger(cronExpression);
    	taskScheduler.schedule(new RunnableTask(organizationIds), cronTrigger);
    }

    class RunnableTask implements Runnable {

        private List<Integer> organizationIds;

        public RunnableTask(List<Integer> organizationIds) {
            this.organizationIds = organizationIds;
        }

        @Override
        public void run() {
        	String orgIds = "";
        	for (Integer orgId: organizationIds) {
        		orgIds += orgId + ",";
        	}
        	log.info("Strating import for organizations: " + orgIds);
        	adrImportService.importDocumentsSinceLastImportOrganizations(organizationIds);
        }
    }

}
