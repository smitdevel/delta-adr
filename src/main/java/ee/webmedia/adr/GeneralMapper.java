package ee.webmedia.adr;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.type.JdbcType;

import ee.webmedia.adr.model.CompoundFunction;
import ee.webmedia.adr.model.CompoundSeries;
import ee.webmedia.adr.model.Function;
import ee.webmedia.adr.model.Series;
import ee.webmedia.adr.model.Volume;

public interface GeneralMapper {

    @Select("SELECT nextval('default_seq')")
    int defaultSeqNextVal();

    @Select({ "SELECT COUNT(*) FROM document",
            "WHERE document.type_id = #{documentTypeId}" })
    int getDocumentCountByType(@Param("documentTypeId") int documentTypeId);

    @Select({ "SELECT COUNT(*) FROM document",
            "WHERE document.volume_id = #{volumeId}" })
    int getDocumentCountByVolume(@Param("volumeId") int volumeId);

    @Select({ "SELECT COUNT(*) FROM volume",
            "WHERE volume.compound_series_id = #{compoundSeriesId}" })
    int getVolumeCount(@Param("compoundSeriesId") int compoundSeriesId);

    @Select({ "SELECT COUNT(*) FROM compound_series",
            "WHERE compound_series.compound_function_id = #{compoundFunctionId}" })
    int getCompoundSeriesCount(@Param("compoundFunctionId") int compoundFunctionId);

    @Select({ "SELECT function.* FROM function",
            "JOIN compound_function ON function.compound_function_id = compound_function.id",
            "WHERE compound_function.organization_id = #{organizationId}",
            "AND function.noderef = #{noderef}" })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "compound_function_id", property = "compoundFunctionId", jdbcType = JdbcType.INTEGER),
            @Result(column = "noderef", property = "noderef", jdbcType = JdbcType.VARCHAR),
            @Result(column = "created_date_time", property = "createdDateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "modified_date_time", property = "modifiedDateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Function getFunction(@Param("organizationId") int organizationId, @Param("noderef") String noderef);

    @Select({ "SELECT compound_function.* FROM compound_function",
            "WHERE compound_function.organization_id = #{organizationId}",
            "AND compound_function.mark = #{mark}",
            "AND compound_function.title = #{title}" })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "organization_id", property = "organizationId", jdbcType = JdbcType.INTEGER),
            @Result(column = "mark", property = "mark", jdbcType = JdbcType.VARCHAR),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "order_num", property = "orderNum", jdbcType = JdbcType.INTEGER),
            @Result(column = "created_date_time", property = "createdDateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "modified_date_time", property = "modifiedDateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    CompoundFunction getCompoundFunction(@Param("organizationId") int organizationId, @Param("mark") String mark, @Param("title") String title);

    @Select({ "SELECT series.* FROM series",
            "JOIN compound_series ON series.compound_series_id = compound_series.id",
            "WHERE compound_series.compound_function_id = #{compoundFunctionId}",
            "AND series.noderef = #{noderef}" })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "compound_series_id", property = "compoundSeriesId", jdbcType = JdbcType.INTEGER),
            @Result(column = "noderef", property = "noderef", jdbcType = JdbcType.VARCHAR),
            @Result(column = "created_date_time", property = "createdDateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "modified_date_time", property = "modifiedDateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Series getSeries(@Param("compoundFunctionId") int compoundFunctionId, @Param("noderef") String noderef);

    @Select({ "SELECT compound_series.* FROM compound_series",
            "WHERE compound_series.compound_function_id = #{compoundFunctionId}",
            "AND compound_series.mark = #{mark}",
            "AND compound_series.title = #{title}" })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "compound_function_id", property = "compoundFunctionId", jdbcType = JdbcType.INTEGER),
            @Result(column = "mark", property = "mark", jdbcType = JdbcType.VARCHAR),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "order_num", property = "orderNum", jdbcType = JdbcType.INTEGER),
            @Result(column = "created_date_time", property = "createdDateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "modified_date_time", property = "modifiedDateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    CompoundSeries getCompoundSeries(@Param("compoundFunctionId") int compoundFunctionId, @Param("mark") String mark, @Param("title") String title);

    @Select({ "SELECT volume.* FROM volume",
            "WHERE volume.compound_series_id = #{compoundSeriesId}",
            "AND volume.noderef = #{noderef}" })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "compound_series_id", property = "compoundSeriesId", jdbcType = JdbcType.INTEGER),
            @Result(column = "noderef", property = "noderef", jdbcType = JdbcType.VARCHAR),
            @Result(column = "mark", property = "mark", jdbcType = JdbcType.VARCHAR),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "valid_from_date", property = "validFromDate", jdbcType = JdbcType.DATE),
            @Result(column = "valid_to_date", property = "validToDate", jdbcType = JdbcType.DATE),
            @Result(column = "created_date_time", property = "createdDateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "modified_date_time", property = "modifiedDateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    Volume getVolume(@Param("compoundSeriesId") int compoundSeriesId, @Param("noderef") String noderef);

    @Select({ "SELECT document.*, document_type.title AS type_title FROM document",
            "JOIN document_type ON document.type_id = document_type.id",
            "WHERE document.volume_id = #{volumeId}",
            "ORDER BY document.reg_date_time DESC, document.reg_number DESC",
            "LIMIT #{limit} OFFSET #{offset}" })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "organization_id", property = "organizationId", jdbcType = JdbcType.INTEGER),
            @Result(column = "reg_date_time", property = "regDateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "reg_number", property = "regNumber", jdbcType = JdbcType.VARCHAR),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "type_id", property = "typeId", jdbcType = JdbcType.INTEGER),
            @Result(column = "type_title", property = "typeTitle", jdbcType = JdbcType.VARCHAR),
            @Result(column = "volume_id", property = "volumeId", jdbcType = JdbcType.INTEGER),
            @Result(column = "recipient", property = "recipient", jdbcType = JdbcType.VARCHAR),
            @Result(column = "access_restriction", property = "accessRestriction", jdbcType = JdbcType.VARCHAR),
            @Result(column = "access_restriction_reason", property = "accessRestrictionReason", jdbcType = JdbcType.VARCHAR),
            @Result(column = "access_restriction_begin_date", property = "accessRestrictionBeginDate", jdbcType = JdbcType.DATE),
            @Result(column = "access_restriction_end_date", property = "accessRestrictionEndDate", jdbcType = JdbcType.DATE),
            @Result(column = "access_restriction_end_desc", property = "accessRestrictionEndDesc", jdbcType = JdbcType.VARCHAR),
            @Result(column = "due_date", property = "dueDate", jdbcType = JdbcType.DATE),
            @Result(column = "compliance_date", property = "complianceDate", jdbcType = JdbcType.DATE),
            @Result(column = "send_date", property = "sendDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "compilator", property = "compilator", jdbcType = JdbcType.VARCHAR),
            @Result(column = "signer", property = "signer", jdbcType = JdbcType.VARCHAR),
            @Result(column = "annex", property = "annex", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sender_reg_number", property = "senderRegNumber", jdbcType = JdbcType.VARCHAR),
            @Result(column = "transmittal_mode", property = "transmittalMode", jdbcType = JdbcType.VARCHAR),
            @Result(column = "party", property = "party", jdbcType = JdbcType.VARCHAR),
            @Result(column = "created_date_time", property = "createdDateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "modified_date_time", property = "modifiedDateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    List<DocumentWithType> getDocumentsByVolume(@Param("volumeId") int volumeId, @Param("offset") int offset, @Param("limit") int limit);

    @Select({
            "SELECT document.*, document_type.title AS type_title FROM document",
            "JOIN document_type ON document.type_id = document_type.id",
            "WHERE document.organization_id = #{organizationId}",
            "AND (document.reg_number ILIKE #{escapedLikeInput} OR document.sender_reg_number ILIKE #{escapedLikeInput} OR ",
            "to_tsvector('simple', document.title || ' ' || document.access_restriction || ' ' || document.access_restriction_reason || ' ' || document.access_restriction_end_desc || ' ' || document.annex || ' ' || document.transmittal_mode || ' ' || document.compilator || ' ' || document.party) @@ #{tsquery}::tsquery)",
            "ORDER BY document.reg_date_time DESC, document.reg_number DESC",
            "LIMIT #{limit} OFFSET #{offset}" })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "organization_id", property = "organizationId", jdbcType = JdbcType.INTEGER),
            @Result(column = "reg_date_time", property = "regDateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "reg_number", property = "regNumber", jdbcType = JdbcType.VARCHAR),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "type_id", property = "typeId", jdbcType = JdbcType.INTEGER),
            @Result(column = "type_title", property = "typeTitle", jdbcType = JdbcType.VARCHAR),
            @Result(column = "volume_id", property = "volumeId", jdbcType = JdbcType.INTEGER),
            @Result(column = "sender", property = "sender", jdbcType = JdbcType.VARCHAR),
            @Result(column = "recipient", property = "recipient", jdbcType = JdbcType.VARCHAR),
            @Result(column = "access_restriction", property = "accessRestriction", jdbcType = JdbcType.VARCHAR),
            @Result(column = "access_restriction_reason", property = "accessRestrictionReason", jdbcType = JdbcType.VARCHAR),
            @Result(column = "access_restriction_begin_date", property = "accessRestrictionBeginDate", jdbcType = JdbcType.DATE),
            @Result(column = "access_restriction_end_date", property = "accessRestrictionEndDate", jdbcType = JdbcType.DATE),
            @Result(column = "access_restriction_end_desc", property = "accessRestrictionEndDesc", jdbcType = JdbcType.VARCHAR),
            @Result(column = "due_date", property = "dueDate", jdbcType = JdbcType.DATE),
            @Result(column = "compliance_date", property = "complianceDate", jdbcType = JdbcType.DATE),
            @Result(column = "send_date", property = "sendDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "compilator", property = "compilator", jdbcType = JdbcType.VARCHAR),
            @Result(column = "signer", property = "signer", jdbcType = JdbcType.VARCHAR),
            @Result(column = "annex", property = "annex", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sender_reg_number", property = "senderRegNumber", jdbcType = JdbcType.VARCHAR),
            @Result(column = "transmittal_mode", property = "transmittalMode", jdbcType = JdbcType.VARCHAR),
            @Result(column = "party", property = "party", jdbcType = JdbcType.VARCHAR),
            @Result(column = "created_date_time", property = "createdDateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "modified_date_time", property = "modifiedDateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    List<DocumentWithType> quickSearchDocuments(@Param("organizationId") int organizationId, @Param("tsquery") String tsquery, @Param("escapedLikeInput") String escapedLikeInput,
            @Param("offset") int offset, @Param("limit") int limit);

    @SelectProvider(type = DocumentSearchSqlProvider.class, method = "selectByAdvancedSearchForm")
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "organization_id", property = "organizationId", jdbcType = JdbcType.INTEGER),
            @Result(column = "reg_date_time", property = "regDateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "reg_number", property = "regNumber", jdbcType = JdbcType.VARCHAR),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "type_id", property = "typeId", jdbcType = JdbcType.INTEGER),
            @Result(column = "type_title", property = "typeTitle", jdbcType = JdbcType.VARCHAR),
            @Result(column = "volume_id", property = "volumeId", jdbcType = JdbcType.INTEGER),
            @Result(column = "sender", property = "sender", jdbcType = JdbcType.VARCHAR),
            @Result(column = "recipient", property = "recipient", jdbcType = JdbcType.VARCHAR),
            @Result(column = "access_restriction", property = "accessRestriction", jdbcType = JdbcType.VARCHAR),
            @Result(column = "access_restriction_reason", property = "accessRestrictionReason", jdbcType = JdbcType.VARCHAR),
            @Result(column = "access_restriction_begin_date", property = "accessRestrictionBeginDate", jdbcType = JdbcType.DATE),
            @Result(column = "access_restriction_end_date", property = "accessRestrictionEndDate", jdbcType = JdbcType.DATE),
            @Result(column = "access_restriction_end_desc", property = "accessRestrictionEndDesc", jdbcType = JdbcType.VARCHAR),
            @Result(column = "due_date", property = "dueDate", jdbcType = JdbcType.DATE),
            @Result(column = "compliance_date", property = "complianceDate", jdbcType = JdbcType.DATE),
            @Result(column = "send_date", property = "sendDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "compilator", property = "compilator", jdbcType = JdbcType.VARCHAR),
            @Result(column = "signer", property = "signer", jdbcType = JdbcType.VARCHAR),
            @Result(column = "annex", property = "annex", jdbcType = JdbcType.VARCHAR),
            @Result(column = "sender_reg_number", property = "senderRegNumber", jdbcType = JdbcType.VARCHAR),
            @Result(column = "transmittal_mode", property = "transmittalMode", jdbcType = JdbcType.VARCHAR),
            @Result(column = "party", property = "party", jdbcType = JdbcType.VARCHAR),
            @Result(column = "created_date_time", property = "createdDateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "modified_date_time", property = "modifiedDateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    List<DocumentWithType> advancedSearchDocuments(AdvancedSearchQuery advancedSearch);

    @Select({
            "SELECT COUNT(*) FROM document",
            "WHERE document.organization_id = #{organizationId}",
            "AND (document.reg_number ILIKE #{escapedLikeInput} OR document.sender_reg_number ILIKE #{escapedLikeInput} OR ",
            "to_tsvector('simple', document.title || ' ' || document.access_restriction || ' ' || document.access_restriction_reason || ' ' || document.access_restriction_end_desc || ' ' || document.annex || ' ' || document.transmittal_mode || ' ' || document.compilator || ' ' || document.party) @@ #{tsquery}::tsquery)" })
    int quickSearchDocumentsCount(@Param("organizationId") int organizationId, @Param("tsquery") String tsquery, @Param("escapedLikeInput") String escapedLikeInput);

    @Select({
            "SELECT document_association.*, true AS source,",
            "document.id AS other_document_id, document.reg_number, document.title, document_type.title AS type_title",
            "FROM document_association",
            "JOIN document",
            "ON document.id = document_association.target_document_id",
            "JOIN document_type",
            "ON document.type_id = document_type.id",
            "WHERE source_document_id = #{documentId}",
            "UNION ALL",
            "SELECT document_association.*, false AS source,",
            "document.id AS other_document_id, document.reg_number, document.title, document_type.title AS type_title",
            "FROM document_association", "JOIN document",
            "ON document.id = document_association.source_document_id",
            "JOIN document_type", "ON document.type_id = document_type.id",
            "WHERE target_document_id = #{documentId}",
            "ORDER BY reg_number ASC, type ASC" })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "source_document_id", property = "sourceDocumentId", jdbcType = JdbcType.INTEGER),
            @Result(column = "target_document_id", property = "targetDocumentId", jdbcType = JdbcType.INTEGER),
            @Result(column = "type", property = "type", jdbcType = JdbcType.VARCHAR),
            @Result(column = "created_date_time", property = "createdDateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "source", property = "source", jdbcType = JdbcType.BOOLEAN),
            @Result(column = "other_document_id", property = "otherDocumentId", jdbcType = JdbcType.INTEGER),
            @Result(column = "reg_number", property = "regNumber", jdbcType = JdbcType.VARCHAR),
            @Result(column = "title", property = "title", jdbcType = JdbcType.VARCHAR),
            @Result(column = "type_title", property = "typeTitle", jdbcType = JdbcType.VARCHAR)
    })
    List<DocumentAssociationWithData> getAssociations(@Param("documentId") int documentId);

    @Select({ "SELECT",
            "LEAST(import.query_begin_date, import.execution_date_time::date - 1) AS begin_date,",
            "LEAST(import.query_end_date, import.execution_date_time::date - 1) AS end_date",
            "FROM import",
            "WHERE import.query_begin_date <= import.query_end_date",
            "AND import.documents_created > -1",
            "AND import.documents_updated > -1",
            "AND import.documents_deleted > -1",
            "AND organization_id = #{organizationId}",
            "ORDER BY begin_date ASC, end_date ASC" })
    @Results({
            @Result(column = "begin_date", property = "beginDate", jdbcType = JdbcType.DATE),
            @Result(column = "end_date", property = "endDate", jdbcType = JdbcType.DATE)
    })
    List<ImportInterval> getImportIntervals(@Param("organizationId") int organizationId);

}
