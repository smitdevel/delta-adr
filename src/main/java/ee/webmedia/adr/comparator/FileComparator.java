package ee.webmedia.adr.comparator;

import java.util.Comparator;

import ee.webmedia.adr.model.File;

public class FileComparator implements Comparator<File> {
	EstonianStringComparator eComp = new EstonianStringComparator();
	
	@Override
	public int compare(File o1, File o2) {
		String title1 = o1.getTitle();
		String title2 = o2.getTitle();

		return eComp.compare(title1, title2);
	}

}
