package ee.webmedia.adr.comparator;

import java.util.Comparator;

public class EstonianStringComparator implements Comparator<String> {
	
	final String ORDER = "0123456789AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsŠšZzŽžTtUuVvWwÕõÄäÖöÜüXxYy";

	@Override
	public int compare(String s1, String s2) {
        int pos1 = 0;
        int pos2 = 0;
        for (int i = 0; i < Math.min(s1.length(), s2.length()) && pos1 == pos2; i++) {          		      		
            pos1 = ORDER.indexOf(s1.charAt(i));
            pos1 = toUpperCase(pos1);
            pos2 = ORDER.indexOf(s2.charAt(i));
            pos2 = toUpperCase(pos2);
        }

        if (pos1 == pos2 && s1.length() != s2.length()) {
        	return s1.length() - s2.length();
        }
        return pos1 - pos2;
    }
	
	public int toUpperCase(int pos){
    	if (pos % 2 != 0 && pos > 9) {
    		pos--;
		}
    	return pos;
    }

}
