package ee.webmedia.adr;

import ee.webmedia.adr.comparator.FileComparator;
import ee.webmedia.adr.dao.*;
import ee.webmedia.adr.model.File;
import ee.webmedia.adr.model.*;
import ee.webmedia.adr.service.AdrImportService;
import ee.webmedia.adr.service.DigiDocService;
import org.apache.commons.lang.StringUtils;
import org.digidoc4j.DataFile;
import org.json.JSONArray;
import org.json.JSONObject;
import org.postgresql.util.PGobject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.*;
import java.text.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang.StringUtils.isBlank;

@Controller
public class AdrController implements InitializingBean {
    protected final Logger log = LoggerFactory.getLogger(getClass());

    private static final String ORGANIZATION_NAME_KEY = "name";

    @Autowired
    protected ServletContext context;
    @Autowired
    protected DocumentMapper documentMapper;
    @Autowired
    protected FileMapper fileMapper;
    @Autowired
    protected DocumentTypeMapper documentTypeMapper;
    @Autowired
    protected CompoundFunctionMapper compoundFunctionMapper;
    @Autowired
    protected CompoundSeriesMapper compoundSeriesMapper;
    @Autowired
    protected VolumeMapper volumeMapper;
    @Autowired
    protected DocumentAssociationMapper documentAssociationMapper;
    @Autowired
    protected GeneralMapper generalMapper;
    @Autowired
    private DataSource dataSource;
    @Autowired
    protected Properties config;
    @Autowired
    protected DigiDocService digiDocService;

    @Autowired
    protected AdrImportService adrImportService;


    @Value("#{config['dir.root']}")
    private String filePath;
    private Map<Integer, Properties> organizationConfigs;
    private Date doNotDisplayFilesBefore;
    private Date doNotDisplayFileModificatioDateBefore;
    private JdbcTemplate jdbcTemplate;
    private Map<String /* organizationName */, Integer /* organizationId */> organizationNameCache;
    private Map<Integer /* organizationId */, Map<Integer, String>> transmittalModesCache;
    private Map<Integer /* organizationId */, Map<Integer /* documentTypeId */, String /* documentTypeTitle */>> documentTypesCache;
    private List<Map<String, String>> organizationLinkCache;

    private final int pageSize = 20;
    private final int pagerShowPages = 5;
    boolean searchResultsLimitEnabled = true; // do not disable, otherwise db queries get slow
    int searchResultsLimit = 100;

    private final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    {
        dateFormat.setLenient(false);
    }

    @Value("#{config['hide.filesOfDocument.documentRegDate']}")
    public void setDoNotDisplayFilesBefore(String doNotDisplayFilesBefore) {
        try {
            this.doNotDisplayFilesBefore = dateFormat.parse(doNotDisplayFilesBefore);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Value("#{config['hide.fileModificationDateBefore']}")
    public void setDoNotDisplayFileModificatioDateBefore(String doNotDisplayFileModificatioDateBefore) {
        try {
            this.doNotDisplayFileModificatioDateBefore = dateFormat.parse(doNotDisplayFileModificatioDateBefore);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        organizationConfigs = new HashMap<Integer, Properties>();
        for (String propKey : config.stringPropertyNames()) {
            if (!StringUtils.startsWithIgnoreCase(propKey, "org.")) {
                continue;
            }
            String value = config.getProperty(propKey);
            String[] parts = propKey.split("\\.", 3);
            if (ORGANIZATION_NAME_KEY.equals(parts[2]) && !value.matches("[a-z0-9_-]*")) {
                RuntimeException e = new RuntimeException("Organization name can contain only a-z, 0-9, - or _ characters. Found: " + value);
                log.debug("Error loading organization properties!", e);
                throw e;
            }

            int orgId = Integer.parseInt(parts[1]);
            Properties orgProps = organizationConfigs.get(orgId);
            if (orgProps == null) {
                orgProps = new Properties();
                organizationConfigs.put(orgId, orgProps);
            }

            orgProps.put(parts[2], value);
        }

        organizationNameCache = new HashMap<String, Integer>();
        transmittalModesCache = new HashMap<Integer, Map<Integer, String>>();
        for (Entry<Integer, Properties> entry : organizationConfigs.entrySet()) {
            Properties props = entry.getValue();
            Integer orgId = entry.getKey();
            organizationNameCache.put(props.getProperty(ORGANIZATION_NAME_KEY), orgId);
            addOrgTransmittalModes(props, orgId);
        }

        jdbcTemplate = new JdbcTemplate(dataSource);
        loadDocumentTypes();
        loadOrganizationLinks();
    }

    private void addOrgTransmittalModes(Properties props, Integer orgId) {
        String transmittalModesStr = props.getProperty("sendModes");
        if (StringUtils.isNotBlank(transmittalModesStr)) {
            String[] split = transmittalModesStr.split(",");
            HashMap<Integer, String> orgTransmittalModes = new HashMap<Integer, String>();
            for (int i = 0; i < split.length; i++) {
                String transmittalMode = StringUtils.trimToNull(split[i]);
                if (transmittalMode == null) {
                    continue;
                }
                orgTransmittalModes.put(i, transmittalMode);
            }
            transmittalModesCache.put(orgId, orgTransmittalModes);
        }
    }

    public void loadDocumentTypes() {
        Map<Integer, Map<Integer, String>> documentTypesAllOrganizations = new LinkedHashMap<Integer, Map<Integer, String>>();
        for (Integer organizationId : organizationConfigs.keySet()) {
            Map<Integer, String> documentTypesInOrganization = new HashMap<Integer, String>();
            documentTypesAllOrganizations.put(organizationId, documentTypesInOrganization);
        }
        DocumentTypeExample documentTypeQuery = new DocumentTypeExample();
        documentTypeQuery.setOrderByClause("organization_id ASC, title ASC");
        List<DocumentType> documentTypes = documentTypeMapper.selectByExample(documentTypeQuery);
        for (DocumentType documentType : documentTypes) {
            Map<Integer, String> documentTypesInOrganization = documentTypesAllOrganizations.get(documentType.getOrganizationId());
            if (documentTypesInOrganization != null) {
                documentTypesInOrganization.put(documentType.getId(), documentType.getTitle());
            }
        }
        documentTypesCache = Collections.unmodifiableMap(documentTypesAllOrganizations);
    }

    private void loadOrganizationLinks() {
        organizationLinkCache = new ArrayList<Map<String, String>>();
        for (Properties props : organizationConfigs.values()) {
            Map<String, String> linkData = new HashMap<String, String>(2);
            linkData.put("url", props.getProperty(ORGANIZATION_NAME_KEY));
            linkData.put("text", props.getProperty("headerTitle"));
            organizationLinkCache.add(linkData);
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getEntry(Model model) {
        model.addAttribute("links", organizationLinkCache);
        Properties prop = new Properties();
        try {
			prop.load(context.getResourceAsStream("/META-INF/MANIFEST.MF"));
		} catch (IOException e) {
			log.error(e.getMessage());
		}
        model.addAttribute("version", prop.getProperty("Implementation-Version"));
        return "entry";
    }

    @RequestMapping(value = "/{organizationName}/", method = RequestMethod.GET)
    public String getFunctions(@PathVariable String organizationName, Model model) {
        int organizationId = getOrganizationId(organizationName);
        String homepageHtmlFile = getOrganizationProperty(organizationId, "homepageHtmlFile");
        String page;
        if (isBlank(homepageHtmlFile)) {
            page = "functions";
            CompoundFunctionExample compoundFunctionQuery = new CompoundFunctionExample();
            compoundFunctionQuery.createCriteria().andOrganizationIdEqualTo(organizationId);
            compoundFunctionQuery.setOrderByClause("order_num ASC, mark ASC, title ASC");
            List<CompoundFunction> compoundFunctions = compoundFunctionMapper.selectByExample(compoundFunctionQuery);
            model.addAttribute("compoundFunctions", compoundFunctions);
        } else {
            page = "/resources/" + homepageHtmlFile;
        }
        return page(model, organizationId, page, true);
    }

    @RequestMapping(value = "/{organizationName}/funktsioon/{functionId}", method = RequestMethod.GET)
    public String getSeries(@PathVariable String organizationName, @PathVariable int functionId, Model model) {
        int organizationId = getOrganizationId(organizationName);
        checkBrowsingAllowed(organizationId);

        // TODO check if function does not exist, throw ResourceNotFoundException

        CompoundSeriesExample compoundSeriesQuery = new CompoundSeriesExample();
        compoundSeriesQuery.createCriteria().andCompoundFunctionIdEqualTo(functionId);
        compoundSeriesQuery.setOrderByClause("order_num ASC, mark ASC, title ASC");
        // TODO check organizationId also
        List<CompoundSeries> compoundSeries = compoundSeriesMapper.selectByExample(compoundSeriesQuery);
        model.addAttribute("compoundSeriesList", compoundSeries);

        return page(model, organizationId, "series", true);
    }

    @RequestMapping(value = "/{organizationName}/sari/{seriesId}", method = RequestMethod.GET)
    public String getVolumes(@PathVariable String organizationName, @PathVariable int seriesId, Model model) {
        int organizationId = getOrganizationId(organizationName);
        checkBrowsingAllowed(organizationId);

        // TODO check if function does not exist, throw ResourceNotFoundException

        VolumeExample volumeQuery = new VolumeExample();
        volumeQuery.createCriteria().andCompoundSeriesIdEqualTo(seriesId);
        volumeQuery.setOrderByClause("valid_from_date DESC, mark ASC, title ASC");
        // TODO check organizationId also
        List<Volume> volumes = volumeMapper.selectByExample(volumeQuery);
        model.addAttribute("volumes", volumes);

        return page(model, organizationId, "volumes", true);
    }

    @RequestMapping(value = "/{organizationName}/toimik/{volumeId}", method = RequestMethod.GET)
    public String getDocuments(@PathVariable String organizationName, @PathVariable int volumeId, Model model) {
        int organizationId = getOrganizationId(organizationName);
        checkBrowsingAllowed(organizationId);
        return "redirect:/" + getOrganizationName(organizationId) + "/toimik/" + volumeId + "/1";
    }

    @RequestMapping(value = "/{organizationName}/toimik/{volumeId}/{pageNumber}", method = RequestMethod.GET)
    public String getDocuments(@PathVariable String organizationName, @PathVariable int volumeId, Model model, @PathVariable int pageNumber) {
        int organizationId = getOrganizationId(organizationName);
        checkBrowsingAllowed(organizationId);

        // TODO check if function does not exist, throw ResourceNotFoundException

        DocumentExample documentCountQuery = new DocumentExample();
        documentCountQuery.createCriteria().andVolumeIdEqualTo(volumeId);
        int documentsCount = documentMapper.countByExample(documentCountQuery);

        String pagerAction = "/" + getOrganizationName(organizationId) + "/toimik/" + volumeId;
        int offset;
        try {
            offset = setPagerWithRedirect(model, pageNumber, documentsCount, pagerAction);
        } catch (PageNumberRedirectException e) {
            // Redirect before any attributes are added to model
            // Because redirecting puts all model attributes into query parameters
            return "redirect:" + e.getView();
        }

        // TODO check organizationId also
        List<DocumentWithType> documents = generalMapper.getDocumentsByVolume(volumeId, offset, pageSize);

        // @formatter:off
        /*
		for (DocumentWithType document : documents) {
            String title = document.getTitle();
            Pattern pattern = Pattern.compile("([^\\s-]{31,})");
            pattern.matcher();
        }
         */
        // @formatter:on
        model.addAttribute("documents", documents);

        return page(model, organizationId, "search", true);
    }

    private int setPagerWithRedirect(Model model, int pageNumber, int rowCount, String pagerAction) throws PageNumberRedirectException {
        return setPager(model, pageNumber, rowCount, pagerAction, true);
    }

    private int setPager(Model model, int pageNumber, int rowCount, String pagerAction) {
        try {
            return setPager(model, pageNumber, rowCount, pagerAction, false);
        } catch (PageNumberRedirectException e) {
            // Cannot happen
            throw new RuntimeException("Impossible", e);
        }
    }

    private int setPager(Model model, int pageNumber, int rowCount, String pagerAction, boolean redirect) throws PageNumberRedirectException {
        int pageNumberMax = (int) Math.ceil(rowCount / (double) pageSize);
        if (pageNumberMax < 1) {
            pageNumberMax = 1;
        }
        int oldPageNumber = pageNumber;
        if (pageNumber < 1) {
            pageNumber = 1;
        } else if (pageNumber > pageNumberMax) {
            pageNumber = pageNumberMax;
        }
        if (redirect && oldPageNumber != pageNumber) {
            throw new PageNumberRedirectException(pagerAction + "/" + pageNumber);
        }
        int pageNumberBegin = pageNumber - pagerShowPages;
        int pageNumberEnd = pageNumber + pagerShowPages;
        if (pageNumberBegin < 1) {
            pageNumberEnd += 1 - pageNumberBegin;
            pageNumberBegin = 1;
        }
        if (pageNumberEnd > pageNumberMax) {
            pageNumberBegin -= pageNumberEnd - pageNumberMax;
            if (pageNumberBegin < 1) {
                pageNumberBegin = 1;
            }
            pageNumberEnd = pageNumberMax;
        }
        model.addAttribute("pageNumber", pageNumber);
        model.addAttribute("pageNumberMax", pageNumberMax);
        model.addAttribute("pageNumberBegin", pageNumberBegin);
        model.addAttribute("pageNumberEnd", pageNumberEnd);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("rowCount", rowCount);
        model.addAttribute("pagerAction", pagerAction);
        int offset = (pageNumber - 1) * pageSize;
        int rowBegin = rowCount == 0 ? 0 : offset + 1;
        model.addAttribute("rowBegin", rowBegin);
        model.addAttribute("rowEnd", Math.min(pageNumber * pageSize, rowCount));
        return offset;
    }

    
    @RequestMapping("/sync-delta")
    public String syncDocuments() {
    	adrImportService.importDocumentsSinceLastImportAllOrganizations();
    	return "Synch with delta has started";
    }
    
    @RequestMapping("/{organizationName}/dokument/{documentId}")
    public String getDocument(@PathVariable String organizationName, @PathVariable int documentId, Model model) {
        int organizationId = getOrganizationId(organizationName);

        DocumentExample query = new DocumentExample();
        query.createCriteria().andIdEqualTo(documentId).andOrganizationIdEqualTo(organizationId);
        List<Document> documents = documentMapper.selectByExample(query);
        if (documents.size() != 1) {
            throw new ResourceNotFoundException();
        }
        Document document = documents.get(0);
        model.addAttribute("document", document);

        DocumentType documentType = documentTypeMapper.selectByPrimaryKey(document.getTypeId());
        model.addAttribute("documentType", documentType);

        Volume volume = volumeMapper.selectByPrimaryKey(document.getVolumeId());
        model.addAttribute("volume", volume);

        CompoundSeries compoundSeries = compoundSeriesMapper.selectByPrimaryKey(volume.getCompoundSeriesId());
        model.addAttribute("compoundSeries", compoundSeries);

        CompoundFunction compoundFunction = compoundFunctionMapper.selectByPrimaryKey(compoundSeries.getCompoundFunctionId());
        model.addAttribute("compoundFunction", compoundFunction);

        List<DocumentAssociationWithData> assocs = generalMapper.getAssociations(documentId);
        model.addAttribute("assocs", assocs);

        FileExample fileQuery = new FileExample();
        fileQuery.createCriteria().andDocumentIdEqualTo(document.getId());
        fileQuery.setOrderByClause("name ASC");
        List<File> files = fileMapper.selectByExample(fileQuery);
        
        List<String> dataFiles = null;
        
        if (document.getRegDateTime() != null && document.getRegDateTime().before(doNotDisplayFilesBefore)) {
            files = new ArrayList<File>();
        }

        Collections.sort(files, new FileComparator());
        boolean accessRestrictionEnabled = false;

        for (File file : files) {
            if (file.getFileModifiedDateTime() != null && file.getFileModifiedDateTime().before(doNotDisplayFileModificatioDateBefore)) {
                file.setFileModifiedDateTime(null);
            }
            String fileName = file.getName();
            if (ContainerFileHelper.isDigiDocFile(fileName)) {
                List<String> filesNames = digiDocService.getFilesNamesFromContainer(file, organizationId);
                List<ContainerFile> containerFiles = ContainerFileHelper.getContainerFiles(file, filesNames);
                if (!ContainerFileHelper.hasOnlyPublicAccessFiles(containerFiles)) {
                    file.setName("Fail");
                    file.setTitle("Fail");
                    file.setPublicAccess(false);
                }
                file.setContainerFiles(containerFiles);
            } else {
                if (file.isPrivateAccess()) {
                    file.setName("Fail");
                    file.setTitle("Fail");
                    file.setPublicAccess(false);
                }
            }
            JSONArray accessRestrictions = StringUtils.isNotBlank(file.getInnerAccessRestriction()) ? new JSONArray(file.getInnerAccessRestriction()) : new JSONArray();
            if (StringUtils.isNotBlank(file.getAccessRestriction()) || accessRestrictions.length() > 0) {
                accessRestrictionEnabled = true;
            }

        }

        String accessRestrictionChangeReason = document.getAccessRestrictionChangeReason();
        if (isBlank(accessRestrictionChangeReason)) {
            model.addAttribute("showReason", false);
            model.addAttribute("accessRestrictionChangeReason", "");
        } else {
            String[] split = accessRestrictionChangeReason.split("¤¤¤");
            if (split.length == 0) {
                model.addAttribute("showReason", false);
                model.addAttribute("accessRestrictionChangeReason", "");
            }
            else {
                if (split[1].equals("true")) {
                    model.addAttribute("showReason", true);
                    model.addAttribute("accessRestrictionChangeReason", split[0]);
                } else {
                    model.addAttribute("showReason", false);
                    model.addAttribute("accessRestrictionChangeReason", "");
                }
            }
        }

        model.addAttribute("files", files);
        model.addAttribute("accessRestrictionEnabled", accessRestrictionEnabled);

        return page(model, organizationId, "document", true);
    }

    @RequestMapping(value = { "/{organizationName}/otsing", "/{organizationName}/kiirotsing" }, method = RequestMethod.GET)
    public String getSearch(@PathVariable String organizationName, Model model) {
        int organizationId = getOrganizationId(organizationName);
        return page(model, organizationId, "search", false);
    }

    @RequestMapping(value = "/{organizationName}/kiirotsing", method = RequestMethod.POST)
    public String doQuickSearch(@PathVariable String organizationName, QuickSearchForm quickSearch, BindingResult result, Model model) {
        int organizationId = getOrganizationId(organizationName);
        doQuickSearch(organizationId, quickSearch, result, model);
        model.addAttribute("quickSearchSubmitted", "submitted");
        model.addAttribute("quickSearch", quickSearch);
        return page(model, organizationId, "search", false);
    }

    private void doQuickSearch(int organizationId, QuickSearchForm quickSearch, BindingResult result, Model model) {
        if (result.hasErrors()) {
            log.debug("Quick search form validation errors: " + result.getAllErrors());
            return;
        }

        String input = quickSearch.getInput();
        if (input == null) {
            input = "";
        }

        String tsquery = getTsquery(input);
        String escapedLikeInput = getEscapedLikeInput(input);

        // TODO try to parse dates from words

        if (tsquery.length() == 0 && escapedLikeInput.length() < 3) {
            model.addAttribute("quickSearchInputEmpty", true);
            return;
        }
        if (!containsLetterOrDigit(input)) {
            model.addAttribute("quickSearchInputDoesNotContainLetterOrDigit", true);
            return;
        }

        int documentsCount;
        List<DocumentWithType> allDocuments = null;
        if (searchResultsLimitEnabled) {
            allDocuments = generalMapper.quickSearchDocuments(organizationId, tsquery, escapedLikeInput, 0, searchResultsLimit + 1);
            documentsCount = allDocuments.size();
            if (documentsCount > searchResultsLimit) {
                documentsCount = searchResultsLimit;
                model.addAttribute("searchResultsLimitExeeded", searchResultsLimit);
            }
        } else {
            documentsCount = generalMapper.quickSearchDocumentsCount(organizationId, tsquery, escapedLikeInput);
        }

        String pagerAction = "/" + getOrganizationName(organizationId) + "/kiirotsing";
        int offset = setPager(model, quickSearch.getPageNumber(), documentsCount, pagerAction);

        List<DocumentWithType> documents;
        if (searchResultsLimitEnabled) {
            documents = new ArrayList<DocumentWithType>(pageSize);
            for (int i = offset; i < Math.min(offset + pageSize, documentsCount); i++) {
                documents.add(allDocuments.get(i));
            }
        } else {
            documents = generalMapper.quickSearchDocuments(organizationId, tsquery, escapedLikeInput, offset, pageSize);
        }

        model.addAttribute("documents", documents);
    }

    private static boolean containsLetterOrDigit(String str) {
        if (str == null) {
            return false;
        }
        for (int i = 0; i < str.length(); i++) {
            if (Character.isLetterOrDigit(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    private <T> String getArray(T[] userValues, Collection<T> validValues) {
        if (userValues == null) {
            return "";
        }
        Set<T> resultValues = new HashSet<T>(validValues);
        resultValues.retainAll(Arrays.asList(userValues));
        return createQueryString(userValues, resultValues, Enclosure.CURLY_BRACES, true);
    }

    private <T, S> String getArray(T[] userValues, Map<T, S> validValues) {
        if (userValues == null || userValues.length == 0) {
            return "";
        }
        Set<S> resultValues = new HashSet<S>(userValues.length);
        for (T userValue : userValues) {
            if (validValues.containsKey(userValue)) {
                resultValues.add(validValues.get(userValue));
            }
        }
        return createQueryString(userValues, resultValues, Enclosure.NONE, false);
    }

    private <T, S> String createQueryString(T[] userValues, Collection<S> resultValues, Enclosure enclosure, boolean useQuotationMarks) {
        String resultArray = "";
        if (resultValues.size() > 0) {
            resultArray = enclosure.start
                    + (useQuotationMarks ? "\"" : "")
                    + StringUtils.join(resultValues, (useQuotationMarks ? "\",\"" : ","))
                    + (useQuotationMarks ? "\"" : "")
                    + enclosure.end;
        }
        if (log.isDebugEnabled()) {
            log.debug("Parsed:\n  input[" + userValues.length + " items]=" + Arrays.asList(userValues) + "\n  array=" + resultArray);
        }
        return resultArray;
    }

    private String getEscapedLikeInput(String input) {
        String escapedLikeInput = StringUtils.strip(input);
        if (escapedLikeInput == null || escapedLikeInput.length() < 3) {
            escapedLikeInput = "";
        } else {
            escapedLikeInput = "%" + StringUtils.replace(StringUtils.replace(escapedLikeInput, "%", "\\%"), "_", "\\_") + "%";
        }
        if (log.isDebugEnabled()) {
            log.debug("Parsed:\n  input[" + input.length() + " chars]=" + input + "\n  escapedLikeInput[" + escapedLikeInput.length() + " chars]=" + escapedLikeInput);
        }
        return escapedLikeInput;
    }

    private String getTsquery(String input) {
        if (input == null) {
            input = "";
        }
        PGobject res = jdbcTemplate.queryForObject("SELECT plainto_tsquery('simple', ?)", PGobject.class, input);
        List<String> lexems = new ArrayList<String>();
        Pattern pattern = Pattern.compile("'[^']+'");
        String originalTsquery = res.getValue();
        Matcher matcher = pattern.matcher(originalTsquery);
        while (matcher.find()) {
            String lexemValue = matcher.group().substring(1, matcher.group().length() - 1);
            if (lexemValue.length() < 3) {
                continue;
            }
            String lexem = "'" + lexemValue + "':*";
            if (!lexems.contains(lexem)) {
                lexems.add(lexem);
            }
        }
        String tsquery = StringUtils.join(lexems, " & ");
        if (log.isDebugEnabled()) {
            log.debug("Parsed:\n  input[" + input.length() + " chars]=" + input + "\n  original tsquery=" + originalTsquery + "\n  new tsquery[" + lexems.size() + " lexems]="
                    + tsquery);
        }
        return tsquery;
    }

    @RequestMapping(value = "/{organizationName}/otsing", method = RequestMethod.POST)
    public String doSearch(@PathVariable String organizationName, AdvancedSearchForm advancedSearch, BindingResult result, Model model) {
        int organizationId = getOrganizationId(organizationName);
        List<MessageSourceResolvable> errors = new ArrayList<MessageSourceResolvable>();
        doSearch(organizationId, advancedSearch, result, model, errors);
        model.addAttribute("errors", errors);
        model.addAttribute("advancedSearchSubmitted", "submitted");
        model.addAttribute("advancedSearch", advancedSearch);
        return page(model, organizationId, "search", false);
    }

    private void doSearch(int organizationId, AdvancedSearchForm advancedSearch, BindingResult result, Model model, List<MessageSourceResolvable> errors) {
        if (result.hasErrors()) {
            errors.addAll(result.getFieldErrors());
        }
        if (!result.hasErrors() && advancedSearch.getRegDateBegin() == null && advancedSearch.getRegDateEnd() == null
                && (advancedSearch.getDocumentTypes() == null || advancedSearch.getDocumentTypes().length == 0)
                && StringUtils.isEmpty(advancedSearch.getTitle()) && StringUtils.isEmpty(advancedSearch.getParty())
                && StringUtils.isEmpty(advancedSearch.getSenderRegNr()) && StringUtils.isEmpty(advancedSearch.getAccessRestriction())
                && StringUtils.isEmpty(advancedSearch.getAccessRestrictionReason())
                && (advancedSearch.getTransmittalModes() == null || advancedSearch.getTransmittalModes().length == 0)
                && advancedSearch.getAccessRestrictionBeginDate() == null && advancedSearch.getAccessRestrictionEndDate() == null) {
            errors.add(new DefaultMessageSourceResolvable("search_all_fields_empty"));
            return;
        }

        String titleTsquery = getTsquery(advancedSearch.getTitle());
        if (StringUtils.isNotEmpty(advancedSearch.getTitle()) && StringUtils.isEmpty(titleTsquery)) {
            errors.add(new DefaultMessageSourceResolvable("search_title_field_short"));
        }
        String partyTsquery = getTsquery(advancedSearch.getParty());
        if (StringUtils.isNotEmpty(advancedSearch.getParty()) && StringUtils.isEmpty(partyTsquery)) {
            errors.add(new DefaultMessageSourceResolvable("search_party_field_short"));
        }
        String senderRegNumberEscapedLike = getEscapedLikeInput(advancedSearch.getSenderRegNr());
        if (StringUtils.isNotEmpty(advancedSearch.getSenderRegNr())) {
            if (StringUtils.isEmpty(senderRegNumberEscapedLike)) {
                errors.add(new DefaultMessageSourceResolvable("search_senderRegNumber_field_short"));
            } else if (!containsLetterOrDigit(advancedSearch.getSenderRegNr())) {
                errors.add(new DefaultMessageSourceResolvable("search_senderRegNumber_field_does_not_contain_letter_or_digit"));
            }
        }
        String accessRestrictionReasonTsquery = getTsquery(advancedSearch.getAccessRestrictionReason());
        if (StringUtils.isNotEmpty(advancedSearch.getAccessRestrictionReason()) && StringUtils.isEmpty(accessRestrictionReasonTsquery)) {
            errors.add(new DefaultMessageSourceResolvable("search_accessRestrictionReason_field_short"));
        }
        if (errors.size() > 0) {
            return;
        }

        // Add 23h59m59s to end dates for searching
        Date regDateEnd = advancedSearch.getRegDateEnd();
        if (regDateEnd != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(regDateEnd);
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
            advancedSearch.setRegDateEnd(cal.getTime());
        }
        Date accessRestrictionEndDate = advancedSearch.getAccessRestrictionEndDate();
        if (accessRestrictionEndDate != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(accessRestrictionEndDate);
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
            advancedSearch.setAccessRestrictionEndDate(cal.getTime());
        }

        String documentTypesArray = getArray(advancedSearch.getDocumentTypes(), documentTypesCache.get(organizationId).keySet());
        String transmittalModesArray = getArray(advancedSearch.getTransmittalModes(), transmittalModesCache.get(organizationId));
        AdvancedSearchQuery advancedSearchQuery = new AdvancedSearchQuery(advancedSearch,
                organizationId,
                documentTypesArray,
                titleTsquery,
                transmittalModesArray,
                partyTsquery,
                senderRegNumberEscapedLike,
                accessRestrictionReasonTsquery,
                0,
                searchResultsLimit + 1);
        List<DocumentWithType> allDocuments = generalMapper.advancedSearchDocuments(advancedSearchQuery);
        int rowCount = allDocuments.size();
        if (rowCount > searchResultsLimit) {
            rowCount = searchResultsLimit;
            model.addAttribute("searchResultsLimitExeeded", searchResultsLimit);
        }
        String pagerAction = "/" + getOrganizationName(organizationId) + "/otsing";
        int offset = setPager(model, advancedSearch.getPageNumber(), rowCount, pagerAction);
        List<DocumentWithType> documents = new ArrayList<DocumentWithType>(pageSize);
        for (int i = offset; i < Math.min(offset + pageSize, rowCount); i++) {
            documents.add(allDocuments.get(i));
        }
        model.addAttribute("documents", documents);
    }

    @RequestMapping(value = "/{organizationName}/fail/{fileId}/{fileName}", method = RequestMethod.GET)
    public void downloadFile(@PathVariable String organizationName, @PathVariable int fileId, @PathVariable String fileName, HttpServletResponse response) throws IOException {
        int organizationId = getOrganizationId(organizationName);

        File file = fileMapper.selectByPrimaryKey(fileId);
        if (file == null || file.isPrivateAccess()) {
            throw new ResourceNotFoundException();
        }

        Document document = documentMapper.selectByPrimaryKey(file.getDocumentId());
        if (document.getRegDateTime() != null && document.getRegDateTime().before(doNotDisplayFilesBefore)) {
            throw new ResourceNotFoundException();
        }

        // Try to open file before any response headers are set
        java.io.File organizationDir = new java.io.File(filePath, Integer.toString(organizationId));
        java.io.File fsFile = new java.io.File(organizationDir, Integer.toString(file.getId()));
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(fsFile));

        response.setContentType(file.getMimeType());
        response.setCharacterEncoding(file.getEncoding());
        response.setContentLength((int) fsFile.length());
        response.setHeader("Content-Disposition", "attachment"); // TODO ;filename=...

        ServletOutputStream out = response.getOutputStream();
        copy(out, in);
    }
    
    @RequestMapping(value = "/{organizationName}/fail/{fileId}/subfile/{fileIndex}", method = RequestMethod.GET)
    public void downloadSubFile(@PathVariable String organizationName, @PathVariable int fileId, @PathVariable Integer fileIndex, HttpServletResponse response) throws IOException {
        int organizationId = getOrganizationId(organizationName);

        File file = fileMapper.selectByPrimaryKey(fileId);
        if (file == null) {
            throw new ResourceNotFoundException();
        }

        Document document = documentMapper.selectByPrimaryKey(file.getDocumentId());
        if (document.getRegDateTime() != null && document.getRegDateTime().before(doNotDisplayFilesBefore)) {
            throw new ResourceNotFoundException();
        }

        // Try to open file before any response headers are set
        List<DataFile> dataFiles = digiDocService.getDataFiles(file, organizationId);
        DataFile dataFile = dataFiles.get(fileIndex);
        JSONArray accessRestrictions = StringUtils.isNotBlank(file.getInnerAccessRestriction()) ? new JSONArray(file.getInnerAccessRestriction()) : new JSONArray();
        JSONObject accessRestriction = !accessRestrictions.isNull(fileIndex) ? (JSONObject) accessRestrictions.get(fileIndex) : null;
        boolean isPublicAccessRestriction = accessRestriction == null || (accessRestriction.isNull("publicAccessRestriction") || (boolean) accessRestriction.get("publicAccessRestriction"));
        if (!isPublicAccessRestriction) {
            throw new ResourceNotFoundException();
        }
        java.io.InputStream inputStream = dataFile.getStream();
        BufferedInputStream in = new BufferedInputStream(inputStream);
        response.setContentType(dataFile.getMediaType());
        response.setCharacterEncoding(file.getEncoding());
        response.setContentLength(in.available());
        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", dataFile.getName()));
        ServletOutputStream out = response.getOutputStream();
        copy(out, in);

    }

    public int getOrganizationId(String organizationName) {
        Integer organizationId = organizationNameCache.get(organizationName);
        if (organizationId != null) {
            return organizationId;
        }
        throw new ResourceNotFoundException();
    }

    public String getOrganizationName(int organizationId) {
        return getOrganizationProperty(organizationId, ORGANIZATION_NAME_KEY);
    }

    private String getOrganizationProperty(int organizationId, String property) {
        Properties organizationConfig = organizationConfigs.get(organizationId);
        if (organizationConfig != null) {
            return organizationConfig.getProperty(property);
        }
        throw new ResourceNotFoundException();
    }

    private void checkBrowsingAllowed(int organizationId) {
        String homepageHtmlFile = getOrganizationProperty(organizationId, "homepageHtmlFile");
        if (StringUtils.isNotBlank(homepageHtmlFile)) {
            throw new ResourceNotFoundException();
        }
    }

    private String page(Model model, int organizationId, String page, boolean documentListActive) {
        model.addAttribute("organizationName", getOrganizationName(organizationId)); // XXX remove this, because lower variable contains it??
        model.addAttribute("organization", organizationConfigs.get(organizationId));
        model.addAttribute("page", StringUtils.contains(page, '.') ? page : page + ".jspx");
        model.addAttribute("documentListActive", documentListActive ? "active" : "");
        model.addAttribute("searchActive", !documentListActive ? "active" : "");
        // these are put here, instead of using @ModelAttribute("documentTypes"), because using redirect puts all model attributes into query arguments
        model.addAttribute("documentTypes", documentTypesCache.get(organizationId));
        model.addAttribute("transmittalModes", transmittalModesCache.get(organizationId));
        model.addAttribute("accessRestrictions", Arrays.asList("", "Avalik", "AK"));
        if (!model.containsAttribute("advancedSearch")) {
            model.addAttribute("advancedSearch", new AdvancedSearchForm());
        }
        if (!model.containsAttribute("quickSearch")) {
            model.addAttribute("quickSearch", new QuickSearchForm());
        }
        return "root";
    }

    private void copy(ServletOutputStream out, BufferedInputStream in)
            throws IOException {
        try {
            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = in.read(buffer)) != -1) {
                try {
                    out.write(buffer, 0, bytesRead);
                } catch (Exception e) {
                    // Writing to HTTP socket may throw exception if client aborts
                    log.debug("Error writing response", e);
                    return;
                }
            }
            try {
                out.flush();
            } catch (Exception e) {
                log.debug("Error flushing response", e);
            }
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                log.warn("Error closing input stream", e);
            }
            try {
                out.close();
            } catch (IOException e) {
                log.debug("Error closing reponse", e);
            }
        }
    }

    public Map<Integer, Properties> getOrganizationConfigs() {
        return Collections.unmodifiableMap(organizationConfigs);
    }

    private enum Enclosure {
        CURLY_BRACES("{", "}"), NONE("", "");

        final String start;
        final String end;

        Enclosure(String start, String end) {
            this.start = start;
            this.end = end;
        }

    }
}
