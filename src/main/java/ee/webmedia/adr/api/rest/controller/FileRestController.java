package ee.webmedia.adr.api.rest.controller;

import ee.webmedia.adr.api.rest.response.UploadFileResponse;
import ee.webmedia.adr.api.rest.service.RestApiService;
import ee.webmedia.adr.dao.FileMapper;
import ee.webmedia.adr.model.Document;
import ee.webmedia.adr.service.AdrImportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(value = "/api/v1/organizations/{organizationName}/documents/{nodeRef}/files", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class FileRestController {
    protected final Logger log = LoggerFactory.getLogger(getClass());
    
    @Value("#{config['dir.root']}")
    private String filePath;

	@Autowired
	private RestApiService restApiService;
	
    @Autowired
    private AdrImportService adrImportService;
    
	@Autowired
	private FileMapper fileMapper;
    
    @PostMapping
    public ResponseEntity<String> uploadFile(@PathVariable String organizationName, @PathVariable String nodeRef, @RequestParam("files") MultipartFile[] files, HttpServletRequest request) {
		String decodedNodeRef = restApiService.decodeBase64IfPossible(nodeRef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		int orgId = restApiService.getOrganizationId(organizationName);
		Document doc = adrImportService.getDocument(orgId, decodedNodeRef);
		if (doc == null) {
			return ResponseEntity.notFound().build();
		}
		String orgDirPath = String.format("%s/%d", filePath, orgId);
		if (Files.notExists(Paths.get(orgDirPath))) {
			try { 
				Files.createDirectory(Paths.get(orgDirPath)); 
			} catch (Exception e) {
	        	log.error("Unable to create directory" + orgDirPath);
	        }
		}
		List<UploadFileResponse> uploadFileResponses = new ArrayList<>();
		for (MultipartFile file : files) {
			ee.webmedia.adr.model.File fileModel = restApiService.createFileModel(doc.getId(), file);
	        Path copyLocation = Paths.get(orgDirPath + File.separator + StringUtils.cleanPath(fileModel.getId().toString()));
	        try {
				Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
				fileMapper.insert(fileModel);
				uploadFileResponses.add(new UploadFileResponse(fileModel.getId(), restApiService.createFileUri(fileModel, organizationName, request.getServerName())));
			} catch (Exception e) {
				return ResponseEntity.badRequest().body(e.getMessage());
			}
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(restApiService.toJSON(uploadFileResponses));
    }

    @GetMapping
	public ResponseEntity<String> getAllDocumentFiles(@PathVariable String organizationName, @PathVariable String nodeRef) {
		String decodedNodeRef = restApiService.decodeBase64IfPossible(nodeRef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		Document document = adrImportService.getDocument(restApiService.getOrganizationId(organizationName), decodedNodeRef);
		if (document == null) {
			return ResponseEntity.notFound().build();
		}

		int orgId = restApiService.getOrganizationId(organizationName);
		List<ee.webmedia.adr.model.File> files = restApiService.getFiles(document.getId());
		for (ee.webmedia.adr.model.File file : files) {
			restApiService.setContainerFileNames(file, orgId);
		}
		return ResponseEntity.ok(restApiService.toJSON(files));
	}
    
    @GetMapping("/{id}")
    public ResponseEntity<?> downloadFile(@PathVariable String organizationName, @PathVariable String nodeRef, @PathVariable Integer id, HttpServletResponse response) throws IOException {
		String decodedNodeRef = restApiService.decodeBase64IfPossible(nodeRef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
        ee.webmedia.adr.model.File file = fileMapper.selectByPrimaryKey(id);
        if (file == null || file.isPrivateAccess()) {
			return ResponseEntity.notFound().build();
        }

    	int organizationId = restApiService.getOrganizationId(organizationName);
		Document document = adrImportService.getDocument(organizationId, decodedNodeRef);
		if (document == null) {
			return ResponseEntity.notFound().build();
		}

        // Try to open file before any response headers are set
        java.io.File organizationDir = new java.io.File(filePath, Integer.toString(organizationId));
        java.io.File fsFile = new java.io.File(organizationDir, Integer.toString(file.getId()));
		Resource resource = new FileSystemResource(fsFile);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(file.getMimeType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
                .body(resource);
    }
    
    @DeleteMapping("/{id}")
	public ResponseEntity<String> deleteDocumentFile(@PathVariable String organizationName, @PathVariable String nodeRef, @PathVariable Integer id) {
    	
		String decodedNodeRef = restApiService.decodeBase64IfPossible(nodeRef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		int orgId = restApiService.getOrganizationId(organizationName);
		Document document = adrImportService.getDocument(orgId, decodedNodeRef);
		if (document == null) {
			return ResponseEntity.notFound().build();
		}
		int deleted = fileMapper.deleteByPrimaryKey(id);
		if (deleted == 0) {
			return ResponseEntity.notFound().build();
		}
		String orgDirPath = String.format("%s/%d", filePath, orgId);
		File file = new File(orgDirPath + File.separator + StringUtils.cleanPath(id.toString()));
		file.delete();

		return ResponseEntity.noContent().build();
	}
    
    @DeleteMapping
	public ResponseEntity<String> deleteDocumentFile(@PathVariable String organizationName, @PathVariable String nodeRef) {
    	
		String decodedNodeRef = restApiService.decodeBase64IfPossible(nodeRef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		int orgId = restApiService.getOrganizationId(organizationName);
		Document document = adrImportService.getDocument(orgId, decodedNodeRef);
		if (document == null) {
			return ResponseEntity.notFound().build();
		}
		List<ee.webmedia.adr.model.File> files = restApiService.getFiles(document.getId());
		if (files.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		for (ee.webmedia.adr.model.File fileModel : files) {
			int deleted = fileMapper.deleteByPrimaryKey(fileModel.getId());
			if (deleted == 0) {
				return ResponseEntity.notFound().build();
			}
			String orgDirPath = String.format("%s/%d", filePath, orgId);
			File file = new File(orgDirPath + File.separator + StringUtils.cleanPath(fileModel.toString()));
			file.delete();
		}

		return ResponseEntity.noContent().build();
	}
    
}
