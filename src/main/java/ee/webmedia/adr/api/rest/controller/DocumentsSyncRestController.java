package ee.webmedia.adr.api.rest.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.webmedia.adr.service.AdrImportService;

@RestController
@RequestMapping(value = "/api/v1/organizations/sync/documents", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class DocumentsSyncRestController {
	
	
    @Autowired
    private AdrImportService adrImportService;

    
	@GetMapping
	public String syncDocuments() {
		adrImportService.importDocumentsSinceLastImportAllOrganizations();
		return "Done at " + new Date();
	}
	
}
