package ee.webmedia.adr.api.rest.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.webmedia.adr.*;
import ee.webmedia.adr.api.rest.controller.*;
import ee.webmedia.adr.dao.*;
import ee.webmedia.adr.model.*;
import ee.webmedia.adr.service.AdrImportService;
import ee.webmedia.adr.service.DigiDocService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service("restApiService")
public class RestApiServiceImpl implements RestApiService {
    protected final Logger log = LoggerFactory.getLogger(getClass());
    protected final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private DocumentTypeMapper documentTypeMapper;

    @Autowired
    private GeneralMapper generalMapper;
    
    @Autowired
    private AdrController adrController;
    
    @Autowired
    private AdrImportService adrImportService;
    
	@Autowired
	private CompoundFunctionMapper compoundFunctionMapper;
	
	@Autowired
	private DigiDocService digiDocService;
	
	@Autowired
	private VolumeMapper volumeMapper;
    
	@Autowired
	private CompoundSeriesMapper compoundSeriesMapper;
	
	@Autowired
	private DocumentRestController documentRestController;
	
	@Autowired
	private VolumeRestController volumeRestController;
	
	@Autowired
	private SeriesRestController seriesRestController;
	
	@Autowired
	private DocumentMapper documentMapper;
	
	@Autowired
	private DocumentAssociationMapper documentAssociationMapper;
	
    private final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    {
        dateFormat.setLenient(false);
    }

    public String encodeBase64(java.io.File file) {
    	try {
			byte[] fileContent = Files.readAllBytes(file.toPath());
			return DatatypeConverter.printBase64Binary(fileContent);
		} catch (IOException e) {
			log.error(e.getMessage());
			return null;
		}
    }
    
    public String encodeBase64(String string) {
    	try {
			return DatatypeConverter.printBase64Binary(string.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage());
			return null;
		}
    }
    
	private String decodeBase64(String base64String) throws UnsupportedEncodingException {
		byte[] decoded = DatatypeConverter.parseBase64Binary(base64String);
		return new String(decoded, "UTF-8");
	}

	public String decodeBase64IfPossible(String base64EncodedString) {
		String decodedString = null;
		try {
			decodedString = decodeBase64(base64EncodedString);
		} catch (UnsupportedEncodingException e) {
			log.error(String.format("Unable to decode base64 (UTF-8) nodeRef %s : %s", decodedString), e.getMessage());
		}
		return decodedString;
	}
	
	public String toJSON(Object object) {
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			String errorMessage = String.format("Unable to convert java object to JSON %s : %s", object, e.getMessage());
			log.error(errorMessage);
			throw new RuntimeException(errorMessage);
		}
	}
	
    public List<DocumentType> getDocTypes(String organizationName, String docTypeName) {
		return getDocTypes(getOrganizationId(organizationName), docTypeName);
	}
	
	public List<DocumentType> getDocTypes(int orgId, String docTypeName) {
		return documentTypeMapper.selectByExample(getDocumentTypeExample(orgId, docTypeName));
	}
	
	public void setupDocumentAssociationProps(DocumentAssociation documentAssociation, Document sourceDocument, Document targetDocument) {
		documentAssociation.setId(generalMapper.defaultSeqNextVal());
		documentAssociation.setSourceDocumentId(sourceDocument.getId());
		documentAssociation.setTargetDocumentId(targetDocument.getId());
		documentAssociation.setCreatedDateTime(new Date());
	}
	
	public void setupDocumentProps(Document document, String docUriBase) {
		document.setUri(createDocumentUri(document, docUriBase));
		document.setDocTypeName(getDocumentTypeName(document));
		Volume volume = volumeMapper.selectByPrimaryKey(document.getVolumeId());
		document.setVolumeNodeRef(volume.getNoderef());
	}
	
	public void setupFunctionProps(CompoundFunction compoundFunction, int orgId) {
		compoundFunction.setId(generalMapper.defaultSeqNextVal());
		compoundFunction.setOrganizationId(orgId);
		Date now = new Date();
		compoundFunction.setCreatedDateTime(now);
		compoundFunction.setModifiedDateTime(now);
	}
	
	public void setupSeriesProps(CompoundSeries compoundSeries, int parentFunctionId) {
		compoundSeries.setId(generalMapper.defaultSeqNextVal());
		Date now = new Date();
		compoundSeries.setCreatedDateTime(now);
		compoundSeries.setModifiedDateTime(now);
		compoundSeries.setCompoundFunctionId(parentFunctionId);
	}
	
	public void setupVolumeProps(Volume volume, int parentSeriesId) {
		volume.setId(generalMapper.defaultSeqNextVal());
		Date now = new Date();
		volume.setCreatedDateTime(now);
		volume.setModifiedDateTime(now);
		volume.setCompoundSeriesId(parentSeriesId);
	}
	
	public void setupDocTypeProps(DocumentType documentType, int orgId) {
		documentType.setId(generalMapper.defaultSeqNextVal());
		documentType.setOrganizationId(orgId);
		Date now = new Date();
		documentType.setCreatedDateTime(now);
		documentType.setModifiedDateTime(now);
	}
	
	public int getOrganizationId(String organizationName) {
		return adrController.getOrganizationId(organizationName);
	}
	
	private String createDocumentUri(Document document, String base) {
		return String.format("https://%s/%s/dokument/%s", base, adrController.getOrganizationName(document.getOrganizationId()), document.getId());
	}
	
	private String getDocumentTypeName(Document document) {
		return documentTypeMapper.selectByPrimaryKey(document.getTypeId()).getName();
	}
	
	public URI createFileUri(File file, String organizationName, String base) {
		String uriPath = String.format("https://%s/%s/fail/%d/%s", base, organizationName, file.getId(), file.getName().replace(" ", "%20"));
		return createLocationUri(uriPath);
	}
	
	public URI createFunctionUri(CompoundFunction compoundFunction, String base) {
		String uriPath = String.format("https://%s/%s/funktsioon/%s", base, adrController.getOrganizationName(compoundFunction.getOrganizationId()), compoundFunction.getId());
		return createLocationUri(uriPath);
	}
	
	public URI createSeriesUri(String organizationName, int seriesId, String base) {
		String uriPath = String.format("https://%s/%s/sari/%s", base, adrController.getOrganizationName(getOrganizationId(organizationName)), seriesId);
		return createLocationUri(uriPath);
	}
	
	public URI createVolumeUri(String organizationName, int volumeId, String base) {
		String uriPath = String.format("https://%s/%s/toimik/%s", base, adrController.getOrganizationName(getOrganizationId(organizationName)), volumeId);
		return createLocationUri(uriPath);
	}
	
	private URI createLocationUri(String uriPath) {
		try {
			return new URI(uriPath);
		} catch (URISyntaxException e) {
			log.error(String.format("Unable to create Uri : %s", uriPath));
			return null;
		}
	}
	
	public List<CompoundFunction> getCompoundFunctions(int organizationId, String mark, String title) {
		CompoundFunctionExample compoundFunctionExample = getCompoundFunctionExample(organizationId, mark, title);
		return compoundFunctionMapper.selectByExample(compoundFunctionExample);
	}
	
	public CompoundFunctionExample getCompoundFunctionExample(int organizationId, String mark, String title) {
		CompoundFunctionExample compoundFunctionExample = new CompoundFunctionExample();
		compoundFunctionExample.createCriteria().andOrganizationIdEqualTo(organizationId).andMarkEqualTo(mark).andTitleEqualTo(title);
		return compoundFunctionExample;
	}
	
	public CompoundFunctionExample getCompoundFunctionExample(int organizationId) {
		CompoundFunctionExample compoundFunctionExample = new CompoundFunctionExample();
		compoundFunctionExample.createCriteria().andOrganizationIdEqualTo(organizationId);
		return compoundFunctionExample;
	}
	
	public DocumentTypeExample getDocumentTypeExample(int organizationId, String docTypeName) {
		DocumentTypeExample documentTypeExample = new DocumentTypeExample();
		documentTypeExample.createCriteria().andOrganizationIdEqualTo(organizationId).andNameEqualTo(docTypeName);
		return documentTypeExample;
	}
	
	public CompoundSeriesExample getSeriesExample(int compoundFunctionId) {
		CompoundSeriesExample compoundSeriesExample = new CompoundSeriesExample();
		compoundSeriesExample.createCriteria().andCompoundFunctionIdEqualTo(compoundFunctionId);
		return compoundSeriesExample;
	}
	
	public CompoundSeriesExample getSeriesExample(int compoundFunctionId, String mark, String title) {
		CompoundSeriesExample compoundSeriesExample = new CompoundSeriesExample();
		compoundSeriesExample.createCriteria().andCompoundFunctionIdEqualTo(compoundFunctionId).andMarkEqualTo(mark).andTitleEqualTo(title);
		return compoundSeriesExample;
	}
	
	public Volume getVolume(int organizationId, String volumeNodeRef) {
		VolumeExample volumeExample = new VolumeExample();
		volumeExample.createCriteria().andNoderefEqualTo(volumeNodeRef);
		List<Volume> volumes = volumeMapper.selectByExample(volumeExample);
		for (Volume volume : volumes) {
			CompoundSeries compoundSeries = compoundSeriesMapper.selectByPrimaryKey(volume.getCompoundSeriesId());
			if (compoundFunctionMapper.selectByPrimaryKey(compoundSeries.getCompoundFunctionId()).getOrganizationId().intValue() == organizationId) {
				return volume;
			}
		}
		return null;
	}
	
	public VolumeExample getVolumeExample(int compoundSeriesId) {
		VolumeExample volumeExample = new VolumeExample();
		volumeExample.createCriteria().andCompoundSeriesIdEqualTo(compoundSeriesId);
		return volumeExample;
	}
	
	public VolumeExample getVolumeExample(int compoundSeriesId, String nodeRef) {
		VolumeExample volumeExample = new VolumeExample();
		volumeExample.createCriteria().andCompoundSeriesIdEqualTo(compoundSeriesId).andNoderefEqualTo(nodeRef);
		return volumeExample;
	}
	
	public List<DocumentAssociation> getDocumentAssociations(int sourceDocumentId, int targetDocumentId) {
		DocumentAssociationExample documentAssociationExample = new DocumentAssociationExample();
		documentAssociationExample.createCriteria().andSourceDocumentIdEqualTo(sourceDocumentId).andTargetDocumentIdEqualTo(targetDocumentId);
		return documentAssociationMapper.selectByExample(documentAssociationExample);
	}
	
	public File createFileModel(int documentId, MultipartFile multipartFile) {
		int id = generalMapper.defaultSeqNextVal();
		File file = new File();
        Date now = new Date();
        file.setId(id);
        file.setNoderef(String.valueOf(id));
        file.setCreatedDateTime(now);
        file.setModifiedDateTime(now);
        file.setFileModifiedDateTime(now);
        file.setDocumentId(documentId);
        file.setName(multipartFile.getOriginalFilename());
        file.setTitle(multipartFile.getOriginalFilename());
        file.setSize((int) multipartFile.getSize());
        file.setMimeType(multipartFile.getContentType());
        file.setEncoding("UTF-8");

        return file;
	}
	
    public List<File> getFiles(Integer documentId) {
    	return adrImportService.getFiles(documentId);
    }

	public void setContainerFileNames(File file, int organizationId) {
		String fileName = file.getName();
		if (ContainerFileHelper.isDigiDocFile(fileName)) {
			List<String> filesNames = digiDocService.getFilesNamesFromContainer(file, organizationId);
			List<ContainerFile> containerFiles = ContainerFileHelper.getContainerFiles(file, filesNames);
			if (!ContainerFileHelper.hasOnlyPublicAccessFiles(containerFiles)) {
				file.setName("File");
				file.setTitle("File");
				file.setPublicAccess(false);
			}
			file.setContainerFiles(containerFiles);
		} else {
			if (file.isPrivateAccess()) {
				file.setName("File");
				file.setTitle("File");
				file.setPublicAccess(false);
			}
		}
	}
    
    public void deleteDocuments(String organizationName, VolumeExample parentVolumeExample) {
		List<Volume> volumes = volumeMapper.selectByExample(parentVolumeExample);
		for (Volume volume : volumes) {
			DocumentExample documentExample = new DocumentExample();
			documentExample.createCriteria().andOrganizationIdEqualTo(getOrganizationId(organizationName)).andVolumeIdEqualTo(volume.getId());
			List<Document> documents = documentMapper.selectByExample(documentExample);
			for (Document document : documents) {
				documentRestController.deleteDocument(organizationName, encodeBase64(document.getNoderef()));
			}
		}
    }
    
    public void deleteVolumes(String organizationName, CompoundFunction compoundFunction, CompoundSeries compoundSeries) {
    	volumeRestController.deleteAllVolumes(organizationName, compoundFunction.getMark(), compoundFunction.getTitle(), compoundSeries.getMark(), compoundSeries.getTitle());
    }
    
    public void deleteSeries(String organizationName, CompoundFunction compoundFunction) {
    	seriesRestController.deleteAllSeries(organizationName, compoundFunction.getMark(), compoundFunction.getTitle());
    }

}
