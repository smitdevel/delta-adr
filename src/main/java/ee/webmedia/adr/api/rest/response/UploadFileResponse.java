package ee.webmedia.adr.api.rest.response;

import java.net.URI;

public class UploadFileResponse {

    private int fileId;
    private URI fileDownloadUri;
    
	public UploadFileResponse(int fileId, URI uri) {
		this.fileId = fileId;
		this.fileDownloadUri = uri;
	}
	
	public int getFileId() {
		return fileId;
	}
	public void setFileId(int fileId) {
		this.fileId = fileId;
	}
	public URI getFileDownloadUri() {
		return fileDownloadUri;
	}
	public void setFileDownloadUri(URI fileDownloadUri) {
		this.fileDownloadUri = fileDownloadUri;
	}
}
