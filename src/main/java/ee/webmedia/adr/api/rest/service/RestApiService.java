package ee.webmedia.adr.api.rest.service;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import ee.webmedia.adr.model.CompoundFunction;
import ee.webmedia.adr.model.CompoundFunctionExample;
import ee.webmedia.adr.model.CompoundSeries;
import ee.webmedia.adr.model.CompoundSeriesExample;
import ee.webmedia.adr.model.Document;
import ee.webmedia.adr.model.DocumentAssociation;
import ee.webmedia.adr.model.DocumentType;
import ee.webmedia.adr.model.DocumentTypeExample;
import ee.webmedia.adr.model.File;
import ee.webmedia.adr.model.Volume;
import ee.webmedia.adr.model.VolumeExample;

public interface RestApiService {
	
	public static final String UNABLE_TO_DECODE_NODEREF = "Unable to decode base64 (UTF-8) nodeRef";

	public static final String UNABLE_TO_DECODE_MARK_OR_TITLE = "Unable to decode base64 (UTF-8) mark or title";
	
	String encodeBase64(String string);
	
	String encodeBase64(java.io.File file);

    String decodeBase64IfPossible(String base64EncodedString);

    String toJSON(Object object);

    int getOrganizationId(String organizationName);

    URI createFunctionUri(CompoundFunction compoundFunction, String base);

    URI createSeriesUri(String organizationName, int seriesId, String base);

    URI createVolumeUri(String organizationName, int volumeId, String base);
    
    URI createFileUri(File file, String organizationName, String base);
    
    void setupDocumentAssociationProps(DocumentAssociation documentAssociation, Document sourceDocument, Document targetDocument);

    void setupDocumentProps(Document document, String docUriBase);

    void setupDocTypeProps(DocumentType documentType, int orgId);

    void setupFunctionProps(CompoundFunction compoundFunction, int orgId);

    void setupSeriesProps(CompoundSeries compoundSeries, int parentFunctionId);

    void setupVolumeProps(Volume volume, int parentSeriesId);

    List<DocumentType> getDocTypes(String organizationName, String docTypeName);

    List<DocumentType> getDocTypes(int orgId, String docTypeName);

    DocumentTypeExample getDocumentTypeExample(int organizationId, String docTypeName);

    List<CompoundFunction> getCompoundFunctions(int organizationId, String mark, String title);
    
    CompoundFunctionExample getCompoundFunctionExample(int organizationId);

    CompoundFunctionExample getCompoundFunctionExample(int organizationId, String mark, String title);

    CompoundSeriesExample getSeriesExample(int compoundFunctionId);

    CompoundSeriesExample getSeriesExample(int compoundFunctionId, String mark, String title);
    
    Volume getVolume(int organizationId, String volumeNodeRef);
    
    VolumeExample getVolumeExample(int compoundSeriesId);

    VolumeExample getVolumeExample(int compoundSeriesId, String nodeRef);
    
    List<DocumentAssociation> getDocumentAssociations(int sourceDocumentId, int targetDocumentId);
    
    void deleteDocuments(String organizationName, VolumeExample parentVolumeExample);

    List<File> getFiles(Integer documentId);

    File createFileModel(int documentId, MultipartFile multipartFile);
    
    void setContainerFileNames(File file, int organizationId);
    
    void deleteVolumes(String organizationName, CompoundFunction compoundFunction, CompoundSeries compoundSeries);
    
    void deleteSeries(String organizationName, CompoundFunction compoundFunction);
    
}
