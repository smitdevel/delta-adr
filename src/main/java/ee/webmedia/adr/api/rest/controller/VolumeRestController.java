package ee.webmedia.adr.api.rest.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.webmedia.adr.api.rest.service.RestApiService;
import ee.webmedia.adr.dao.CompoundSeriesMapper;
import ee.webmedia.adr.dao.VolumeMapper;
import ee.webmedia.adr.model.CompoundFunction;
import ee.webmedia.adr.model.CompoundSeries;
import ee.webmedia.adr.model.Volume;
import ee.webmedia.adr.model.VolumeExample;

@RestController
@RequestMapping(value = "/api/v1/organizations/{organizationName}/functions/{functionMark}/{functionTitle}/series/{seriesMark}/{seriesTitle}/volumes", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class VolumeRestController {
	
	private final String NODEREF_AND_MARK_AND_TITLE_AND_VALIDFROM_AND_VALIDTO_CAN_NOT_BE_NULL = "Fields noderef, mark, title, validFromDate, validToDate can not be null";
	private final String PARENT_SERIES_AND_NODEREF_COMBINATION_ALREADY_EXIST = "Volumes with this parentSeriesId and noderef already exist";

	@Autowired
	private RestApiService restApiService;
	
	@Autowired
	private CompoundSeriesMapper compoundSeriesMapper;
	
	@Autowired
	private VolumeMapper volumeMapper;
	
	@PostMapping
	public ResponseEntity<String> createVolume(@PathVariable String organizationName, @PathVariable String functionMark,
			@PathVariable String functionTitle, @PathVariable String seriesMark, @PathVariable String seriesTitle, @RequestBody Volume volume, HttpServletRequest request) {
		functionMark = restApiService.decodeBase64IfPossible(functionMark);
		functionTitle = restApiService.decodeBase64IfPossible(functionTitle);
		seriesTitle = restApiService.decodeBase64IfPossible(seriesTitle);
		seriesMark = restApiService.decodeBase64IfPossible(seriesMark);
		if (functionMark == null || functionTitle == null || seriesMark == null || seriesTitle == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(restApiService.getOrganizationId(organizationName), functionMark, functionTitle);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		CompoundFunction compoundFunction = compoundFunctions.get(0);
		List<CompoundSeries> compoundSeriesList = compoundSeriesMapper.selectByExample(restApiService.getSeriesExample(compoundFunction.getId(), seriesMark, seriesTitle));
		if (compoundSeriesList.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		CompoundSeries compoundSeries = compoundSeriesList.get(0);
		restApiService.setupVolumeProps(volume, compoundSeries.getId());
		if (volume.getNoderef() == null || volume.getMark() == null || volume.getTitle() == null || volume.getValidFromDate() == null || volume.getValidToDate() == null) {
			return ResponseEntity.badRequest().body(NODEREF_AND_MARK_AND_TITLE_AND_VALIDFROM_AND_VALIDTO_CAN_NOT_BE_NULL);
		}
		if (!volumeMapper.selectByExample(restApiService.getVolumeExample(volume.getCompoundSeriesId(), volume.getNoderef())).isEmpty()) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(PARENT_SERIES_AND_NODEREF_COMBINATION_ALREADY_EXIST);
		}
		volumeMapper.insert(volume);
		return ResponseEntity.created(restApiService.createVolumeUri(organizationName, volume.getId(), request.getServerName())).build();
	}
	
	@PutMapping("/{nodeRef}")
	public ResponseEntity<String> updateFunction(@PathVariable String organizationName, @PathVariable String functionMark,
			@PathVariable String functionTitle, @PathVariable String seriesMark, @PathVariable String seriesTitle, @PathVariable String nodeRef, @RequestBody Volume record,  HttpServletRequest request) {
		functionMark = restApiService.decodeBase64IfPossible(functionMark);
		functionTitle = restApiService.decodeBase64IfPossible(functionTitle);
		seriesTitle = restApiService.decodeBase64IfPossible(seriesTitle);
		seriesMark = restApiService.decodeBase64IfPossible(seriesMark);
		if (functionMark == null || functionTitle == null || seriesMark == null || seriesTitle == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		String decodedNodeRef = restApiService.decodeBase64IfPossible(nodeRef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		int orgId = restApiService.getOrganizationId(organizationName);
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(orgId, functionMark, functionTitle);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		List<CompoundSeries> compoundSeriesList = compoundSeriesMapper.selectByExample(restApiService.getSeriesExample(compoundFunctions.get(0).getId(), seriesMark, seriesTitle));
		if (compoundSeriesList.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		CompoundSeries compoundSeries = compoundSeriesList.get(0);
		List<Volume> volumes = volumeMapper.selectByExample(restApiService.getVolumeExample(compoundSeries.getId()));
		if (volumes.isEmpty()) {
			return createVolume(organizationName, functionMark, functionTitle, seriesMark, seriesTitle, record, request);
		}
		Volume volume = volumes.get(0);
		record.setId(volume.getId());
		record.setCompoundSeriesId(volume.getCompoundSeriesId());
		record.setNoderef(volume.getNoderef());
		record.setCreatedDateTime(volume.getCreatedDateTime());
		record.setModifiedDateTime(new Date());
		int updated = volumeMapper.updateByPrimaryKey(record);
		if (updated == 0) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().build();
	}
	
	@GetMapping
	public ResponseEntity<String> getAllVolumes(@PathVariable String organizationName, @PathVariable String functionMark,
			@PathVariable String functionTitle, @PathVariable String seriesMark, @PathVariable String seriesTitle) {
		functionMark = restApiService.decodeBase64IfPossible(functionMark);
		functionTitle = restApiService.decodeBase64IfPossible(functionTitle);
		seriesTitle = restApiService.decodeBase64IfPossible(seriesTitle);
		seriesMark = restApiService.decodeBase64IfPossible(seriesMark);
		if (functionMark == null || functionTitle == null || seriesMark == null || seriesTitle == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(restApiService.getOrganizationId(organizationName), functionMark, functionTitle);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		CompoundFunction compoundFunction = compoundFunctions.get(0);
		List<CompoundSeries> compoundSeries = compoundSeriesMapper.selectByExample(restApiService.getSeriesExample(compoundFunction.getId(), seriesMark, seriesTitle));
		if (compoundSeries.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		List<Volume> volumes = volumeMapper.selectByExample(restApiService.getVolumeExample(compoundSeries.get(0).getId()));
		return ResponseEntity.ok(restApiService.toJSON(volumes));	
	}
	
	@GetMapping("/{nodeRef}")
	public ResponseEntity<String> getVolumes(@PathVariable String organizationName, @PathVariable String functionMark,
			@PathVariable String functionTitle, @PathVariable String seriesMark, @PathVariable String seriesTitle, @PathVariable String nodeRef) {
		functionMark = restApiService.decodeBase64IfPossible(functionMark);
		functionTitle = restApiService.decodeBase64IfPossible(functionTitle);
		seriesTitle = restApiService.decodeBase64IfPossible(seriesTitle);
		seriesMark = restApiService.decodeBase64IfPossible(seriesMark);
		if (functionMark == null || functionTitle == null || seriesMark == null || seriesTitle == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		String decodedNodeRef = restApiService.decodeBase64IfPossible(nodeRef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(restApiService.getOrganizationId(organizationName), functionMark, functionTitle);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		CompoundFunction compoundFunction = compoundFunctions.get(0);
		List<CompoundSeries> compoundSeries = compoundSeriesMapper.selectByExample(restApiService.getSeriesExample(compoundFunction.getId(), seriesMark, seriesTitle));
		if (compoundSeries.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		List<Volume> volumes = volumeMapper.selectByExample(restApiService.getVolumeExample(compoundSeries.get(0).getId(), decodedNodeRef));
		if (volumes.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(restApiService.toJSON(volumes));	
	}
	
	@DeleteMapping("/{nodeRef}")
	public ResponseEntity<String> deleteVolume(@PathVariable String organizationName, @PathVariable String functionMark,
			@PathVariable String functionTitle, @PathVariable String seriesMark, @PathVariable String seriesTitle, @PathVariable String nodeRef) {
		functionMark = restApiService.decodeBase64IfPossible(functionMark);
		functionTitle = restApiService.decodeBase64IfPossible(functionTitle);
		seriesTitle = restApiService.decodeBase64IfPossible(seriesTitle);
		seriesMark = restApiService.decodeBase64IfPossible(seriesMark);
		if (functionMark == null || functionTitle == null || seriesMark == null || seriesTitle == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		String decodedNodeRef = restApiService.decodeBase64IfPossible(nodeRef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		int orgId = restApiService.getOrganizationId(organizationName);
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(orgId, functionMark, functionTitle);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		CompoundFunction compoundFunction = compoundFunctions.get(0);
		List<CompoundSeries> compoundSeries = compoundSeriesMapper.selectByExample(restApiService.getSeriesExample(compoundFunction.getId(), seriesMark, seriesTitle));
		if (compoundSeries.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		VolumeExample volumeExample = restApiService.getVolumeExample(compoundSeries.get(0).getId(), decodedNodeRef);
		restApiService.deleteDocuments(organizationName, volumeExample);

		int deleted = volumeMapper.deleteByExample(volumeExample);
		if (deleted == 0) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping
	public ResponseEntity<String> deleteAllVolumes(@PathVariable String organizationName, @PathVariable String functionMark,
			@PathVariable String functionTitle, @PathVariable String seriesMark, @PathVariable String seriesTitle) {

		int orgId = restApiService.getOrganizationId(organizationName);
		functionMark = restApiService.decodeBase64IfPossible(functionMark);
		functionTitle = restApiService.decodeBase64IfPossible(functionTitle);
		seriesTitle = restApiService.decodeBase64IfPossible(seriesTitle);
		seriesMark = restApiService.decodeBase64IfPossible(seriesMark);
		if (functionMark == null || functionTitle == null || seriesMark == null || seriesTitle == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(orgId, functionMark, functionTitle);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		CompoundFunction compoundFunction = compoundFunctions.get(0);
		List<CompoundSeries> compoundSeries = compoundSeriesMapper.selectByExample(restApiService.getSeriesExample(compoundFunction.getId(), seriesMark, seriesTitle));
		if (compoundSeries.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		
		VolumeExample volumeExample = restApiService.getVolumeExample(compoundSeries.get(0).getId());
		List<Volume> volumes = volumeMapper.selectByExample(volumeExample);
		if (volumes.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		for (Volume volume : volumes) {
			deleteVolume(organizationName, functionMark, functionTitle, seriesMark, seriesTitle, restApiService.encodeBase64(volume.getNoderef()));
		}

		return ResponseEntity.noContent().build();
	}
}
