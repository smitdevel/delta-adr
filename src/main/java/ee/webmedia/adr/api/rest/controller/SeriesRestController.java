package ee.webmedia.adr.api.rest.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.webmedia.adr.api.rest.service.RestApiService;
import ee.webmedia.adr.dao.CompoundSeriesMapper;
import ee.webmedia.adr.model.CompoundFunction;
import ee.webmedia.adr.model.CompoundSeries;

@RestController
@RequestMapping(value = "/api/v1/organizations/{organizationName}/functions/{functionMark}/{functionTitle}/series", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class SeriesRestController {
	
	private final String PARENT_FUNCTION_AND_MARK_AND_TITLE_COMBINATION_ALREADY_EXIST = "Series with this parent function, mark and title already exist";

	@Autowired
	private RestApiService restApiService;
	
	@Autowired
	private CompoundSeriesMapper compoundSeriesMapper;

	@PostMapping
	public ResponseEntity<String> createSeries(@PathVariable String organizationName, @PathVariable String functionMark, @PathVariable String functionTitle,
			@RequestBody CompoundSeries compoundSeries, HttpServletRequest request) {
		functionMark = restApiService.decodeBase64IfPossible(functionMark);
		functionTitle = restApiService.decodeBase64IfPossible(functionTitle);
		if (functionMark == null || functionTitle == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		int orgId = restApiService.getOrganizationId(organizationName);
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(orgId, functionMark, functionTitle);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		int parentFunctionId = compoundFunctions.get(0).getId();
		restApiService.setupSeriesProps(compoundSeries, parentFunctionId);
		
		if (!compoundSeriesMapper.selectByExample(restApiService.getSeriesExample(compoundSeries.getCompoundFunctionId(), compoundSeries.getMark(), compoundSeries.getTitle())).isEmpty()) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(PARENT_FUNCTION_AND_MARK_AND_TITLE_COMBINATION_ALREADY_EXIST);
		}

		compoundSeriesMapper.insert(compoundSeries);
		return ResponseEntity.created(restApiService.createSeriesUri(organizationName, parentFunctionId, request.getServerName())).build();
	}
	
	@GetMapping
	public ResponseEntity<String> getAllSeries(@PathVariable String organizationName, @PathVariable String functionMark, @PathVariable String functionTitle) {
		functionMark = restApiService.decodeBase64IfPossible(functionMark);
		functionTitle = restApiService.decodeBase64IfPossible(functionTitle);
		if (functionMark == null || functionTitle == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(restApiService.getOrganizationId(organizationName), functionMark, functionTitle);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		CompoundFunction compoundFunction = compoundFunctions.get(0);
		return ResponseEntity.ok(restApiService.toJSON(compoundSeriesMapper.selectByExample(restApiService.getSeriesExample(compoundFunction.getId()))));
	}
	
	@PutMapping("/{seriesMark}/{seriesTitle}")
	public ResponseEntity<String> updateFunction(@PathVariable String organizationName, @PathVariable String functionMark,
			@PathVariable String functionTitle, @PathVariable String seriesMark, @PathVariable String seriesTitle, @RequestBody CompoundSeries record,  HttpServletRequest request) {
		int orgId = restApiService.getOrganizationId(organizationName);
		functionMark = restApiService.decodeBase64IfPossible(functionMark);
		functionTitle = restApiService.decodeBase64IfPossible(functionTitle);
		seriesTitle = restApiService.decodeBase64IfPossible(seriesTitle);
		seriesMark = restApiService.decodeBase64IfPossible(seriesMark);
		if (functionMark == null || functionTitle == null || seriesMark == null || seriesTitle == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(orgId, functionMark, functionTitle);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		record.setMark(seriesMark);
		record.setTitle(seriesTitle);
		List<CompoundSeries> compoundSeriesList = compoundSeriesMapper.selectByExample(restApiService.getSeriesExample(compoundFunctions.get(0).getId(), seriesMark, seriesTitle));
		if (compoundSeriesList.isEmpty()) {
			return createSeries(organizationName, functionMark, functionTitle, record, request);
		}
		CompoundSeries compoundSeries = compoundSeriesList.get(0);
		record.setId(compoundSeries.getId());
		record.setCompoundFunctionId(compoundSeries.getCompoundFunctionId());
		record.setCreatedDateTime(compoundSeries.getCreatedDateTime());
		record.setModifiedDateTime(new Date());
		int updated = compoundSeriesMapper.updateByPrimaryKey(record);
		if (updated == 0) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{seriesMark}/{seriesTitle}")
	public ResponseEntity<String> getSeries(@PathVariable String organizationName, @PathVariable String functionMark,
			@PathVariable String functionTitle, @PathVariable String seriesMark, @PathVariable String seriesTitle) {
		functionMark = restApiService.decodeBase64IfPossible(functionMark);
		functionTitle = restApiService.decodeBase64IfPossible(functionTitle);
		seriesTitle = restApiService.decodeBase64IfPossible(seriesTitle);
		seriesMark = restApiService.decodeBase64IfPossible(seriesMark);
		if (functionMark == null || functionTitle == null || seriesMark == null || seriesTitle == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(restApiService.getOrganizationId(organizationName), functionMark, functionTitle);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		CompoundFunction compoundFunction = compoundFunctions.get(0);
		List<CompoundSeries> compoundSeries = compoundSeriesMapper.selectByExample(restApiService.getSeriesExample(compoundFunction.getId(), seriesMark, seriesTitle));
		if (compoundSeries.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(restApiService.toJSON(compoundSeries.get(0)));	
	}

	@DeleteMapping("/{seriesMark}/{seriesTitle}")
	public ResponseEntity<String> deleteSeries(@PathVariable String organizationName, @PathVariable String functionMark,
			@PathVariable String functionTitle, @PathVariable String seriesMark, @PathVariable String seriesTitle) {
		functionMark = restApiService.decodeBase64IfPossible(functionMark);
		functionTitle = restApiService.decodeBase64IfPossible(functionTitle);
		seriesTitle = restApiService.decodeBase64IfPossible(seriesTitle);
		seriesMark = restApiService.decodeBase64IfPossible(seriesMark);
		if (functionMark == null || functionTitle == null || seriesMark == null || seriesTitle == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(restApiService.getOrganizationId(organizationName), functionMark, functionTitle);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		CompoundFunction compoundFunction = compoundFunctions.get(0);
		List<CompoundSeries> compoundSeries = compoundSeriesMapper.selectByExample(restApiService.getSeriesExample(compoundFunction.getId(), seriesMark, seriesTitle));
		if (!compoundSeries.isEmpty()) {
			restApiService.deleteVolumes(organizationName, compoundFunction, compoundSeries.get(0));
		}
		
		int deleted = compoundSeriesMapper.deleteByExample(restApiService.getSeriesExample(compoundFunction.getId(), seriesMark, seriesTitle));
		if (deleted == 0) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping
	public ResponseEntity<String> deleteAllSeries(@PathVariable String organizationName, @PathVariable String functionMark,
			@PathVariable String functionTitle) {
		functionMark = restApiService.decodeBase64IfPossible(functionMark);
		functionTitle = restApiService.decodeBase64IfPossible(functionTitle);
		if (functionMark == null || functionTitle == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(restApiService.getOrganizationId(organizationName), functionMark, functionTitle);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		CompoundFunction compoundFunction = compoundFunctions.get(0);
		List<CompoundSeries> allCompoundSeries = compoundSeriesMapper.selectByExample(restApiService.getSeriesExample(compoundFunction.getId()));
		if (allCompoundSeries.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		for (CompoundSeries compoundSeries : allCompoundSeries) {
			deleteSeries(organizationName, compoundFunction.getMark(), compoundFunction.getTitle(), compoundSeries.getMark(), compoundSeries.getTitle());
		}
		return ResponseEntity.noContent().build();
	}

}
