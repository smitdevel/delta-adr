package ee.webmedia.adr.api.rest.controller;

import ee.webmedia.adr.api.rest.service.RestApiService;
import ee.webmedia.adr.dao.DocumentMapper;
import ee.webmedia.adr.model.*;
import ee.webmedia.adr.service.AdrImportService;
import ee.webmedia.adr.ws.DokumentDetailidegaV2;
import org.apache.commons.lang.StringUtils;
import org.owasp.encoder.Encode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = {"/api/v1/organizations/{organizationName}/documents", "/api/organizations/{organizationName}/documents"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class DocumentRestController {
	
	private final String NODEREF_MUST_BE_UNIQUE = "Unable to create document. Field noderef must be unique.";
	
	private final String VOLUME_NOT_EXIST = "Volume with this volumeNodeRef does not exist";
	
	@Autowired
	private RestApiService restApiService;

    @Autowired
    private AdrImportService adrImportService;

    @Autowired
    private DocumentMapper documentMapper;
    
	@PostMapping
	public ResponseEntity<String> createDocument(@PathVariable String organizationName, @RequestBody Document document, HttpServletRequest request) {
		int orgId = restApiService.getOrganizationId(organizationName);
		List<DocumentType> docTypes = restApiService.getDocTypes(orgId, document.getDocTypeName());
		Document doc = adrImportService.getDocument(orgId, document.getNoderef());
		if (doc != null) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(NODEREF_MUST_BE_UNIQUE);
		}
		if (document.getNoderef() == null) {
			return ResponseEntity.badRequest().body(VOLUME_NOT_EXIST);
		}
		Volume volume = restApiService.getVolume(orgId, document.getVolumeNodeRef());
		if (volume == null) {
			return ResponseEntity.badRequest().body(VOLUME_NOT_EXIST);
		}
		document.setVolumeId(volume.getId());
		if (docTypes.isEmpty()) {
			return ResponseEntity.badRequest().body(String.format("Unable to create document. DocumentType %s is not exist", Encode.forHtml(document.getDocTypeName())));
		}
		document.setTypeId(docTypes.get(0).getId());
		try {
			URI documentLocation = adrImportService.createDocument(orgId, document, request);
			return ResponseEntity.created(documentLocation).build();
		} catch (DataIntegrityViolationException | URISyntaxException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	@GetMapping
	public ResponseEntity<String> getAllDocuments(@PathVariable String organizationName, HttpServletRequest request) {
		List<Document> docs = adrImportService.getDocuments(restApiService.getOrganizationId(organizationName));
		for (Document doc : docs) {
			restApiService.setupDocumentProps(doc, request.getServerName());
		}
		return ResponseEntity.ok().body(restApiService.toJSON(docs));
	}

	@GetMapping("/{nodeRef}")
	public ResponseEntity<String> getDocumentByNodeRef(@PathVariable String organizationName, @PathVariable String nodeRef, HttpServletRequest request) {
		String decodedNodeRef = restApiService.decodeBase64IfPossible(nodeRef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		Document doc = adrImportService.getDocument(restApiService.getOrganizationId(organizationName), decodedNodeRef);
		if (doc == null) {
			return ResponseEntity.notFound().build();
		}
		restApiService.setupDocumentProps(doc, request.getServerName());
		return ResponseEntity.ok(restApiService.toJSON(doc));
	}
	
	@PutMapping("/{nodeRef}")
	public ResponseEntity<String> updateDocument(@PathVariable String organizationName, @PathVariable String nodeRef, @RequestBody Document record, HttpServletRequest request) {
		String decodedNodeRef = restApiService.decodeBase64IfPossible(nodeRef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		int orgId = restApiService.getOrganizationId(organizationName);
		Document document = adrImportService.getDocument(orgId, decodedNodeRef);
		record.setNoderef(decodedNodeRef);

		if (document == null) {
			return createDocument(organizationName, record, request);
		}
		record.setId(document.getId());
		record.setOrganizationId(orgId);
		List<DocumentType> docTypes = restApiService.getDocTypes(orgId, record.getDocTypeName());
		if (docTypes.isEmpty()) {
			return ResponseEntity.badRequest().body(String.format("Unable to create document. Field DocumentType with name %s is not exist", Encode.forHtml(record.getDocTypeName())));
		}
		record.setTypeId(docTypes.get(0).getId());
		record.setCreatedDateTime(document.getCreatedDateTime());
		record.setModifiedDateTime(new Date());
		Volume volume = restApiService.getVolume(orgId, record.getVolumeNodeRef());
		if (volume == null) {
			return ResponseEntity.badRequest().body(VOLUME_NOT_EXIST);
		}
		record.setVolumeId(volume.getId());
		try {
			documentMapper.updateByPrimaryKey(record);
			return ResponseEntity.ok().build();
		} catch (DataIntegrityViolationException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}

	}

	@DeleteMapping("/{nodeRef}")
	public ResponseEntity<String> deleteDocument(@PathVariable String organizationName, @PathVariable String nodeRef) {
		String decodedNodeRef = restApiService.decodeBase64IfPossible(nodeRef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		boolean isDeleted = adrImportService.deleteDocument(restApiService.getOrganizationId(organizationName), decodedNodeRef);
		if (!isDeleted) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.noContent().build();
	}

	@PostMapping("/single")
	public ResponseEntity<String> addDocument(@PathVariable String organizationName, @RequestBody DokumentDetailidegaV2 wsDocument) {
		String totalDocumentNumberPadded = StringUtils.leftPad("1", 1, '0');
		try {
			adrImportService.importDocument(restApiService.getOrganizationId(organizationName), wsDocument, new Date(), totalDocumentNumberPadded);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
	}
	
	@DeleteMapping
	public ResponseEntity<String> deleteAllDocument(@PathVariable String organizationName) {
		List<Document> docs = adrImportService.getDocuments(restApiService.getOrganizationId(organizationName));
		for (Document doc : docs) {
			adrImportService.deleteDocument(restApiService.getOrganizationId(organizationName), doc.getNoderef());
		}
		if (docs.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.noContent().build();
	}
}
