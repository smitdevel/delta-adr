package ee.webmedia.adr.api.rest.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.webmedia.adr.api.rest.service.RestApiService;
import ee.webmedia.adr.dao.DocumentTypeMapper;
import ee.webmedia.adr.model.DocumentType;
import ee.webmedia.adr.model.DocumentTypeExample;

@RestController
@RequestMapping(value = "/api/v1/organizations/{organizationName}/document-types", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class DocumentTypeRestController {
	
	private final String DOCTYPE_ALREADY_EXISTS = "DocumentType already exists. Field name must be unique.";
	@Autowired
	private RestApiService restApiService;
	
	@Autowired
	private DocumentTypeMapper documentTypeMapper;

	@PostMapping
	public ResponseEntity<String> createDocumentType(@PathVariable String organizationName, @RequestBody DocumentType documentType) {
		int orgId = restApiService.getOrganizationId(organizationName);
		List<DocumentType> docTypes = documentTypeMapper.selectByExample(restApiService.getDocumentTypeExample(orgId, documentType.getName()));
		if (!docTypes.isEmpty()) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(DOCTYPE_ALREADY_EXISTS);
		}
		restApiService.setupDocTypeProps(documentType, orgId);
        
		documentTypeMapper.insert(documentType);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}
	
	@GetMapping
	public ResponseEntity<String> getAllDocTypes(@PathVariable String organizationName) {
		int orgId = restApiService.getOrganizationId(organizationName);
		DocumentTypeExample documentTypeExample = new DocumentTypeExample();
		documentTypeExample.createCriteria().andOrganizationIdEqualTo(orgId);
		List<DocumentType> docTypes = documentTypeMapper.selectByExample(documentTypeExample);
		return ResponseEntity.ok(restApiService.toJSON(docTypes));
	}
	
	@GetMapping("/{docTypeName}")
	public ResponseEntity<String> getDocType(@PathVariable String organizationName, @PathVariable String docTypeName) {
		List<DocumentType> docTypes = restApiService.getDocTypes(organizationName, docTypeName);
		if (docTypes.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(restApiService.toJSON(docTypes.get(0)));
	}
	
	@PutMapping("/{docTypeName}")
	public ResponseEntity<String> updateDocumentType(@PathVariable String organizationName, @PathVariable String docTypeName, @RequestBody DocumentType record) {
		int orgId = restApiService.getOrganizationId(organizationName);
		List<DocumentType> docTypes = restApiService.getDocTypes(orgId, docTypeName);
		record.setName(docTypeName);

		if (docTypes.isEmpty()) {
			return createDocumentType(organizationName, record);
		}
		DocumentType documentType = docTypes.get(0);
		record.setId(documentType.getId());
		record.setOrganizationId(orgId);
		record.setCreatedDateTime(documentType.getCreatedDateTime());
		record.setModifiedDateTime(new Date());

		int updated = documentTypeMapper.updateByPrimaryKey(record);
		if (updated == 0) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().build();
	}
	
	@DeleteMapping("/{docTypeName}")
	public ResponseEntity<String> deleteDocType(@PathVariable String organizationName, @PathVariable String docTypeName) {
		DocumentTypeExample documentTypeExample = restApiService.getDocumentTypeExample(restApiService.getOrganizationId(organizationName), docTypeName);
		int deleted = documentTypeMapper.deleteByExample(documentTypeExample);
		if (deleted == 0) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping
	public ResponseEntity<String> deleteAllDocType(@PathVariable String organizationName) {
		int orgId = restApiService.getOrganizationId(organizationName);
		DocumentTypeExample documentTypeExample = new DocumentTypeExample();
		documentTypeExample.createCriteria().andOrganizationIdEqualTo(orgId);
		int deleted = documentTypeMapper.deleteByExample(documentTypeExample);
		if (deleted == 0) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.noContent().build();
	}
}
