package ee.webmedia.adr.api.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.webmedia.adr.api.rest.service.RestApiService;
import ee.webmedia.adr.dao.DocumentAssociationMapper;
import ee.webmedia.adr.dao.DocumentMapper;
import ee.webmedia.adr.model.Document;
import ee.webmedia.adr.model.DocumentAssociation;
import ee.webmedia.adr.model.DocumentAssociationExample;
import ee.webmedia.adr.service.AdrImportService;

@RestController
@RequestMapping(value = "/api/v1/organizations/{organizationName}/documents/{sourceDocNoderef}/document-associations", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class DocumentAssociationRestController {
	
	private final String TARGET_DOCUMENT_NODEREF_CAN_NOT_BE_NULL = "Target document noderef can not be null";
	private final String TARGET_DOCUMENT_NOT_EXISTS = "Target document is not exists";
	private final String SOURCE_AND_TARGET_DOCUMENTS_EQUALS = "Source and target documents are equals";
	private final String ASSOCIATION_ALREADY_EXISTS = "Association with this sourceDocumentId and targetDocumentId already exists";
	private final String INVALID_ASSOCIATION_TYPE = "Invalid association type. Avaliable types [DEFAULT, REPLY, FOLLOWUP]";
	
	@Autowired
	private RestApiService restApiService;
	
	@Autowired
	private AdrImportService adrImportService;
	
	@Autowired
	private DocumentAssociationMapper documentAssociationMapper;
	
	@Autowired
	private DocumentMapper documentMapper;
	
	@PostMapping
	public ResponseEntity<String> createDocumentAssociation(@PathVariable String organizationName, @PathVariable String sourceDocNoderef, @RequestBody DocumentAssociation documentAssociation) {
		String decodedNodeRef = restApiService.decodeBase64IfPossible(sourceDocNoderef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		if (documentAssociation.getTargetDocumentNoderef() == null) {
			return ResponseEntity.badRequest().body(TARGET_DOCUMENT_NODEREF_CAN_NOT_BE_NULL);
		}
		if (!(documentAssociation.getType().equals("DEFAULT") || documentAssociation.getType().equals("REPLY") || documentAssociation.getType().equals("FOLLOWUP"))) {
			return ResponseEntity.badRequest().body(INVALID_ASSOCIATION_TYPE);
		}
		int orgId = restApiService.getOrganizationId(organizationName);
		Document sourceDocument = adrImportService.getDocument(orgId, decodedNodeRef);
		if (sourceDocument == null) {
			return ResponseEntity.notFound().build();
		}
		Document targetDocument = adrImportService.getDocument(orgId, documentAssociation.getTargetDocumentNoderef());
		if (targetDocument == null) {
			return ResponseEntity.badRequest().body(TARGET_DOCUMENT_NOT_EXISTS);
		}
		if (sourceDocument.equals(targetDocument)) {
			return ResponseEntity.badRequest().body(SOURCE_AND_TARGET_DOCUMENTS_EQUALS);
		}
		restApiService.setupDocumentAssociationProps(documentAssociation, sourceDocument, targetDocument);
		
		if (!restApiService.getDocumentAssociations(sourceDocument.getId(), targetDocument.getId()).isEmpty()) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(ASSOCIATION_ALREADY_EXISTS);
		}
		documentAssociationMapper.insert(documentAssociation);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}
	
	@PutMapping("/{targetDocNoderef}")
	public ResponseEntity<String> updateDocumentAssociation(@PathVariable String organizationName, @PathVariable String sourceDocNoderef, @PathVariable String targetDocNoderef, @RequestBody DocumentAssociation record) {
		String decodedNodeRef = restApiService.decodeBase64IfPossible(sourceDocNoderef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		if (!(record.getType().equals("DEFAULT") || record.getType().equals("REPLY") || record.getType().equals("FOLLOWUP"))) {
			return ResponseEntity.badRequest().body(INVALID_ASSOCIATION_TYPE);
		}
		int orgId = restApiService.getOrganizationId(organizationName);
		Document sourceDocument = adrImportService.getDocument(orgId, decodedNodeRef);
		if (sourceDocument == null) {
			return ResponseEntity.notFound().build();
		}
		decodedNodeRef = restApiService.decodeBase64IfPossible(targetDocNoderef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		Document targetDocument = adrImportService.getDocument(orgId, decodedNodeRef);
		if (targetDocument == null) {
			return ResponseEntity.badRequest().body(TARGET_DOCUMENT_NOT_EXISTS);
		}
		DocumentAssociationExample documentAssociationExample = new DocumentAssociationExample();
		documentAssociationExample.createCriteria().andSourceDocumentIdEqualTo(sourceDocument.getId()).andTargetDocumentIdEqualTo(targetDocument.getId());
		List<DocumentAssociation> documentAssociations = documentAssociationMapper.selectByExample(documentAssociationExample);
		
		if (documentAssociations.isEmpty()) {
			return createDocumentAssociation(organizationName, sourceDocNoderef, record);
		}
		DocumentAssociation documentAssociation = documentAssociations.get(0);
		documentAssociation.setType(record.getType());
		
		int updated = documentAssociationMapper.updateByPrimaryKey(documentAssociation);
		if (updated == 0) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().build();
		
	}
	
	@GetMapping("/{targetDocNoderef}")
	public ResponseEntity<String> getDocumentAssociation(@PathVariable String organizationName, @PathVariable String sourceDocNoderef, @PathVariable String targetDocNoderef) {
		String decodedNodeRef = restApiService.decodeBase64IfPossible(sourceDocNoderef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		int orgId = restApiService.getOrganizationId(organizationName);
		Document document = adrImportService.getDocument(orgId, decodedNodeRef);
		if (document == null) {
			return ResponseEntity.notFound().build();
		}
		decodedNodeRef = restApiService.decodeBase64IfPossible(targetDocNoderef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		Document targetDocument = adrImportService.getDocument(orgId, decodedNodeRef);
		if (targetDocument == null) {
			return ResponseEntity.badRequest().body(TARGET_DOCUMENT_NOT_EXISTS);
		}
		DocumentAssociationExample documentAssociationExample = new DocumentAssociationExample();
		documentAssociationExample.createCriteria().andSourceDocumentIdEqualTo(document.getId()).andTargetDocumentIdEqualTo(targetDocument.getId());
		List<DocumentAssociation> documentAssociations = documentAssociationMapper.selectByExample(documentAssociationExample);
		
		if (documentAssociations.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		DocumentAssociation documentAssociation = documentAssociations.get(0);
		documentAssociation.setTargetDocumentNoderef(targetDocument.getNoderef());

		return ResponseEntity.ok(restApiService.toJSON(documentAssociation));
	}
	
	@DeleteMapping("/{targetDocNoderef}")
	public ResponseEntity<String> deleteDocumentAssociation(@PathVariable String organizationName, @PathVariable String sourceDocNoderef, @PathVariable String targetDocNoderef) {
		String decodedNodeRef = restApiService.decodeBase64IfPossible(sourceDocNoderef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		int orgId = restApiService.getOrganizationId(organizationName);
		Document document = adrImportService.getDocument(orgId, decodedNodeRef);
		if (document == null) {
			return ResponseEntity.notFound().build();
		}
		decodedNodeRef = restApiService.decodeBase64IfPossible(targetDocNoderef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		Document targetDocument = adrImportService.getDocument(orgId, decodedNodeRef);
		if (targetDocument == null) {
			return ResponseEntity.badRequest().body(TARGET_DOCUMENT_NOT_EXISTS);
		}
		DocumentAssociationExample documentAssociationExample = new DocumentAssociationExample();
		documentAssociationExample.createCriteria().andSourceDocumentIdEqualTo(document.getId()).andTargetDocumentIdEqualTo(targetDocument.getId());
		int deleted = documentAssociationMapper.deleteByExample(documentAssociationExample);
		
		if (deleted == 0) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping
	public ResponseEntity<String> getAllDocumentAssociations(@PathVariable String organizationName, @PathVariable String sourceDocNoderef) {
		String decodedNodeRef = restApiService.decodeBase64IfPossible(sourceDocNoderef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		int orgId = restApiService.getOrganizationId(organizationName);
		Document document = adrImportService.getDocument(orgId, decodedNodeRef);
		if (document == null) {
			return ResponseEntity.notFound().build();
		}
		DocumentAssociationExample documentAssociationExample = new DocumentAssociationExample();
		documentAssociationExample.createCriteria().andSourceDocumentIdEqualTo(document.getId());
		List<DocumentAssociation> documentAssociations = documentAssociationMapper.selectByExample(documentAssociationExample);
		
		for (DocumentAssociation documentAssociation :documentAssociations) {
			Document targetDoc = documentMapper.selectByPrimaryKey(documentAssociation.getTargetDocumentId());
			documentAssociation.setTargetDocumentNoderef(targetDoc.getNoderef());
		}
		return ResponseEntity.ok(restApiService.toJSON(documentAssociations));
	}
	
	@DeleteMapping
	public ResponseEntity<String> deleteAllDocumentAssociations(@PathVariable String organizationName, @PathVariable String sourceDocNoderef) {
		String decodedNodeRef = restApiService.decodeBase64IfPossible(sourceDocNoderef);
		if (decodedNodeRef == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_NODEREF);
		}
		int orgId = restApiService.getOrganizationId(organizationName);
		Document document = adrImportService.getDocument(orgId, decodedNodeRef);
		if (document == null) {
			return ResponseEntity.notFound().build();
		}
		DocumentAssociationExample documentAssociationExample = new DocumentAssociationExample();
		documentAssociationExample.createCriteria().andSourceDocumentIdEqualTo(document.getId());
		int deleted = documentAssociationMapper.deleteByExample(documentAssociationExample);
		
		if (deleted == 0) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.noContent().build();
	}

}
