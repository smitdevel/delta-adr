package ee.webmedia.adr.api.rest.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ee.webmedia.adr.api.rest.service.RestApiService;
import ee.webmedia.adr.dao.CompoundFunctionMapper;
import ee.webmedia.adr.model.CompoundFunction;
import ee.webmedia.adr.model.CompoundFunctionExample;

@RestController
@RequestMapping(value = "/api/v1/organizations/{organizationName}/functions", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class FunctionRestController {

	private final String MARK_AND_TITLE_COMBINATION_ALREADY_EXIST = "Function with this mark and title already exist";

	@Autowired
	private RestApiService restApiService;

	@Autowired
	private CompoundFunctionMapper compoundFunctionMapper;

	@PostMapping
	public ResponseEntity<String> createFunction(@PathVariable String organizationName, @RequestBody CompoundFunction compoundFunction, HttpServletRequest request) {
		int orgId = restApiService.getOrganizationId(organizationName);
		if (!restApiService.getCompoundFunctions(orgId, compoundFunction.getMark(), compoundFunction.getTitle()).isEmpty()) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(MARK_AND_TITLE_COMBINATION_ALREADY_EXIST);
		}
		restApiService.setupFunctionProps(compoundFunction, orgId);
		compoundFunctionMapper.insert(compoundFunction);
		return ResponseEntity.created(restApiService.createFunctionUri(compoundFunction, request.getServerName())).build();
	}

	@GetMapping
	public ResponseEntity<String> getAllFunctions(@PathVariable String organizationName) {
		CompoundFunctionExample compoundFunctionExample = new CompoundFunctionExample();
		compoundFunctionExample.createCriteria().andOrganizationIdEqualTo(restApiService.getOrganizationId(organizationName));
		return ResponseEntity.ok(restApiService.toJSON(compoundFunctionMapper.selectByExample(compoundFunctionExample)));
	}
	
	@PutMapping("/{mark}/{title}")
	public ResponseEntity<String> updateFunction(@PathVariable String organizationName, @PathVariable String mark, @PathVariable String title, @RequestBody CompoundFunction record,  HttpServletRequest request) {
		int orgId = restApiService.getOrganizationId(organizationName);
		mark = restApiService.decodeBase64IfPossible(mark);
		title = restApiService.decodeBase64IfPossible(title);
		if (mark == null || title == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(orgId, mark, title);
		record.setMark(mark);
		record.setTitle(title);
		if (compoundFunctions.isEmpty()) {
			return createFunction(organizationName, record, request);
		}
		
		CompoundFunction compoundFunction = compoundFunctions.get(0);
		record.setId(compoundFunction.getId());
		record.setOrganizationId(orgId);
		record.setCreatedDateTime(compoundFunction.getCreatedDateTime());
		record.setModifiedDateTime(new Date());
		int updated = compoundFunctionMapper.updateByPrimaryKey(record);
		if (updated == 0) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{mark}/{title}")
	public ResponseEntity<String> getFunction(@PathVariable String organizationName, @PathVariable String mark, @PathVariable String title) {
		mark = restApiService.decodeBase64IfPossible(mark);
		title = restApiService.decodeBase64IfPossible(title);
		if (mark == null || title == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		List<CompoundFunction> compoundFunctions = restApiService.getCompoundFunctions(restApiService.getOrganizationId(organizationName), mark, title);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(restApiService.toJSON(compoundFunctions.get(0)));
	}

	@DeleteMapping("/{mark}/{title}")
	public ResponseEntity<String> deleteFunction(@PathVariable String organizationName, @PathVariable String mark, @PathVariable String title) {
		mark = restApiService.decodeBase64IfPossible(mark);
		title = restApiService.decodeBase64IfPossible(title);
		if (mark == null || title == null) {
			return ResponseEntity.badRequest().body(RestApiService.UNABLE_TO_DECODE_MARK_OR_TITLE);
		}
		CompoundFunctionExample compoundFunctionExample = restApiService.getCompoundFunctionExample(restApiService.getOrganizationId(organizationName), mark, title);
		List<CompoundFunction> compoundFunctions = compoundFunctionMapper.selectByExample(compoundFunctionExample);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		restApiService.deleteSeries(organizationName, compoundFunctions.get(0));
		int deleted = compoundFunctionMapper.deleteByExample(compoundFunctionExample);
		if (deleted == 0) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping
	public ResponseEntity<String> deleteAllFunctions(@PathVariable String organizationName) {
		CompoundFunctionExample compoundFunctionExample = restApiService.getCompoundFunctionExample(restApiService.getOrganizationId(organizationName));
		List<CompoundFunction> compoundFunctions = compoundFunctionMapper.selectByExample(compoundFunctionExample);
		if (compoundFunctions.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		for (CompoundFunction compoundFunction : compoundFunctions) {
			 deleteFunction(organizationName, compoundFunction.getMark(), compoundFunction.getTitle());
		}
		return ResponseEntity.noContent().build();
	}
}
