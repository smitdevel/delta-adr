package ee.webmedia.adr;

public class PageNumberRedirectException extends Exception {
    private static final long serialVersionUID = 1L;

    private final String view;

    public PageNumberRedirectException(String view) {
        this.view = view;
    }

    public String getView() {
        return view;
    }

}
