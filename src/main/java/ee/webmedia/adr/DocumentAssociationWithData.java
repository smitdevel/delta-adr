package ee.webmedia.adr;

import ee.webmedia.adr.model.DocumentAssociation;

public class DocumentAssociationWithData extends DocumentAssociation {

    private boolean source;
    private String otherDocumentId;
    private String regNumber;
    private String title;
    private String typeTitle;

    public boolean isSource() {
        return source;
    }

    public void setSource(boolean source) {
        this.source = source;
    }

    public String getOtherDocumentId() {
        return otherDocumentId;
    }

    public void setOtherDocumentId(String otherDocumentId) {
        this.otherDocumentId = otherDocumentId;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTypeTitle() {
        return typeTitle;
    }

    public void setTypeTitle(String typeTitle) {
        this.typeTitle = typeTitle;
    }

}
