package ee.webmedia.adr;

import static org.apache.ibatis.jdbc.SelectBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SelectBuilder.FROM;
import static org.apache.ibatis.jdbc.SelectBuilder.JOIN;
import static org.apache.ibatis.jdbc.SelectBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SelectBuilder.SELECT;
import static org.apache.ibatis.jdbc.SelectBuilder.SQL;
import static org.apache.ibatis.jdbc.SelectBuilder.WHERE;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ee.webmedia.adr.service.AdrImportServiceImpl;

public class DocumentSearchSqlProvider {
    private static final Logger log = LoggerFactory.getLogger(DocumentSearchSqlProvider.class);

    public String selectByAdvancedSearchForm(AdvancedSearchQuery advancedSearch) {
        BEGIN();
        SELECT("document.*");
        SELECT("document_type.title AS type_title");
        FROM("document");
        JOIN("document_type ON document.type_id = document_type.id");
        WHERE("document.organization_id = #{organizationId}");
        if (advancedSearch.getRegDateBegin() != null) {
            WHERE("document.reg_date_time >= #{regDateBegin}");
        }
        if (advancedSearch.getRegDateEnd() != null) {
            WHERE("document.reg_date_time <= #{regDateEnd}");
        }
        if (StringUtils.isNotEmpty(advancedSearch.getDocumentTypesArray())) {
            WHERE("document.type_id = ANY(#{documentTypesArray}::integer[])");
        }
        if (StringUtils.isNotEmpty(advancedSearch.getTitleTsquery())) {
            WHERE("to_tsvector('simple', document.title) @@ #{titleTsquery}::tsquery");
        }
        if (StringUtils.isNotEmpty(advancedSearch.getPartyTsquery())) {
            WHERE("to_tsvector('simple', document.party) @@ #{partyTsquery}::tsquery");
        }
        if (StringUtils.isNotEmpty(advancedSearch.getSenderRegNumberEscapedLike())) {
            WHERE("document.sender_reg_number ILIKE #{senderRegNumberEscapedLike}");
        }
        if (StringUtils.isNotEmpty(advancedSearch.getAccessRestriction())) {
            WHERE("document.access_restriction = #{accessRestriction}");
        }
        if (StringUtils.isNotEmpty(advancedSearch.getAccessRestrictionReasonTsquery())) {
            WHERE("to_tsvector('simple', document.access_restriction_reason) @@ #{accessRestrictionReasonTsquery}::tsquery");
        }
        if (advancedSearch.getAccessRestrictionBeginDate() != null) {
            WHERE("document.access_restriction_begin_date >= #{accessRestrictionBeginDate}");
        }
        if (advancedSearch.getAccessRestrictionEndDate() != null) {
            WHERE("document.access_restriction_end_date <= #{accessRestrictionEndDate}");
        }
        if (StringUtils.isNotEmpty(advancedSearch.getTransmittalModesArray())) {
            WHERE("string_to_array (lower(document.transmittal_mode), ', ') && string_to_array (lower(#{transmittalModesArray}), ',')");
        }
        ORDER_BY("document.reg_date_time DESC, document.reg_number DESC LIMIT #{limit} OFFSET #{offset}");
        String sql = SQL();
        if (log.isDebugEnabled()) {
            log.debug("Advanced search SQL: " + sql + "\n  advancedSearchQuery=" + AdrImportServiceImpl.toString(advancedSearch));
        }
        return sql;
    }

}
