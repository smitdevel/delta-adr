#!/bin/sh
set -eu
psql -e -c "DROP DATABASE adr" -U postgres -h localhost
psql -e -c "CREATE DATABASE adr WITH ENCODING='UTF8' OWNER=adr" -U postgres -h localhost
pg_restore -d adr -U adr adr-dump.sql -h localhost
echo "Successfully completed"
