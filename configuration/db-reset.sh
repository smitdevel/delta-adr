#!/bin/sh
set -eu
psql -e -c "DROP DATABASE adr" -U postgres -h localhost
psql -e -c "CREATE DATABASE adr WITH ENCODING='UTF8' OWNER=adr" -U postgres -h localhost
psql -f adr.sql -U adr -h localhost
echo "Successfully completed"
