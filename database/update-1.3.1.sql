-- Adding a new field to the web service
ALTER TABLE document ADD COLUMN by_request_only boolean DEFAULT FALSE;
-- Allow duplicate file names in ADR and distinguish them by noderef
ALTER TABLE file DROP CONSTRAINT file_document_id_key;
ALTER TABLE file ADD COLUMN noderef text;
UPDATE file SET noderef = document_id || '_' || name;
ALTER TABLE file ALTER COLUMN noderef SET NOT NULL;
