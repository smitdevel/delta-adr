-- Adding a new field to the web service
ALTER TABLE document ADD COLUMN access_restriction_change_reason text NULL;

-- Add new indexes
CREATE INDEX compound_function_sort ON compound_function (organization_id ASC, order_num ASC, mark ASC, title ASC);
CREATE INDEX compound_series_sort ON compound_series (compound_function_id ASC, order_num ASC, mark ASC, title ASC);
CREATE INDEX volume_sort ON volume (compound_series_id ASC, valid_from_date DESC, mark ASC, title ASC);
CREATE INDEX document_sort ON document (volume_id ASC, reg_date_time DESC, reg_number DESC);
CREATE INDEX document_association_source_sort ON document_association (source_document_id ASC, type ASC);
CREATE INDEX document_association_target_sort ON document_association (target_document_id ASC, type ASC);
CREATE INDEX file_sort ON file (document_id ASC, name ASC); 