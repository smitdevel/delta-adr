--CREATE ROLE adr LOGIN PASSWORD 'adr';
--CREATE DATABASE adr WITH ENCODING='UTF8' OWNER=adr;

CREATE SEQUENCE default_seq;

CREATE TABLE compound_function
(
   id integer NOT NULL, 
   organization_id integer NOT NULL,
   mark text NOT NULL, 
   title text NOT NULL,
   order_num int NULL,
   created_date_time timestamp with time zone NOT NULL, 
   modified_date_time timestamp with time zone NOT NULL, 
    PRIMARY KEY (id), 
    UNIQUE (organization_id, mark, title)
) 
WITH (
  OIDS = FALSE
)
;

CREATE TABLE function
(
   id integer NOT NULL, 
   compound_function_id integer NOT NULL, 
   noderef text NOT NULL, 
   created_date_time timestamp with time zone NOT NULL, 
   modified_date_time timestamp with time zone NOT NULL, 
    PRIMARY KEY (id), 
    FOREIGN KEY (compound_function_id) REFERENCES compound_function (id), 
    UNIQUE (compound_function_id, noderef)
    -- actually unique is all function/series/volume noderefs under organization_id
) 
WITH (
  OIDS = FALSE
)
;

CREATE TABLE compound_series
(
   id integer NOT NULL, 
   compound_function_id integer NOT NULL, 
   mark text NOT NULL, 
   title text NOT NULL, 
   order_num int NULL,
   created_date_time timestamp with time zone NOT NULL, 
   modified_date_time timestamp with time zone NOT NULL, 
    PRIMARY KEY (id), 
    FOREIGN KEY (compound_function_id) REFERENCES compound_function (id), 
    UNIQUE (compound_function_id, mark, title)
) 
WITH (
  OIDS = FALSE
)
;

CREATE TABLE series
(
   id integer NOT NULL, 
   compound_series_id integer NOT NULL, 
   noderef text NOT NULL, 
   created_date_time timestamp with time zone NOT NULL, 
   modified_date_time timestamp with time zone NOT NULL, 
    PRIMARY KEY (id), 
    FOREIGN KEY (compound_series_id) REFERENCES compound_series (id), 
    UNIQUE (compound_series_id, noderef)
    -- actually unique is all function/series/volume noderefs under organization_id
) 
WITH (
  OIDS = FALSE
)
;

CREATE TABLE volume
(
   id integer NOT NULL, 
   compound_series_id integer NOT NULL, 
   noderef text NOT NULL, 
   mark text NOT NULL, 
   title text NOT NULL, 
   valid_from_date date NOT NULL, 
   valid_to_date date NULL, 
   created_date_time timestamp with time zone NOT NULL, 
   modified_date_time timestamp with time zone NOT NULL, 
    PRIMARY KEY (id), 
    FOREIGN KEY (compound_series_id) REFERENCES compound_series (id), 
    UNIQUE (compound_series_id, noderef)
    -- actually unique is all function/series/volume noderefs under organization_id
) 
WITH (
  OIDS = FALSE
)
;

CREATE TABLE document_type
(
   id integer NOT NULL, 
   organization_id integer NOT NULL,
   name text NOT NULL, 
   title text NOT NULL, 
   created_date_time timestamp with time zone NOT NULL, 
   modified_date_time timestamp with time zone NOT NULL, 
    PRIMARY KEY (id),
    UNIQUE (organization_id, name)
) 
WITH (
  OIDS = FALSE
)
;

CREATE TABLE document
(
   id integer NOT NULL, 
   organization_id integer NOT NULL,
   noderef text NOT NULL, 
   reg_date_time timestamp with time zone NOT NULL, 
   reg_number text NOT NULL, 
   title text NOT NULL, 
   type_id integer NOT NULL, 
   volume_id integer NOT NULL,
   access_restriction text NOT NULL,
   access_restriction_reason text NOT NULL,
   access_restriction_begin_date date NULL,
   access_restriction_end_date date NULL,
   access_restriction_end_desc text NOT NULL,
   due_date date NULL,
   compliance_date date NULL,
   send_date timestamp with time zone NULL,
   compilator text NOT NULL,
   annex text NOT NULL,
   sender_reg_number text NOT NULL,
   transmittal_mode text NOT NULL,
   party text NOT NULL,
   by_request_only boolean DEFAULT FALSE,
   access_restriction_change_reason text NULL,
   created_date_time timestamp with time zone NOT NULL, 
   modified_date_time timestamp with time zone NOT NULL, 
    PRIMARY KEY (id), 
    FOREIGN KEY (type_id) REFERENCES document_type (id),
    FOREIGN KEY (volume_id) REFERENCES volume (id),
    UNIQUE (organization_id, noderef)
) 
WITH (
  OIDS = FALSE
)
;
CREATE INDEX document_quicksearch ON document USING gin(to_tsvector('simple', title || ' ' || access_restriction || ' ' || access_restriction_reason || ' ' || access_restriction_end_desc || ' ' || annex || ' ' || transmittal_mode || ' ' || compilator || ' ' || party));

-- TODO NB! It would be correct to duplicate the organization_id column here so it would be possible to implement UNIQUE(organization_id, noderef) constraint for files
CREATE TABLE file
(
   id integer NOT NULL, 
   document_id integer NOT NULL, 
   name text NOT NULL, 
   title text NOT NULL, 
   size integer NOT NULL, 
   mime_type text NOT NULL, 
   encoding text NOT NULL,
   file_modified_date_time timestamp with time zone NOT NULL, 
   created_date_time timestamp with time zone NOT NULL, 
   modified_date_time timestamp with time zone NOT NULL,
   noderef text NOT NULL,
    PRIMARY KEY (id), 
    FOREIGN KEY (document_id) REFERENCES document (id)
) 
WITH (
  OIDS = FALSE
)
;

-- kui seos ja lisamise hetkel target dok ei eksisteeri
-- target dok tuleb hiljem, siis tal endal on seos olemas (teistpidi)

-- CREATE TYPE association_type AS ENUM ('DEFAULT', 'FOLLOWUP', 'REPLY');
-- TODO need to get ^^ TYPE working in mybatis 

CREATE TABLE document_association
(
   id integer NOT NULL, 
   source_document_id integer NOT NULL, -- initial document
   target_document_id integer NOT NULL, -- followup or reply document 
   type text NOT NULL, 
   created_date_time timestamp with time zone NOT NULL, 
    PRIMARY KEY (id), 
    FOREIGN KEY (source_document_id) REFERENCES document (id), 
    FOREIGN KEY (target_document_id) REFERENCES document (id), 
    UNIQUE (source_document_id, target_document_id, type)
    -- in addition to ^^ this constraint, there should not exist same type of assoc between two docs where source and target are switched, it is checked in code
) 
WITH (
  OIDS = FALSE
)
;

CREATE TABLE import
(
   id integer NOT NULL, 
   organization_id integer NOT NULL, 
   query_begin_date date NOT NULL, 
   query_end_date date NOT NULL, 
   execution_date_time timestamp with time zone NOT NULL, 
   type text NOT NULL, 
   documents_created integer NOT NULL, 
   documents_updated integer NOT NULL, 
   documents_deleted integer NOT NULL, 
   documents_error integer NOT NULL, 
    PRIMARY KEY (id) 
) 
WITH (
  OIDS = FALSE
)
;

-- Add indexes for common query needs
CREATE INDEX compound_function_sort ON compound_function (organization_id ASC, order_num ASC, mark ASC, title ASC);
CREATE INDEX compound_series_sort ON compound_series (compound_function_id ASC, order_num ASC, mark ASC, title ASC);
CREATE INDEX volume_sort ON volume (compound_series_id ASC, valid_from_date DESC, mark ASC, title ASC);
CREATE INDEX document_sort ON document (volume_id ASC, reg_date_time DESC, reg_number DESC);
CREATE INDEX document_association_source_sort ON document_association (source_document_id ASC, type ASC);
CREATE INDEX document_association_target_sort ON document_association (target_document_id ASC, type ASC);
CREATE INDEX file_sort ON file (document_id ASC, name ASC);