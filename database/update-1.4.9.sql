ALTER TABLE file ADD COLUMN access_restriction text;
ALTER TABLE file ADD COLUMN inner_access_restriction text;
ALTER TABLE file ADD COLUMN public_access boolean DEFAULT TRUE NOT NULL;
