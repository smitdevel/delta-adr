<?xml version="1.0" ?>
<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:pom="http://maven.apache.org/POM/4.0.0"
	version="1.0"
>
	<xsl:output
		method="xml" 
	/>

	<xsl:param
		name="cl.versionString"
	/>

	<xsl:template
		match="/pom:project/pom:version"
	>
		<xsl:call-template
			name="replaceVersion"
		/>
	</xsl:template>

	<xsl:template
		match="/pom:project/pom:parent/pom:version"
	>
		<xsl:call-template
			name="replaceVersion"
		/>
	</xsl:template>

	<xsl:template
		name="replaceVersion"
	>
		<xsl:copy>
			<xsl:value-of select="$cl.versionString"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template 
		match="/|node()|@*"
	>
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>